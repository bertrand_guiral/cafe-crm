package bg.calendar.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import com.google.api.services.calendar.model.Event;


public abstract class AbstractProcessFileExcel implements Runnable {

	protected File fileInput = new File("Classeur4.xlsx");
	protected ILogger logger; 
	private Thread thread; 
	CalendarBg calendarBg = new CalendarBg();
	
	public AbstractProcessFileExcel(String fileName, CalendarBg calendarBg, ILogger logger) throws Exception {
		this(new File(fileName),calendarBg,logger);
	}

	public AbstractProcessFileExcel(File file, CalendarBg calendarBg, ILogger logger) throws Exception {
		this.fileInput = file;
		this.logger = logger;
		this.calendarBg = calendarBg;
		
		thread = new Thread(this);
		thread.start();
	}

	abstract protected List<EventBg> processFile() throws Exception;

	
	protected void log(String s) {
		if (this.logger == null){
			System.out.println(s);
		}else {
			this.logger.log(s);
		}
	}


	public void run() {
		try {
			List<EventBg> listEvents = processFile();
			log("Start sending info to gmail .... ");
			log(" Please wait ....");
			this.processListEvent(listEvents);
		}  catch (Exception e) {
			e.printStackTrace();
			if (this.logger != null){
				this.logger.alert(e.getMessage());
				this.logger.log(e);
			}
		}
	}

	

	private void processListEvent(List<EventBg> listEvents2) {
		
		this.log("processListEvent start");
		int i=0;
		for(EventBg eventBg : listEvents2){
			try {
				
				 calendarBg.innsert(eventBg);
				this.logger.log("Inserted  "+i+++" "+eventBg);
			} catch (Exception e) {
				this.logger.log("---ProcessListEvent excep :"+e.getMessage());
				e.printStackTrace();
			}
		}
		this.logger.log("The End ");
		
	}

	public void stop() {
		thread.interrupt();
	}

}
