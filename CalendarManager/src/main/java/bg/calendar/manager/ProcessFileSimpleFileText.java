package bg.calendar.manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.mortbay.log.Log;

public class ProcessFileSimpleFileText extends AbstractProcessFileExcel {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh/mm/ss");
	SimpleDateFormat sdf_2 = new SimpleDateFormat("dd/MM/yyyy");

	public ProcessFileSimpleFileText(String fileName, CalendarBg calendarBg,ILogger logger) throws Exception {
		super(fileName, calendarBg, logger);

	}

	public ProcessFileSimpleFileText(File fileExcel, CalendarBg calendarBg, ILogger logger) throws Exception {
		super(fileExcel,calendarBg, logger);
		
	}

	String[] columns;

	private List<EventBg> processFileExcel() throws Exception{
			File fileExcel = this.fileInput;
			log("Start reading Excel File");
			
			log(" File f : " + fileExcel.exists() + "  "+ fileExcel.getAbsolutePath());
			log("Please wait ....");
			FileInputStream fin = new FileInputStream(fileExcel);
			Workbook wb = new XSSFWorkbook(fin);
			 List<EventBg> list = process(wb);
			fin.close();
			log("Lecture du fichier termin�e");
			log("Phase suivante : Envoie des informations vers gmail");
			return list;
	}
	
	
	public List<EventBg> process(Workbook wb) throws Exception{		
		Sheet xssfSheet = wb.getSheetAt(0);
		Iterator ite = xssfSheet.rowIterator();
		List<EventBg> listEvents = new ArrayList<EventBg>();
		int i=0;
		while (ite.hasNext()) {
			XSSFRow xssfRow = (XSSFRow) ite.next();
			String lineevent = toLine(xssfRow);
			log("lineevent >"+lineevent);
			EventBg event = parse(lineevent);
			log(i+++" "+event);
			listEvents.add(event);
		}
		
		log("End reading file");
		log("Please wait ..... ");
		return listEvents;
	}
	
	private  String  toLine(XSSFRow xssfRow) {
		int nbCell = xssfRow.getPhysicalNumberOfCells();
		String line="";
		System.out.println("xssfRow   AAA "+xssfRow.getCell(0)+" |  type: "+xssfRow.getCell(0).getCellType());
		System.out.println("xssfRow   BBB ");
		for (int i = 0; i < nbCell; i++) {
			
			XSSFCell cell = xssfRow.getCell(i);
			String cellStr = parseCell(cell,i);
			line += cellStr+" ";
		  
		}
		
		
		return line;
	}
	
	private  String parseCell(XSSFCell cell,int i) {
		String s="";
		if (cell == null) {

		} else {
			int type = cell.getCellType();
			
			if (type == Cell.CELL_TYPE_STRING) {
				s = cell.getStringCellValue();
			} else if (type == Cell.CELL_TYPE_NUMERIC) {
				if (DateUtil.isCellDateFormatted(cell) && (i==0) ) {
                   s += sdf_2.format(cell.getDateCellValue());
                }else {
                	s = ""+(long) cell.getNumericCellValue();
                }
			} else if (type == Cell.CELL_TYPE_BLANK) {
				s = "";
			} else if (type == Cell.CELL_TYPE_FORMULA) {
				s = "";
			}else {
				s = "";
				log(" unknown cell  !!!  "+type);
			}
			
		}
		return s;
	}
	

	@Override
	protected List<EventBg> processFile() throws Exception {
		if (this.fileInput.getName().endsWith(".xlsx")){
			return processFileExcel();
		}else if (this.fileInput.getName().endsWith(".xlt")){
			throw new Exception("Invalid format: .xlsx or .csv only");
		}else {
			return processFileSimpleText();
		}
	}
	
	protected List<EventBg> processFileSimpleText() throws Exception {
		
		System.out.println("ProcessFileSimpleFileText ProcessFile "+fileInput); 
		List<EventBg> list = new ArrayList<EventBg>();
		BufferedReader r = new BufferedReader(new FileReader(this.fileInput));
		String line = r.readLine();
		System.out.println("ProcessFileSimpleFileText ProcessFile  line :"+line);
		columns = line.split(";");
		while ((line = r.readLine()) != null) {
			System.out.println("ProcessFileSimpleFileText ProcessFile  line :"+line);
			EventBg eventBg = parse(line);
			this.log(""+eventBg);
			if (eventBg != null){
				list.add(eventBg);
			}
		}
		return list;
	}

	private EventBg parse(String line) {
		EventBg eventBg  = null;
		try {
			
			line = line.trim();
			if (line.length() > 10) {
			// Date;PhoneNumber;PhoneUser;Duration;TypeCall;ContactName;Comment;100;
			StringTokenizer st = new StringTokenizer(line," ,;:");
			String dateStr = st.nextToken();
			String title = line.substring(dateStr.length()+1);
				title=title.trim().replace(","," ").replace(";"," ");
				Date dateStart =  parseDate(dateStr); 
				long duration = 1;
				Date dateEnd = new Date(dateStart.getTime() + duration + 1000);
				title = "CRM "+title;
				
				eventBg = new EventBg(dateStart, dateEnd, title);
				
			}else {
				System.out.println("parse Event s.length < 10  length : "+line);
			}
		} catch (ExceptionNoDate e) {
			this.logger.log("Not Event line !! "+e.getMessage());
		} catch (Exception e) {
						// TODO Auto-generated catch block
			e.printStackTrace();
			this.logger.log(e);
			
		}
		return eventBg;
	}

	private Date parseDate(String dateStr) throws Exception {
		dateStr = dateStr.replace('.', '/').replace(':', '/').replace("\"", " ").trim();
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (Exception e) {
			try {
				date = sdf_2.parse(dateStr);
			} catch (Exception e1) {
			}
		}
		if (date==null ) throw new ExceptionNoDate("Unparsable date : >"+dateStr+"<");
		return date;
	}

}
