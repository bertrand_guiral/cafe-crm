/*
 * Copyright (c) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package bg.calendar.manager;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.Lists;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar.CalendarList.Delete;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Calendar;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Event.Creator;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.swing.JSpinner.DateEditor;

/**
 * @author Yaniv Inbar
 */
public class CalendarBg {

	/**
	 * Be sure to specify the name of your application. If the application name
	 * is {@code null} or blank, the application will log a warning. Suggested
	 * format is "MyCompany-ProductName/1.0".
	 */
	private final String APPLICATION_NAME = "";
	private static String CALENDAR__SUMMARY = "WORK CALENDAR";
	/** Directory to store user credentials. */
	private final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"), ".store/calendar_sample");

	/**
	 * Global instance of the {@link DataStoreFactory}. The best practice is to
	 * make it a single globally shared instance across your application.
	 */
	private FileDataStoreFactory dataStoreFactory;

	/** Global instance of the HTTP transport. */
	private HttpTransport httpTransport;

	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	private com.google.api.services.calendar.Calendar clientService;

	static final java.util.List<Calendar> addedCalendarsUsingBatch = Lists.newArrayList();

	public CalendarBg() {
		init();
	}

	/** Authorizes the installed application to access user's protected data. */
	private Credential authorize() throws Exception {
		// load client secrets
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(CalendarBg.class.getResourceAsStream("/client_secrets.json")));

		// set up authorization code flow
		GoogleAuthorizationCodeFlow googleAuthorizationCodeFlow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets, Collections.singleton(CalendarScopes.CALENDAR)).setDataStoreFactory(dataStoreFactory).build();
		// authorize
		AuthorizationCodeInstalledApp acia = new AuthorizationCodeInstalledApp(googleAuthorizationCodeFlow, new LocalServerReceiver());
		Credential credential = acia.authorize("user");
		return credential;
	}

	public static void main(String[] args) {
		new CalendarBg();
	}

	private void init() {
		try {
			// initialize the transport
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();

			// initialize the data store factory
			dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);

			// authorization
			Credential credential = authorize();
			System.out.println("CalendarBg    credential " + credential);
			// set up global Calendar instance
			clientService = new com.google.api.services.calendar.Calendar.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
			//getWorkCalandarOrCreate();
		} catch (Throwable t) {
			t.printStackTrace();
		}

	}
	public void insertTest()  throws Exception{
		for(CalendarListEntry calend : listSelectedCalendar){
			insertTest(calend.getId());
		}
	}
	public void insertTest(String calendarWorkId) throws Exception {
		
		addEventTest(calendarWorkId, new Date(), "bgTest zorg toto yaya");
		Events events = searchEvents(calendarWorkId, "bgTest");
		for (Event event : events.getItems()) {
			System.out.println("    ---------- " + event.getSummary());
		}
	}

	String calendarWorkId___OLD = null;
	List<CalendarListEntry> listSelectedCalendar = new ArrayList<CalendarListEntry>();
	public CalendarList getCalendarList() throws Exception {
		CalendarList calendarList = UtilCalendars.getCalendarList(clientService);
		return calendarList;
	}
	
	
	public void getWorkCalandarOrCreate_() throws Exception {
		CalendarListEntry calendarListEntry = getWorkCalendar(CALENDAR__SUMMARY);
		if (calendarListEntry == null) {
			System.out.println("calendarListEntry is null for :" + CALENDAR__SUMMARY);
			Calendar calendar = this.addCalendar(CALENDAR__SUMMARY," Bg Calendar Created");
		} else {
		}
	}

	private CalendarListEntry getWorkCalendar(String summary) throws Exception {
		CalendarList calendarList = UtilCalendars.getCalendarList(clientService);
		System.out.println("getWorkCalendar size :" + calendarList.size());
		int i = 0;
		CalendarListEntry calendarSelected = null;
		for (CalendarListEntry entry : calendarList.getItems()) {

			System.out.println(i++ + " ---------------------------------------------id :" + entry.getId() + " | description :" + entry.getDescription() + " | summary " + entry.getSummary());
			if (entry.getSummary().trim().equalsIgnoreCase(summary)) {
				calendarSelected = entry;
			}
		}
		return calendarSelected;
	}

	public Calendar addCalendar(String summary, String description) throws IOException {
		View.header("Add Calendar");
		Calendar entry = new Calendar();
		entry.setSummary(summary);
		entry.setDescription(description);
		
		Calendar result = clientService.calendars().insert(entry).execute();
		System.out.println("addCalendar ID: " + result.getId());
		System.out.println("addCalendarSummary: " + result.getSummary());
		if (entry.getDescription() != null) {
			System.out.println("Description: " + result.getDescription());
		}
		View.display(result);
		return result;
	}
	
	public void deleteCalendar(String calendarId) throws IOException {
		com.google.api.services.calendar.Calendar.Calendars.Delete delete = clientService.calendars().delete(calendarId);
		delete.execute();
	}

	private void addEventTest(String calendarId, Date date, String title) throws IOException {
		View.header("Add Event");
		Event event = newEvent();
		event.set("bg", "Blablab ");
		event.set("bg2", "Blablab ");
		event.setSummary(title);
		event.setDescription("BgDescription");
		Creator creator = new Creator();
		creator.setDisplayName("bgCreator");
		event.setCreator(creator);
		event.setKind("bgKind");

		Date endDate = new Date(date.getTime() + 1000);
		DateTime start = new DateTime(date, TimeZone.getTimeZone("UTC"));
		event.setStart(new EventDateTime().setDateTime(start));
		DateTime end = new DateTime(endDate, TimeZone.getTimeZone("UTC"));
		event.setEnd(new EventDateTime().setDateTime(end));
		Event result = clientService.events().insert(calendarId, event).execute();
		View.display(result);
	}

	private Event newEvent() {
		Event event = new Event();
		event.setSummary("New Event");
		Date startDate = new Date();
		Date endDate = new Date(startDate.getTime() + 3600000);
		DateTime start = new DateTime(startDate, TimeZone.getTimeZone("UTC"));
		event.setStart(new EventDateTime().setDateTime(start));
		DateTime end = new DateTime(endDate, TimeZone.getTimeZone("UTC"));
		event.setEnd(new EventDateTime().setDateTime(end));
		return event;
	}

	private Events searchEvents(String calendarId, String q) throws IOException {
		com.google.api.services.calendar.Calendar.Events events = clientService.events();
		com.google.api.services.calendar.Calendar.Events.List list = events.list(calendarId);
		list.setQ(q);
		Events feed = list.execute();
		return feed;
	}

	private void showEvents(Calendar calendar) throws IOException {
		View.header("Show Events");
		Events feed = clientService.events().list(calendar.getId()).execute();
		View.displayEvent(feed);
	}

	private void deleteCalendarsUsingBatch() throws IOException {
		View.header("Delete Calendars Using Batch");
		BatchRequest batch = clientService.batch();
		for (Calendar calendar : addedCalendarsUsingBatch) {
			clientService.calendars().delete(calendar.getId()).queue(batch, new JsonBatchCallback<Void>() {

				public void onSuccess(Void content, HttpHeaders responseHeaders) {
					System.out.println("Delete is successful!");
				}

				@Override
				public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
					System.out.println("Error Message: " + e.getMessage());
				}
			});
		}

		batch.execute();
	}

	private void deleteCalendar(Calendar calendar) throws IOException {
		View.header("Delete Calendar");
		clientService.calendars().delete(calendar.getId()).execute();
	}

	public void  innsert(EventBg eventBg) throws Exception {
		System.out.println("innsert event : title : "+eventBg.getTitle()+" date Start: "+eventBg.getDateStart()+"  DateEnd "+eventBg.getDateEnd());
		Event event = newEvent();
	
		event.setSummary(eventBg.getTitle());
		event.setDescription(eventBg.getTitle());
		Creator creator = new Creator();
		creator.setDisplayName("bgCreator");
		event.setCreator(creator);
		event.setKind("CRM");

		DateTime start = new DateTime(eventBg.getDateStart(), TimeZone.getTimeZone("UTC"));
		event.setStart(new EventDateTime().setDateTime(start));
		DateTime end = new DateTime(eventBg.getDateEnd(), TimeZone.getTimeZone("UTC"));
		event.setEnd(new EventDateTime().setDateTime(end));
		for(CalendarListEntry calendarListEntry : listSelectedCalendar){
			
			Event result = clientService.events().insert(calendarListEntry.getId(), event).execute();
			System.out.println(" REsult Event ::: "+result+"  "+result.getStart()+"  "+result.getEnd()+ "   "+result.getSummary()+"  "+calendarListEntry.getId());
		}
		return ;
	}

	public void setListSelectedCalendar(List<CalendarListEntry> listSelectedCalendar) {
		this.listSelectedCalendar = listSelectedCalendar;
	}
	
	
}
