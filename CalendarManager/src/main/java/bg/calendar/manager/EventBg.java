package bg.calendar.manager;

import java.util.Date;
import java.util.Properties;


public class EventBg {
	private Date dateStart;
	private Date dateEnd;
	private String title;
	
	public EventBg(Date dateStart, Date dateEnd, String title) {
		super();
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.title = title;
		
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	

	@Override
	public String toString() {
		return "dateStart=" + dateStart +" title = " + title +  "";
	}

}
