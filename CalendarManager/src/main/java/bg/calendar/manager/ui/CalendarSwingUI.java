package bg.calendar.manager.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.filechooser.FileFilter;

import bg.calendar.manager.AbstractProcessFileExcel;
import bg.calendar.manager.CalendarBg;
import bg.calendar.manager.ILogger;
import bg.calendar.manager.ProcessFileSimpleFileText;

import com.google.api.services.calendar.model.CalendarListEntry;

public class CalendarSwingUI implements ILogger {

	private static String KEY_DIRECTORY_FILE_CHOOSER = "DIRECTORY_FILE_CHOOSER";
	private static String KEY_GMAIL_LOGIN = "GMAIL_LOGIN";
	JFrame frame = new JFrame();
	JDialog dialogLog;
	Properties properties = new Properties();
	File fileProperties = new File("preferencesCalendar.properties");
	JTextField labelFieldFileExcel = new JTextField();
	JButton buttonFileChooserExcel = new JButton("Text File To Import");
	JButton buttonStartImport = new JButton("Process File Selected");
	
	JTextArea textArea = new JTextArea(10, 80);
	JScrollPane scrollPaneTextArea;
	JFileChooser fileChooser = new JFileChooser();
	
	JPanelGoogleCalendarInfo panelGoogleInfo;
	FileFilter fileFilterDirAndExcell__ = new FileFilter() {
		

		@Override
		public String getDescription() {
			return "Excel";
		}

		@Override
		public boolean accept(File f) {
			if (f.isDirectory()) {
				return true;
			}
			return (f.getName().endsWith(".xlsx") || (f.getName().endsWith(".csv")));
		}
	};

	public CalendarSwingUI() throws Exception {
		labelFieldFileExcel.setText("xxx.txt");
		if (fileProperties.exists()) {
			FileReader reader = new FileReader(fileProperties);
			properties.load(reader);
			reader.close();
			String directoryFileChooser = properties.getProperty(KEY_DIRECTORY_FILE_CHOOSER, ".");
			fileChooser.setCurrentDirectory(new File(directoryFileChooser));
		}

		JMenuItem menuInfo = new JMenuItem("Info");
		menuInfo.setEnabled(true);
		menuInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frame, "This application can import events from excel file to  Google Calendar ", "Info", JOptionPane.PLAIN_MESSAGE);
			}
		});

		JMenuItem menuFileChoose = new JMenuItem("Ouverture Fichier Excel");
		menuFileChoose.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				chooseFile();
			}
		});

		JMenu menuFile = new JMenu("File");
		menuFile.add(menuFileChoose);

		JMenuBar menuBar = new JMenuBar();
		menuBar.add(menuFile);
		menuBar.add(menuInfo);

		buttonFileChooserExcel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chooseFile();
			}
		});

		buttonStartImport.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				processFileSimpleExcelFile();
			}
		});
		ActionListener listenerAddCalendar = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				showCreateCalendarDialog();
			}
		};
		ActionListener listenerListCAlendar = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				startFetchGoogleCalendarnfo();
			}
		};ActionListener listenerDeleteCAlendar = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				showDeleteCalendarDialog();
			}
		};
	    panelGoogleInfo = new JPanelGoogleCalendarInfo(listenerListCAlendar,listenerAddCalendar,listenerDeleteCAlendar);

		JPanel panelExcelInfo = new JPanel(new GridLayout(0, 1));
		Border border = BorderFactory.createLineBorder(Color.red);
		panelExcelInfo.setBorder(BorderFactory.createTitledBorder(border, "Import events from file to calendar selected" ));
		
		panelExcelInfo.add(UtilGUI.getPanelLabel(labelFieldFileExcel, buttonFileChooserExcel, "Simple Text Events File \n Each line starting by a Date will be imported\n Format of date must be 31/01/2014 "));
		panelExcelInfo.add(UtilGUI.getPanelLabel(" ", buttonStartImport, "Start import events from  file \n to selected Google Calendar"));
		// panelGoogleInfo.add(getPanelLabel(" ",buttonProcessSimple));
		JPanel panelCenter = new JPanel();
		BoxLayout boxLayout = new BoxLayout(panelCenter, BoxLayout.Y_AXIS);
		panelCenter.setLayout(boxLayout);
		panelCenter.add(new JLabel("Event Calendar Manager  "));
		panelCenter.add(panelGoogleInfo);
		panelCenter.add(panelExcelInfo);

		JPanel panelGlobal = new JPanel(new BorderLayout());
		panelGlobal.add(menuBar, BorderLayout.NORTH);
		panelGlobal.add(panelCenter, BorderLayout.CENTER);
		frame.add(panelGlobal);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setTitle("Management Events Calendar ");
		frame.setVisible(true);

		JButton buttonStop = new JButton(" Stop ");
		buttonStop.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				stop();
			}
		});
		JPanel panelButton = new JPanel(new BorderLayout());
		panelButton.add(buttonStop, BorderLayout.EAST);
		panelButton.add(new JLabel(""), BorderLayout.CENTER);

		JPanel panelGlobalDialog = new JPanel(new BorderLayout());
		scrollPaneTextArea = new JScrollPane(textArea);
		textArea.setEditable(false);
		panelGlobalDialog.add(scrollPaneTextArea, BorderLayout.CENTER);
		panelGlobalDialog.add(panelButton, BorderLayout.SOUTH);
		dialogLog = new JDialog(frame);
		dialogLog.setVisible(false);
		dialogLog.add(panelGlobalDialog);
		dialogLog.pack();
		dialogLog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		this.buttonFileChooserExcel.setEnabled(false);
		this.buttonStartImport.setEnabled(false);

	}

	File fileSelected;

	private void chooseFile() {
		System.out.println("Choose Sed File !!!!!!!!!!!!");
		int returnVal = fileChooser.showOpenDialog(frame);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			fileSelected = fileChooser.getSelectedFile();
			labelFieldFileExcel.setText(fileSelected.getName());
			System.out.println("File exists : " + fileSelected.exists() + "  " + fileSelected.getAbsolutePath());
			properties.setProperty(KEY_DIRECTORY_FILE_CHOOSER, fileSelected.getParentFile().getAbsolutePath());
			saveProperties();

		} else {
			System.out.println("Open command cancelled by user.");
		}

	}

	AbstractProcessFileExcel process;

	private void processFileSimpleExcelFile() {
		log("ProcessFile_SimpleExcelFile A");
		List<CalendarListEntry>  listSelected = this.panelGoogleInfo.getListSelected();
		if (listSelected.isEmpty()){
			JOptionPane.showMessageDialog(this.frame, "No Calendar Selected!");
		}else if(fileSelected == null) {
			JOptionPane.showMessageDialog(this.frame, "Please, Select a File to Import!");
		}else if(!fileSelected.exists()) {
			JOptionPane.showMessageDialog(this.frame, "File doen't exist !!\n"+fileSelected.getAbsolutePath());
		}else {
					dialogLog.setVisible(true);		
			try {
				CalendarBg calendarBg = new CalendarBg();
				calendarBg.setListSelectedCalendar(listSelected);
				process = new ProcessFileSimpleFileText(fileSelected,calendarBg, this);
			} catch (Exception e) {
				log(e);
			}
		}
	}

	public void log(Throwable e) {
		log(e.getClass().getName());
		log(e.getMessage());
		for (StackTraceElement s : e.getStackTrace()) {
			log("   at " + s.getClassName() + "." + s.getMethodName() + "(" + s.getLineNumber() + ")");
		}

	}

	public void log(String s) {
		System.out.println(s);
		// String fullLog = textArea.getText()+s+"\n";
		// textArea.setSelectionStart(0);
		// textArea.setSelectionEnd(0);
		textArea.append(s + "\n");
		textArea.updateUI();
		JScrollBar bar = scrollPaneTextArea.getVerticalScrollBar();
		bar.setValue(bar.getMaximum());
	}

	private void saveProperties() {
		try {
			// properties.setProperty(KEY_GMAIL_LOGIN,
			// this.textFieldLoginGmail.getText());
			FileOutputStream out = new FileOutputStream(fileProperties);
			properties.store(out, "From Bg");
			out.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void stop() {
		log("Stop Request");
		process.stop();
	}

	public void alert(String message) {
		JOptionPane.showMessageDialog(frame, "" + message, "ERROR", JOptionPane.ERROR_MESSAGE);
	}
	
	private void startFetchGoogleCalendarnfo() {
		try {
			panelGoogleInfo.clear();
			System.out.println("startFetchGoogleCalendarnfo ");
			CalendarBg calendarBg = new CalendarBg();
			com.google.api.services.calendar.model.CalendarList  calendarList = calendarBg.getCalendarList();
			int i=0;
			for (CalendarListEntry entry : calendarList.getItems()) {

				JPanelCalendar panelCalendar_ = new JPanelCalendar(entry);
				System.out.println(i +"  "+panelCalendar_.hashCode()+ " calendar_ id :" + entry.getId() + " | description :" + entry.getDescription() + " | summary " + entry.getSummary());
				
				i++;
				panelGoogleInfo.addCalendar(panelCalendar_);
			}
			this.frame.pack();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.buttonFileChooserExcel.setEnabled(true);
		this.buttonStartImport.setEnabled(true);
	}
	
	
	
	private void addCalendar_() {
		try {
			CalendarBg calendarBg = new CalendarBg();
			calendarBg.addCalendar(textFieldCreateCalendarSummary.getText(),textFieldCreateCalendarDescription.getText());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	JTextField textFieldCreateCalendarSummary = new JTextField();
	JTextField textFieldCreateCalendarDescription = new JTextField();
	private boolean showCreateCalendarDialog() {
		System.out.println("Show dialog");
		final JDialog dialog = new JDialog(frame,true);
		dialog.setTitle("Create Calendar");
		JPanel panelDialog = new JPanel(new GridLayout(0,1));
		JButton buttonCreate= new JButton("Create");
		buttonCreate.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				addCalendar_();
				dialog.setVisible(false);
			}
		});
		panelDialog.add(UtilGUI.getPanelLabel("Summary", textFieldCreateCalendarSummary, "Summary of the calendar to create"));
		panelDialog.add(UtilGUI.getPanelLabel("Description", textFieldCreateCalendarDescription, "Description of the calendar to create"));
		panelDialog.add(UtilGUI.getPanelLabel("", buttonCreate, ""));
		dialog.setContentPane(panelDialog);
		dialog.pack();
		dialog.setVisible(true);
		dialog.setDefaultCloseOperation( JDialog.DO_NOTHING_ON_CLOSE);
		return true;
	}
	
	private void showDeleteCalendarDialog() {
		log("ProcessFile_SimpleExcelFile A");
		List<CalendarListEntry>  listSelected = this.panelGoogleInfo.getListSelected();
		if (listSelected.isEmpty()){
			JOptionPane.showMessageDialog(this.frame, "No Calendar Selected!");
		}else {
			int r = JOptionPane.showConfirmDialog(this.frame, "Are you sure you want to delete "+listSelected.size()+" Calendars ?");
			System.out.println("showDeleteCalendarDialog  r:"+r);
			if (r == 0){
				CalendarBg calendarBg = new CalendarBg();
				for (CalendarListEntry c : listSelected){
					System.out.println(" Deleteing "+c.getSummary()+"  "+c.getDescription());
					try {
						calendarBg.deleteCalendar(c.getId());
					} catch (IOException e) {
						
						e.printStackTrace();
					}
				}
				startFetchGoogleCalendarnfo();
			}
		}
		
	}
	

}
