package bg.calendar.manager.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;

import com.google.api.services.calendar.model.CalendarListEntry;

public class JPanelGoogleCalendarInfo extends JPanel {
	private JButton buttonAddCalendar;
	private JButton buttonDeleteSelectedCalendar;
	private final JPanel panelCAlendars = new JPanel(new GridLayout(0,3));
	public JPanelGoogleCalendarInfo(ActionListener listenerListCalendars,ActionListener listenerAddCalendar_, ActionListener listenerDeleteCalendar) {
		this.setLayout(new BorderLayout());
		JButton buttonListCalendars = new JButton("List Calendars");
		buttonAddCalendar = new JButton("Add Calendar");
		buttonDeleteSelectedCalendar = new JButton("Delete Calendars");
		buttonAddCalendar.setEnabled(false);
		buttonDeleteSelectedCalendar.setEnabled(false);
		buttonDeleteSelectedCalendar.addActionListener(listenerDeleteCalendar);
		JPanel panelSouth = new JPanel(new BorderLayout());
		JPanel panelButtons = new JPanel();
		panelButtons.add(buttonAddCalendar);
		panelButtons.add(buttonDeleteSelectedCalendar);
		panelSouth.add(panelButtons,BorderLayout.WEST);
		JPanel panelNorth = new JPanel(new BorderLayout());
		panelNorth.add(buttonListCalendars,BorderLayout.WEST);
		this.add(panelNorth,BorderLayout.NORTH);
		this.add(panelSouth,BorderLayout.SOUTH);
		buttonListCalendars.addActionListener(listenerListCalendars);
		buttonAddCalendar.addActionListener(listenerAddCalendar_);
		this.add(panelCAlendars,BorderLayout.WEST);
	}
	
	public List<CalendarListEntry> getListSelected () {
		Component[] components =   this.panelCAlendars.getComponents();
		List<CalendarListEntry> list = new ArrayList<CalendarListEntry>();
		for(Component c : components){
			if (c instanceof JPanelCalendar){
				JPanelCalendar jpc = (JPanelCalendar) c;
				CalendarListEntry entry = jpc.getEntry();
				if (jpc.isSelected()){
					list.add(entry);
				}
			}
		}
		return list;
	}

	public void addCalendar(JPanelCalendar panelCalendar_) {
		this.panelCAlendars.add(panelCalendar_);
		buttonAddCalendar.setEnabled(true);
		buttonDeleteSelectedCalendar.setEnabled(true);
	}

	public void clear() {
		this.panelCAlendars.removeAll();
	}
	
	
	
}
