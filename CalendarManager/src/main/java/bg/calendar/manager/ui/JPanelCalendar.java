package bg.calendar.manager.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.api.services.calendar.model.CalendarListEntry;

public class JPanelCalendar extends JPanel {

	CalendarListEntry entry;
	

	private static final long serialVersionUID = 1L;
	private JCheckBox checkBox = new JCheckBox(" ");
	
	public JPanelCalendar(CalendarListEntry entry) {
		System.out.println( " calendar_ id :" + entry.getId() + " | description :" + entry.getDescription() + " | summary " + entry.getSummary());
		
		this.entry=entry;
		JLabel labelId = new JLabel(entry.getId());
		labelId.setToolTipText(entry.getId());
		
		String description = entry.getDescription();
		if (description == null){
			description = "";
		}
		JLabel labelDescription = new JLabel(description);
		labelDescription.setToolTipText(description);
		
		JLabel labelSummary = new JLabel(entry.getSummary());
		labelSummary.setToolTipText(entry.getSummary());
		
		UtilGUI.setSize(labelId, new Dimension(80,30));
		
		
		this.setLayout(new GridLayout(0,1));
		this.add(checkBox);
		this.add(labelId);
		this.add(labelDescription);
		this.add(labelSummary);
		
		this.setBorder();
	}

	
	public CalendarListEntry getEntry() {
		return entry;
	}


	private void setBorder(){
		
			this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.blue, 1),"Calendar"));
	
	}
	
	boolean isSelected() {
		return this.checkBox.isSelected();
	}
}
