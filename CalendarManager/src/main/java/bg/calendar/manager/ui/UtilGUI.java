package bg.calendar.manager.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class UtilGUI {

	public static Component getPanelLabel(String label, JComponent component, String help) {
		JLabel label_1 = new JLabel(label);
		return getPanelLabel(label_1, component, help);
	}

	public static Component getPanelLabel(JComponent label_1, JComponent component, final String help) {
		final JPanel panelGlobal = new JPanel(new FlowLayout(FlowLayout.LEFT));

		int w1 = 200;
		int w2 = 200;
		int w3 = 30;
		Dimension dim1 = new Dimension(w1, 30);
		label_1.setSize(dim1);
		label_1.setPreferredSize(dim1);
		JComponent componentEnd = null;
		
		if ((help != null) && (help.length() > 2)) {
			JButton buttonHelp = new JButton("", imageIconHelp);
			buttonHelp.setBorder(BorderFactory.createEmptyBorder());
			buttonHelp.setOpaque(false);

			buttonHelp.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					JOptionPane.showMessageDialog(panelGlobal, help);
				}
			});
			componentEnd = buttonHelp;
			
		}
		if (componentEnd == null){
			componentEnd = new JLabel();
		}
		Dimension dim2 = new Dimension(w3, 30);
		componentEnd.setSize(dim2);
		componentEnd.setPreferredSize(dim2);
		
		Dimension dimCentral = new Dimension(w2, 30);
		component.setSize(dimCentral);
		component.setPreferredSize(dimCentral);
		component.setMaximumSize(dimCentral);

		panelGlobal.add(label_1);
		panelGlobal.add(component);
		panelGlobal.add(componentEnd);
		return panelGlobal;
	}

	public static void setSize(JComponent c, Dimension dimension) {
		c.setPreferredSize(dimension);
		c.setMaximumSize(dimension);
		c.setMinimumSize(dimension);
		c.setSize(dimension);
	}

	private static ImageIcon imageIconHelp = createImageIcon("/help.png", "Help");

	protected static ImageIcon createImageIcon(String path, String description) {
		java.net.URL imgURL = UtilGUI.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

}
