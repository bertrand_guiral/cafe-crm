package bg.calendar.manager;

import java.io.IOException;

import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.Calendar;

public class UtilCalendars {

	private static void showCalendars() throws IOException {

	}

	public static CalendarList  getCalendarList(Calendar client) throws Exception {
		View.header("Show Calendars");
		CalendarList calendarList = client.calendarList().list().execute();
		return calendarList;
	}
}
