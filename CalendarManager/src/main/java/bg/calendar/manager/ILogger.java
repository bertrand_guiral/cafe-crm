package bg.calendar.manager;

public interface ILogger {

	public void log(String s);

	public void log(Throwable e);

	public void alert(String message); 
}
