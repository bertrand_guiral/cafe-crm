package phone.trace.calendar.date;

import java.util.Calendar;

public interface IDateTimeListener {

	void onDateSet(Calendar c);

}
