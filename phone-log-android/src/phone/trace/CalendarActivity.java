package phone.trace;



import phone.trace.R;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public class CalendarActivity extends Activity {

	private String TAG = getClass().getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            Log.i(TAG, "Starting Calendar Test");

            // ListAllCalendarDetails();

            // will return the last found calendar with "Test" in the name
            int iTestCalendarID = listSelectedCalendars();

            // change this when you know which calendar you want to use
            // If you create a new calendar, you may need to manually sync the
            // phone first
            if (iTestCalendarID != 0) {

                Uri newEvent2 = makeNewCalendarEntry2_(iTestCalendarID);
                int eventID2 = Integer.parseInt(newEvent2.getLastPathSegment());
                listCalendarEntry(eventID2);

                Uri newEvent = makeNewCalendarEntry_(iTestCalendarID);
                int eventID = Integer.parseInt(newEvent.getLastPathSegment());
                listCalendarEntry(eventID);
                
                // uncomment these to show updating and deleting entries

                //UpdateCalendarEntry(eventID);
                //ListCalendarEntrySummary(eventID);
                //DeleteCalendarEntry(eventID);
                
                //ListCalendarEntrySummary(eventID);
               // ListAllCalendarEntries(iTestCalendarID);
            } else {
                Log.i(TAG, "No 'Test' calendar found.");
            }

            Log.i(TAG, "Ending Calendar Test");


        } catch (Exception e) {
            Log.e(TAG, "General failure", e);
        }
    }

    private int listSelectedCalendars() {
        int result = 0;
        String[] projection = new String[] { "_id", "name" };
        String selection = "selected=1";
        String path = "calendars";

        Cursor managedCursor = getCalendarManagedCursor(projection, selection,  path);

        if (managedCursor != null && managedCursor.moveToFirst()) {

            Log.i(TAG, "Listing Selected Calendars Only");

            int nameColumn = managedCursor.getColumnIndex("name");
            int idColumn = managedCursor.getColumnIndex("_id");

            do {
                String calName = managedCursor.getString(nameColumn);
                String calId = managedCursor.getString(idColumn);
                Log.i(TAG, "Found Calendar '" + calName + "' (ID=" + calId + ")");
                if (calName != null && calName.contains("Test")) {
                    result = Integer.parseInt(calId);
                }
            } while (managedCursor.moveToNext());
        } else {
            Log.i(TAG, "No Calendars");
        }

        return result;

    }

    private void listAllCalendarDetails() {
        Cursor managedCursor = getCalendarManagedCursor(null, null, "calendars");

        if (managedCursor != null && managedCursor.moveToFirst()) {

            Log.i(TAG, "Listing Calendars with Details");

            do {

                Log.i(TAG, "**START Calendar Description**");

                for (int i = 0; i < managedCursor.getColumnCount(); i++) {
                    Log.i(TAG, managedCursor.getColumnName(i) + "="
                            + managedCursor.getString(i));
                }
                Log.i(TAG, "**END Calendar Description**");
            } while (managedCursor.moveToNext());
        } else {
            Log.i(TAG, "No Calendars");
        }

    }

    private void listAllCalendarEntries(int calID) {

        Cursor managedCursor = getCalendarManagedCursor(null, "calendar_id="+ calID, "events");

        if (managedCursor != null && managedCursor.moveToFirst()) {

            Log.i(TAG, "Listing Calendar Event Details");

            do {

                Log.i(TAG, "**START Calendar Event Description**");

                for (int i = 0; i < managedCursor.getColumnCount(); i++) {
                    Log.i(TAG, managedCursor.getColumnName(i) + "="
                            + managedCursor.getString(i));
                }
                Log.i(TAG, "**END Calendar Event Description**");
            } while (managedCursor.moveToNext());
        } else {
            Log.i(TAG, "No Calendars");
        }

    }

    private void listCalendarEntry(int eventId) {
        Cursor managedCursor = getCalendarManagedCursor(null, null, "events/" + eventId);
    
        if (managedCursor != null && managedCursor.moveToFirst()) {

            Log.i(TAG, "Listing Calendar Event Details");

            do {

                Log.i(TAG, "**START Calendar Event Description**");

                for (int i = 0; i < managedCursor.getColumnCount(); i++) {
                    Log.i(TAG, managedCursor.getColumnName(i) + "="
                            + managedCursor.getString(i));
                }
                Log.i(TAG, "**END Calendar Event Description**");
            } while (managedCursor.moveToNext());
        } else {
            Log.i(TAG, "No Calendar Entry");
        }

    }

    private void listCalendarEntrySummary(int eventId) {
        String[] projection = new String[] { "_id", "title", "dtstart" };
        Cursor managedCursor = getCalendarManagedCursor(projection,
                null, "events/" + eventId);

        if (managedCursor != null && managedCursor.moveToFirst()) {

            Log.i(TAG, "Listing Calendar Event Details");

            do {

                Log.i(TAG, "**START Calendar Event Description**");

                for (int i = 0; i < managedCursor.getColumnCount(); i++) {
                    Log.i(TAG, managedCursor.getColumnName(i) + "="
                            + managedCursor.getString(i));
                }
                Log.i(TAG, "**END Calendar Event Description**");
            } while (managedCursor.moveToNext());
        } else {
            Log.i(TAG, "No Calendar Entry");
        }

    }

    private Uri makeNewCalendarEntry_(int calId) {
        ContentValues event = new ContentValues();

        event.put("calendar_id", calId);
        event.put("title", "Today's Event [TEST]");
        event.put("description", "2 Hour Presentation");
        event.put("eventLocation", "Online");

        long startTime = System.currentTimeMillis() + 1000 * 60 * 60;
        long endTime = System.currentTimeMillis() + 1000 * 60 * 60 * 2;

        event.put("dtstart", startTime);
        event.put("dtend", endTime);

        event.put("allDay", 0); // 0 for false, 1 for true
        event.put("eventStatus", 1);
        event.put("visibility", 0);
        event.put("transparency", 0);
        event.put("hasAlarm", 0); // 0 for false, 1 for true

        Uri eventsUri = Uri.parse(getCalendarUriBase()+"events");

        Uri insertedUri = getContentResolver().insert(eventsUri, event);
        return insertedUri;
    }

    private Uri makeNewCalendarEntry2_(int calId) {
        ContentValues event = new ContentValues();

        event.put("calendar_id", calId);
        event.put("title", "Birthday [TEST]");
        event.put("description", "All Day Event");
        event.put("eventLocation", "Worldwide");

        long startTime = System.currentTimeMillis() + 1000 * 60 * 60 * 24;

        event.put("dtstart", startTime);
        event.put("dtend", startTime);

        event.put("allDay", 1); // 0 for false, 1 for true
        event.put("eventStatus", 1);
        event.put("visibility", 0);
        event.put("transparency", 0);
        event.put("hasAlarm", 0); // 0 for false, 1 for true

        Uri eventsUri = Uri.parse(getCalendarUriBase()+"events");
        Uri insertedUri = getContentResolver().insert(eventsUri, event);
        return insertedUri;

    }

    private int updateCalendarEntry(int entryID) {
        int iNumRowsUpdated = 0;

        ContentValues event = new ContentValues();

        event.put("title", "Changed Event Title");
        event.put("hasAlarm", 1); // 0 for false, 1 for true

        Uri eventsUri = Uri.parse(getCalendarUriBase()+"events");
        Uri eventUri = ContentUris.withAppendedId(eventsUri, entryID);

        iNumRowsUpdated = getContentResolver().update(eventUri, event, null,
                null);

        Log.i(TAG, "Updated " + iNumRowsUpdated + " calendar entry.");

        return iNumRowsUpdated;
    }

    private int deleteCalendarEntry(int entryID) {
        int iNumRowsDeleted = 0;

        Uri eventsUri = Uri.parse(getCalendarUriBase()+"events");
        Uri eventUri = ContentUris.withAppendedId(eventsUri, entryID);
        iNumRowsDeleted = getContentResolver().delete(eventUri, null, null);

        Log.i(TAG, "Deleted " + iNumRowsDeleted + " calendar entry.");

        return iNumRowsDeleted;
    }

    /**
     * @param projection
     * @param selection
     * @param path
     * @return
     */
    private Cursor getCalendarManagedCursor(String[] projection, String selection, String path) {
        
    	Uri calendars = Uri.parse("content://calendar/" + path);

        Cursor managedCursor = null;
        try {
            managedCursor = managedQuery(calendars, projection, selection, null, null);
        } catch (IllegalArgumentException e) {
            Log.w(TAG, "Failed to get provider at [" + calendars.toString() + "]");
        }

        if (managedCursor == null) {
            // try again
            calendars = Uri.parse("content://com.android.calendar/" + path);
            try {
                managedCursor = managedQuery(calendars, projection, selection, null, null);
            } catch (IllegalArgumentException e) {
                Log.w(TAG, "Failed to get provider at ["   + calendars.toString() + "]");
            }
        }
        return managedCursor;
    }

    /*
     * Determines if it's a pre 2.1 or a 2.2 calendar Uri, and returns the Uri
     */
    private String getCalendarUriBase() {
        String calendarUriBase = null;
        Uri calendars = Uri.parse("content://calendar/calendars");
        Cursor managedCursor = null;
        try {
            managedCursor = managedQuery(calendars, null, null, null, null);
        } catch (Exception e) {
            // eat
        }

        if (managedCursor != null) {
            calendarUriBase = "content://calendar/";
        } else {
            calendars = Uri.parse("content://com.android.calendar/calendars");
            try {
                managedCursor = managedQuery(calendars, null, null, null, null);
            } catch (Exception e) {
                // eat
            }

            if (managedCursor != null) {
                calendarUriBase = "content://com.android.calendar/";
            }

        }

        return calendarUriBase;
    }

}
