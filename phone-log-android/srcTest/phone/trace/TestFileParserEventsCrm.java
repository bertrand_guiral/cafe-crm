package phone.trace;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import phone.trace.file.event.FileParserCSVEvents;
import phone.trace.file.event.IParserLigneEvent;
import junit.framework.TestCase;

public class TestFileParserEventsCrm extends TestCase {

	public static SimpleDateFormat sdf_us = new SimpleDateFormat("dd/MM/yyyy",  Locale.US);
	public static SimpleDateFormat sdf_fr = new SimpleDateFormat("dd/MM/yyyy",  Locale.FRANCE);
	
	private static File DIR = new File("filesTest");
	/**
	 * Test method for
	 * {@link phone.trace.UtilCalendar#listEvent(android.content.ContentResolver, long, java.lang.String)}
	 * .
	 */
	public void test0() {
		assertTrue(true); // Test Junit. Should always working!
		assertTrue(DIR.exists());
	}

	public void test1() {
		File file = new File("filesTest/events.csv");
		assertTrue(file.exists());
		IParserLigneEvent parserLine = new IParserLigneEvent() {
			@Override
			public void parseLigne( String line) {
				System.out.println("-------------------------- "+line);
			}

			@Override
			public void parseFileTerminated(File file, int nbLignesParsed, int nbLignesTotal) {
				// TODO Auto-generated method stub
				
			}
		};
		FileParserCSVEvents parser = new FileParserCSVEvents(null, file,parserLine);
		parser.processFile();
		Date date = new Date();
		System.out.println(sdf_fr.format(date));
		System.out.println(sdf_us.format(date));
	}
}
