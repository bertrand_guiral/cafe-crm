package phone.trace;

import java.io.File;

import phone.trace.file.FileParserCSVContacts;
import phone.trace.file.IProcessItem;
import phone.trace.file.FileParserCSVContacts.ContactMap;
import junit.framework.TestCase;

public class TestFileParser extends TestCase {

	private static File DIR = new File("filesTest");
	/**
	 * Test method for
	 * {@link phone.trace.UtilCalendar#listEvent(android.content.ContentResolver, long, java.lang.String)}
	 * .
	 */
	public void test0() {
		assertTrue(true); // Test Junit. Should always working!
		assertTrue(DIR.exists());
	}

	public void test1() {
		assertTrue(true); // Test Junit. Should always working!
		File file = new File(DIR, "testContacts.csv");
		assertTrue(file.exists());
		
		IProcessItem iProcessItem = new IProcessItem(){
			 int i;
			@Override
			public void processItem(FileParserCSVContacts.ContactMap contactMap) {
				String firstname = contactMap.getColumn(FileParserCSVContacts.KEY_FIRST_NAME,0);
				String clientId = contactMap.getColumn(FileParserCSVContacts.KEY_CLIENT_ID,0);
				String name = contactMap.getColumn(FileParserCSVContacts.KEY_NAME,0);
				String email = contactMap.getColumn(FileParserCSVContacts.KEY_MAIL,0);
				String number = contactMap.getColumn(FileParserCSVContacts.KEY_PHONE_NUMBER,0);
				System.out.println(" ------------  firstname : "+firstname+" "+name+" "+number+" "+" "+clientId);
				assertEquals("firstname"+i, firstname);
				assertEquals("clientid_"+i, clientId);
				assertTrue(number.startsWith("06824263"));
				assertTrue(email.indexOf("@")>0);
				assertNotNull(name);
				i++;
			}
			
		};
		FileParserCSVContacts fileParserCSV = new FileParserCSVContacts(null, file,iProcessItem);
		
		fileParserCSV.processFile();
		
		assertEquals((int) 0, (int)fileParserCSV.mapColumns.get(fileParserCSV.KEY_FIRST_NAME).get(0));
		assertEquals((int) 1, (int)fileParserCSV.mapColumns.get(fileParserCSV.KEY_NAME).get(0));
		assertEquals((int) 2, (int)fileParserCSV.mapColumns.get(fileParserCSV.KEY_MAIL).get(0));
		assertEquals((int) 3, (int)fileParserCSV.mapColumns.get(fileParserCSV.KEY_PHONE_NUMBER).get(0));
		assertEquals((int) 4, (int)fileParserCSV.mapColumns.get(fileParserCSV.KEY_CLIENT_ID).get(0));
		
	}
}
