/**
 * 
 */
package phone.trace;

import phone.trace.BgCalendar;
import phone.trace.UtilCalendar;
import phone.trace.model.AppAccount;
import phone.trace.model.Contact;
import phone.trace.model.Event;
import phone.trace.model.PhoneCall;
import junit.framework.TestCase;

/**
 * @author Bertrand
 *
 */
public class TestUtilCalendar extends TestCase {

	private BgCalendar bgCalendar = new BgCalendar("test", "test", "test", 12345, "test");
	/**
	 * Test method for {@link phone.trace.UtilCalendar#listEvent(android.content.ContentResolver, long, java.lang.String)}.
	 */
	public void test0() {
			assertTrue(true); // Test Junit. Should always working!
	}


	public void test1() {
		long date = 10000;
		Event event = UtilCalendar.parsePhoneCall("xxxx","dddd", date, date+10, 0,bgCalendar);
		assertNull(event);
	}
	public void test2() {
		int type = Event.TYPE_OUTGOING_CALL;
		long date = 1000;
		int duration = 12345;
		String comment ="A comment";
		String number ="012345678";
		String name="TOTO";
		Contact contact = new Contact();
		contact.setNumber(number);
		contact.setName(name);
		String email ="zzzz";
		AppAccount appAccount = new AppAccount();
		appAccount.setMail(email);
		PhoneCall phoneCall_0 = new PhoneCall();
		phoneCall_0.setContact(contact);
		phoneCall_0.setDate(date);
		phoneCall_0.setDuration_ms(duration);
		phoneCall_0.setType(type);
		phoneCall_0.setComment(comment);
		phoneCall_0.setAccount(appAccount);
		String title = UtilCalendar.toTitleFromPhoneCall(phoneCall_0);
		
		Event event = UtilCalendar.parsePhoneCall(title,comment, date, date+duration, 0,bgCalendar);
		
		assertTrue(event instanceof PhoneCall);
		PhoneCall phoneCall_2 = (PhoneCall) event;
		
		assertEquals(phoneCall_0.getDate() ,phoneCall_2.getDate());
		assertEquals(phoneCall_0.getDuration_ms() ,phoneCall_2.getDuration_ms());
		assertEquals(phoneCall_0.getComment() ,phoneCall_2.getComment());
		assertEquals(phoneCall_0.getContact().getNumber() ,phoneCall_2.getContact().getNumber());
		assertEquals(phoneCall_0.getContact().getName() ,phoneCall_2.getContact().getName());
		assertEquals(phoneCall_0.getType() ,phoneCall_2.getType());
		assertEquals(phoneCall_0.getAccount().getMail() ,phoneCall_2.getAccount().getMail());
	}
	
	public void testCRM() {
		// CRM 4201964 633114 007 BEOUSSE 47 RUE DE L ECOLE 47550 BOE "103 32" || dStart 1391122800000 dEnd 1391122801000 descrip
		String title = "CRM 4201964 633114 007 BEOUSSE 47 RUE DE L ECOLE 47550 BOE ";
		String comment = "";
		long date = System.currentTimeMillis();
		long duration = 20;
		Event event = UtilCalendar.parsePhoneCall(title,comment, date, date+duration, 0,bgCalendar);
		System.out.println(" event "+event);
		assertNotNull(event);
	}
	
}
