package bg.generate;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class PhoneCall {

	public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private Date date ;
	private String email  ;
	private String phoneUser ;
	private int type;
	private String phoneNumber;
	private String  contactName;
	private  String comment;
	private Long duration;
	
	PhoneCall() {
		
	}
	
	public PhoneCall(String dateStr, String email, String phoneUser, int type,
			String phoneNumber, String contactName, String comment,
			Long duration) throws ParseException {
		this(simpleDateFormat.parse(dateStr),email,phoneUser,type,phoneNumber,contactName,comment,duration);
	}
	

	public PhoneCall(Date date, String email, String phoneUser, int type,
			String phoneNumber, String contactName, String comment,
			Long duration) {
		super();
		this.date = date;
		this.email = email;
		this.phoneUser = phoneUser;
		this.type = type;
		this.phoneNumber = phoneNumber;
		this.contactName = contactName;
		this.comment = comment;
		this.duration = duration;
	}

	//@FormParam("emailUser") String emailUser, @FormParam("phonecall") String phonecall, @FormParam("contactName") String contactName, @FormParam("cryptPhrase") String  cryptPhrase) throws Exception {
	public String getUrlParameterAlert() {
		String r="";
		r+="date="+URLEncoder.encode(simpleDateFormat.format(this.date));
		r+="&emailUser="+URLEncoder.encode(email);
		r+="&phoneUser="+phoneUser;
		r+="&type="+type;
		r+="&phonecall="+phoneNumber;
		r+="&contactName="+URLEncoder.encode(contactName);
		r+="&comment="+URLEncoder.encode(comment);
		r+="&duration="+duration;
		r+="&cryptPhrase="+URLEncoder.encode("phone-log");
		return r;
	}	
	
	public String getUrlParameter() {
		String r="";
		r+="date="+URLEncoder.encode(simpleDateFormat.format(this.date));
		r+="&emailUser="+URLEncoder.encode(email);
		r+="&phoneUser="+phoneUser;
		r+="&type="+type;
		r+="&phoneNumber="+phoneNumber;
		r+="&contactName="+URLEncoder.encode(contactName);
		r+="&comment="+URLEncoder.encode(comment);
		r+="&duration="+duration;
		r+="&cryptPhrase="+URLEncoder.encode("phone-log");
		return r;
	}
	
	

}
