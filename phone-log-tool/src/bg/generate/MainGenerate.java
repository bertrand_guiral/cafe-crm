package bg.generate;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainGenerate {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
	 System.out.println(	PhoneCall.simpleDateFormat.format(new Date()));
	 String email = "demo@phone-log.com";
	 String phoneUser = "0612345678";
	 List<PhoneCall> list = new ArrayList<>();
	 list.add(new PhoneCall("2013-07-11 17:23:25", email, phoneUser, 0, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-11 17:22:25", email, phoneUser, 1, "0682426362", "Georges", "", 20L));
	 list.add(new PhoneCall("2013-07-11 17:21:25", email, phoneUser, 2, "0682426363", "Lucette", "", 21L));
	 list.add(new PhoneCall("2013-07-11 17:20:25", email, phoneUser, 0, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-11 17:19:25", email, phoneUser, 1, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-11 17:18:25", email, phoneUser, 2, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-11 17:17:25", email, phoneUser, 0, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-11 17:16:25", email, phoneUser, 0, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-10 17:15:25", email, phoneUser, 2, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-09 17:28:25", email, phoneUser, 2, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-08 17:28:25", email, phoneUser, 1, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-07 17:28:25", email, phoneUser, 2, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-06 17:28:25", email, phoneUser, 1, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-05 17:28:25", email, phoneUser, 1, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-04 17:28:25", email, phoneUser, 0, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-03 17:28:25", email, phoneUser, 1, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-02 17:28:25", email, phoneUser, 1, "0682426361", "bg", "", 2L));
	 list.add(new PhoneCall("2013-07-01 17:28:25", email, phoneUser, 0, "0682426361", "bg", "", 2L));
	 send(list)	;
	}

	private static void send(List<PhoneCall> list) throws Exception {
		for(PhoneCall pc : list){
			send(pc);
		}
		alert(list.get(0));
	}

	private static int alert(PhoneCall phoneCall) throws Exception {
		URL url = new URL("https://phone-log.appspot.com/r/phonecall/alert");
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		String urlParameters =phoneCall.getUrlParameterAlert();
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		int responseCode = con.getResponseCode();
		BufferedReader in = new BufferedReader(  new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println("alert "+responseCode +"     --- "+response);
		return responseCode;
	}

	private static int send(PhoneCall pc) throws Exception {
		URL url = new URL("https://phone-log.appspot.com/r/phonecall/generateTest");
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		String urlParameters =pc.getUrlParameter();
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		int responseCode = con.getResponseCode();
		BufferedReader in = new BufferedReader(  new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println("send "+responseCode +"     --- "+response);
		return responseCode;
	}
	
	

}
