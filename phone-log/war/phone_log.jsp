<!DOCTYPE html>
<html lang="en">

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="utf-8"%>

<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bg"  uri="http://bg" %>

<jsp:useBean id="href_logout" class="java.lang.String" scope="request" />
<jsp:useBean id="util" class="bg.phonelog.server.call.BeanUtil"	scope="request" />
<jsp:useBean id="user" class="bg.phonelog.server.user.User_SS2"	scope="request" />

<c:if test="${user.displayPhoneLog}">
		
 <script type="text/javascript" language="javascript" src="phone_log/phone_log.nocache.js"></script>
</c:if>
<head>
<meta charset="utf-8">
<title>Phone-Log</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="phone-log">
<meta name="author" content="bertrand">
<!-- Le styles -->
<link href="/assets/css/bootstrap.css" rel="stylesheet">
<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="/assets/css/docs.css" rel="stylesheet">
<link href="/assets/js/google-code-prettify/prettify.css"
	rel="stylesheet">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

<!-- Le fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="assets/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="assets/ico/favicon.png">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45278285-1', 'phone-log.appspot.com');
  ga('send', 'pageview');

</script>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar">

	<!-- Navbar
    ================================================== -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<%@include file="include_navbar.jsp" %>
	</div>


	<!-- Subhead ================================================== -->
	<div class="container" id="overview">
		<div class="hero-unit">
		<div>
			
			<c:if test="${user.displayPaypal}">
			 <table>
			 	<tr>
			 	<td>  </td>
			 	<td>
			 		<form action="${util.urlPaypal}" method="post" target="_top">
			 			<input type="hidden" name="cmd" value="_xclick-subscriptions">
						<input type="hidden" name="currency_code" value="EUR">
						<input type="hidden" name="item_name" value="Phone-Log Subscription ">
						<input type="hidden" name="no_shipping"  value="1">
						<input type="hidden" name="return" value="https://phone-log.appspot.com/controller?action=displayJournal">
						<input type="hidden" name="cancel_return" value="https://phone-log.appspot.com/controller?action=displayJournal">
						<input type="hidden" name="option_selection1"  value="emailUser">
						<input type="hidden" name="item_number" value="Phone-Log emailUser">
						<input type="hidden" name="custom" value="${util.email}">
						<input type="hidden" name="transaction_subject" value="Subscribtion ${util.email}">
						<input type="hidden" name="business" value="${util.business}">
						<input type="hidden" name="a3" value="2.00">
						<input type="hidden" name="p3" value="1">
						<input type="hidden" name="t3" value="M">
						<input type="hidden" name="src" value="1">
						<input type="hidden" name="sra" value="1">
						
						<input type="hidden" name="on0" value="phone-log ">
						<input type="hidden" name="os0" value="${util.email}">
							
						<input type="image" src="https://www.paypal.com/fr_FR/FR/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt=" PayPal ">
						<img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1">
				</form>
				
				</td>
			 	</tr>
			 </table>
				
			</c:if>
		
		</div>
		<!-- 
				user.displayPhoneLog : ${user.displayPhoneLog}<br/>
				user.abonnementOK : ${user.abonnementOK}<br/>
				user.dateAbonnementOK : ${user.dateAbonnementOK}<br/>
				user.date ${user.date}<br/>
		-->
		
			<div id="crypto"></div>
			<span id="alert"></span>
		
		</div>
	</div>

	<div class="container">

		<!-- Docs nav
    ================================================== -->
		<div class="row">
			<div class="span12">
				<div class="row">
					<div class="span3">
						<ul class="nav nav-list bs-docs-sidenav">
							<li>${util.log}</li>
							
						</ul>
					</div>
					
				</div>

				<section id="calls">
					<table align="center">
				      <tr>
				        <td id="alert2"></td>
				       </tr><tr>
				        <td id="comment"> </td>				    
				        </tr><tr>
				        <td id="journal"></td>				    
				      </tr>
				      <tr>
				        <td colspan="2" style="color:red;" id="errorLabelContainer"></td>
				      </tr>
				    </table>
				</section>

				


				<section id="logs">
					<div class="page-header">
						
					</div>
				</section>
	
				</div>
				</div>
				
				

				<!-- Footer
    ================================================== -->
				<footer class="footer">
					<div class="container">
						<p>Designed by BG</p>
					</div>
				</footer>

      </div>

				<!-- Le javascript
    ================================================== -->
				<!-- Placed at the end of the document so the pages load faster -->
				<script type="text/javascript"
					src="http://platform.twitter.com/widgets.js"></script>
				<script src="assets/js/jquery.js"></script>
				<script src="assets/js/bootstrap-transition.js"></script>
				<script src="assets/js/bootstrap-alert.js"></script>
				<script src="assets/js/bootstrap-modal.js"></script>
				<script src="assets/js/bootstrap-dropdown.js"></script>
				<script src="assets/js/bootstrap-scrollspy.js"></script>
				<script src="assets/js/bootstrap-tab.js"></script>
				<script src="assets/js/bootstrap-tooltip.js"></script>
				<script src="assets/js/bootstrap-popover.js"></script>
				<script src="assets/js/bootstrap-button.js"></script>
				<script src="assets/js/bootstrap-collapse.js"></script>
				<script src="assets/js/bootstrap-carousel.js"></script>
				<script src="assets/js/bootstrap-typeahead.js"></script>
				<script src="assets/js/bootstrap-affix.js"></script>

				<script src="assets/js/holder/holder.js"></script>
				<script src="assets/js/google-code-prettify/prettify.js"></script>

				<script src="assets/js/application.js"></script>


				
</body>
</html>
