<!DOCTYPE html>
<html lang="en">

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="utf-8"%>

<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bg"  uri="http://bg" %>

<jsp:useBean id="href_logout" class="java.lang.String" scope="request" />
<jsp:useBean id="phoneCall" class="bg.phonelog.server.call.BeanPhoneCallEntityCrypted2" scope="request" />
<jsp:useBean id="util" class="bg.phonelog.server.call.BeanUtil"	scope="request" />
<jsp:useBean id="listCalls" class="bg.phonelog.server.call.ListCalls"	scope="request" />


<head>
<meta charset="utf-8">
<title>Phone-Log</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="phone-log">
<meta name="author" content="bertrand">

<!-- Le styles -->
<link href="/assets/css/bootstrap.css" rel="stylesheet">
<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="/assets/css/docs.css" rel="stylesheet">
<link href="/assets/js/google-code-prettify/prettify.css"
	rel="stylesheet">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

<!-- Le fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="assets/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="assets/ico/favicon.png">

<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push([ '_setAccount', 'UA-146052-10' ]);
	_gaq.push([ '_trackPageview' ]);
	(function() {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl'
				: 'http://www')
				+ '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();
</script>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar">

	<!-- Navbar
    ================================================== -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<%@include file="include_navbar.jsp" %>
	</div>


	<!-- Subhead ================================================== -->
	<div class="container" id="overview">
		<div class="hero-unit">
		
		<c:if test="${not empty alert.phonecall}">
			 Alert : ${alert.phonecall} 
			 ${alert.contactName} 
			 ${alert.age} ago
			
			<fmt:formatDate value="${alert.date}" pattern="HH:mm:ss MM/dd/yyyy"/> 
			
		
			 <a class="btn" href="https://mail.google.com/mail/u/0/?hl=fr&zx=oam611o9l1zh&shva=1#search/${alert.search}">Search In Mail</a> 
			
			 <a class="btn" href="https://mail.google.com/mail/u/0/?hl=fr&zx=oam611o9l1zh&shva=1#contacts/search/${alert.search}">Search In Contacts gmail</a>
		    
		</c:if>
		</div>
	</div>

	<div class="container">

		<!-- Docs nav
    ================================================== -->
		<div class="row">
			<div class="span3 bs-docs-sidebar">
				<div class="row">
					<div class="span3">
						<ul class="nav nav-list bs-docs-sidenav">
							
							<li>${util.log}</li>
						</ul>
					</div>
					
				</div>
			</div>

			<div class="span9">




				<!-- Télécharger
        ================================================== -->

				
				
				<section id="displayCall">
				
					<c:if test="${empty phoneCall.phoneNumber}">
						No Phone Call Selected
					</c:if>
					<c:if test="${not empty phoneCall.phoneNumber}">
						<div class="page-header">
							${call.phoneNumber}
				 			${phoneCall.contactName} 
				 			${phoneCall.age}  ago 
				 			<br/>
							<fmt:formatDate value="${phoneCall.date}" pattern="HH:mm:ss MM/dd/yyyy"/>
						</div>
					
						<div>
							<form action="/controller" method="post">
								<input type="hidden" name="action" value="addComment">
								<input type="hidden" name="id" value="${phoneCall.id}">
								<input type="hidden" name="phoneNumber" value="${phoneCall.phoneNumber}">
								<input type="hidden" name="contactName" value="${phoneCall.contactName}">
								<input type="hidden" name="action" value="${phoneCall.email}">
								<textarea rows="2" cols="20" name="comment">${phoneCall.comment}</textarea>
								<div>
									<input type="submit">
								</div>
							</form>
						</div>
						
					</c:if>
				</section>

				<section id="calls">
					List Calls  ${util.indexTableau}
					<div class="page-header">
					<table border=1>
						<tr>
							<th>Number</th>
							<th>Contact Name</th>
							<th>Date</th>
							<th></th>
							<th>Duration</th>
							<th>Phone</th>
							<th>Type</th>
							<th>Log</th>
							<th></th>
							<th></th>
						</tr>
						<c:forEach var="call"  items="${listCalls}">
							<tr>
								<td> <bg:displayPhoneNumber value="${call.phoneNumber}"/></td>
								<td> <c:out value="${call.contactName}"/></td>
								<td> <fmt:formatDate value="${call.date}" pattern="HH:mm:ss  dd/MM/yyyy"/> </td>
								<td> <c:out value="${call.age}"/> </td>
								<td> <c:out value="${call.duration}"/> s</td>
								<td> <c:out value="${call.phoneUser}"/></td>
								<td>   <bg:displayCallType type="${call.type}"/> </td>
								<td><c:out value="${call.comment}"/> </td>
								
								<td> <a href="/controller?action=deletePhoneCall&id=<c:out value="${call.id}"/>&index=<c:out value="${util.indexTableau}"/>"> del</a></td>
								<td> <a href="/controller?action=displayPhoneCall&id=<c:out value="${call.id}"/>&index=<c:out value="${util.indexTableau}"/>"> display</a></td>
							</tr>
						</c:forEach>
					 </table>
					 
					 <a href="/controller?action=displayFromIndex&index=${util.indexTableau - util.maxResult}" > Previous</a> -
					 <a href="/controller?action=displayFromIndex&index=${fn:length(listCalls)+util.indexTableau}" > Next</a>
					</div>
				
				</section>

				


				<section id="logs">
					<div class="page-header">
						
					</div>
				</section>
	
				</div>
				</div>
				
				

				<!-- Footer
    ================================================== -->
				<footer class="footer">
					<div class="container">
						<p>Designed by BG</p>
					</div>
				</footer>

      </div>

				<!-- Le javascript
    ================================================== -->
				<!-- Placed at the end of the document so the pages load faster -->
				<script type="text/javascript"
					src="http://platform.twitter.com/widgets.js"></script>
				<script src="assets/js/jquery.js"></script>
				<script src="assets/js/bootstrap-transition.js"></script>
				<script src="assets/js/bootstrap-alert.js"></script>
				<script src="assets/js/bootstrap-modal.js"></script>
				<script src="assets/js/bootstrap-dropdown.js"></script>
				<script src="assets/js/bootstrap-scrollspy.js"></script>
				<script src="assets/js/bootstrap-tab.js"></script>
				<script src="assets/js/bootstrap-tooltip.js"></script>
				<script src="assets/js/bootstrap-popover.js"></script>
				<script src="assets/js/bootstrap-button.js"></script>
				<script src="assets/js/bootstrap-collapse.js"></script>
				<script src="assets/js/bootstrap-carousel.js"></script>
				<script src="assets/js/bootstrap-typeahead.js"></script>
				<script src="assets/js/bootstrap-affix.js"></script>

				<script src="assets/js/holder/holder.js"></script>
				<script src="assets/js/google-code-prettify/prettify.js"></script>

				<script src="assets/js/application.js"></script>


				<!-- Analytics
    ================================================== -->
				<script>
					var _gauges = _gauges || [];
					(function() {
						var t = document.createElement('script');
						t.type = 'text/javascript';
						t.async = true;
						t.id = 'gauges-tracker';
						t.setAttribute('data-site-id',
								'4f0dc9fef5a1f55508000013');
						t.src = '//secure.gaug.es/track.js';
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(t, s);
					})();
				</script>
</body>
</html>
