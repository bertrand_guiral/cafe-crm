<!DOCTYPE html>
<html lang="en">

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>


<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="util" class="bg.phonelog.server.call.BeanUtil"	scope="request" />



<head>
<meta charset="utf-8">
<title>Phone-Log</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="phone-log">
<meta name="author" content="bertrand">

<!-- Le styles -->
<link href="/assets/css/bootstrap.css" rel="stylesheet">
<link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="/assets/css/docs.css" rel="stylesheet">
<link href="/assets/js/google-code-prettify/prettify.css"
	rel="stylesheet">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

<!-- Le fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="assets/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="assets/ico/favicon.png">

<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push([ '_setAccount', 'UA-146052-10' ]);
	_gaq.push([ '_trackPageview' ]);
	(function() {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl'
				: 'http://www')
				+ '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();
</script>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar">

	<!-- Navbar
    ================================================== -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<c:if test="${connected}">
			<%@include file="include_navbar.jsp" %>
		</c:if>
		<c:if test="${!connected}">
			<%@include file="include_navbar_no_connected.jsp" %>
		</c:if>
		
		
		

	</div>


	<!-- Subhead ================================================== -->
	

	<div class="container">

		<!-- Docs nav
    ================================================== -->
		<div class="row">
			<div class="span3 bs-docs-sidebar">
				<div class="row">
					<div class="span3">
						<ul class="nav nav-list bs-docs-sidenav">
							
						</ul>
					</div>
					<div class="span3">
						
					</div>
				</div>
			</div>

			<div class="span6">
				<section id="post">	
					<h2><span style="color: red"> ${util.log} </span>	</h2>			
									
					<div class="page-header">
						<form action="/login" method="post">
						<input type="hidden" name="action" value="login">
						<h2>Login</h2>
						<table>
						<tr> <td>email</td> <td> <input type="text" name="email" value="${util.email}"> </td></tr>
						<tr> <td>password</td> <td> <input type="password" name="password" value=""> </td></tr>
						<tr>  <td> </td><td> <input type="submit" name="submit" value="login"> </td></tr>
						</table>
						</form>
				   </div>
				   <a href="/login?action=registryReRequest">registry</a>
				   <br/>
				   <a href="/login?action=passwordForgottenRequest">Password forgotten</a>
				</section>


				
		</div>
</div>

				<!-- Footer
    ================================================== -->
				<footer class="footer">
					<div class="container">
						<p>Designed by BG</p>
					</div>
				</footer>

</div>

				<!-- Le javascript
    ================================================== -->
				<!-- Placed at the end of the document so the pages load faster -->
				<script type="text/javascript"
					src="http://platform.twitter.com/widgets.js"></script>
				<script src="assets/js/jquery.js"></script>
				<script src="assets/js/bootstrap-transition.js"></script>
				<script src="assets/js/bootstrap-alert.js"></script>
				<script src="assets/js/bootstrap-modal.js"></script>
				<script src="assets/js/bootstrap-dropdown.js"></script>
				<script src="assets/js/bootstrap-scrollspy.js"></script>
				<script src="assets/js/bootstrap-tab.js"></script>
				<script src="assets/js/bootstrap-tooltip.js"></script>
				<script src="assets/js/bootstrap-popover.js"></script>
				<script src="assets/js/bootstrap-button.js"></script>
				<script src="assets/js/bootstrap-collapse.js"></script>
				<script src="assets/js/bootstrap-carousel.js"></script>
				<script src="assets/js/bootstrap-typeahead.js"></script>
				<script src="assets/js/bootstrap-affix.js"></script>

				<script src="assets/js/holder/holder.js"></script>
				<script src="assets/js/google-code-prettify/prettify.js"></script>

				<script src="assets/js/application.js"></script>


				<!-- Analytics
    ================================================== -->
				<script>
					var _gauges = _gauges || [];
					(function() {
						var t = document.createElement('script');
						t.type = 'text/javascript';
						t.async = true;
						t.id = 'gauges-tracker';
						t.setAttribute('data-site-id',
								'4f0dc9fef5a1f55508000013');
						t.src = '//secure.gaug.es/track.js';
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(t, s);
					})();
				</script>
</body>
</html>
