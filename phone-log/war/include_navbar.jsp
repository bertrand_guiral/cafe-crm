<div class="navbar-inner">
           
	<div class="container">
		<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			<span class="icon-bar"><img src="images/logo_phone_log.png"/></span> 
			<span class="icon-bar"></span> 
			<span class="icon-bar"></span>
		</button>
		
		<ul class="nav">
			<li class="active"><a href="/controller?action=displayHome">Home</a></li>
			<li class="active"><a href="/controller?action=displayJournal">Phones Calls</a></li>
			<li class="active"><a href="/controller?action=displayContact">Contact</a></li>
			<li><a class="brand" href="https://phone-log.appspot.com/controller?action=displayJournal">Phone-Log</a></li>
		</ul>


		<div class="pull-right">
		  <ul class="nav">
			<li><a href=""> ${util.email}</a></li>
			<li><a href="/controller?action=displayParams"> Change password </a> </li>
			<li><a href="/controller?action=displayManagementDelegates"> Manage delegates </a> </li>
			<li><a href="${href_logout}"> logout </a> </li>
		  </ul>
		</div>
	</div>
</div>
		
