package bg.crypt;

import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.ShortBlob;

public class StringEncrypter {
	Cipher ecipher;
	boolean hasPassPhrase = true;

	public StringEncrypter(String passPhrase) {

		init(passPhrase);

	}

	public void init(String passPhrase) {
		if (passPhrase == null) {
			hasPassPhrase = false;
		} else if (passPhrase.trim().length() == 0) {
			hasPassPhrase = false;
		}
		if (hasPassPhrase && (ecipher != null) ) {
			// Iteration count
			int iterationCount = BgUtilCypher.iterationCount;

			try {

				KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), BgUtilCypher.salt, iterationCount);
				SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

				ecipher = Cipher.getInstance(key.getAlgorithm());

				// Prepare the parameters to the cipthers
				AlgorithmParameterSpec paramSpec = new PBEParameterSpec(BgUtilCypher.salt, iterationCount);

				ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);

			} catch (Exception e) {
				System.out.println("EXCEPTION: InvalidAlgorithmParameterException");
			}
		}
	}

	public byte[] encrypt(String str) throws Exception {
		if (str == null) {
			return null;
		}

		if (hasPassPhrase) {
			// Encode the string into bytes using utf-8
			byte[] utf8 = str.getBytes("UTF8");

			// Encrypt
			byte[] enc = ecipher.doFinal(utf8);

			// Encode bytes to base64 to get a string
			return enc;
		} else {
			return str.getBytes("UTF8");
		}

	}

	public Blob encryptBlob(String str) throws Exception {
		if (str == null) {
			return null;
		}

		byte[] b = encrypt(str);
		Blob blob = new Blob(b);
		return blob;
	}

	public ShortBlob encryptShortBlob(String str) throws Exception {
		if (str == null) {
			return null;
		}
		byte[] b = encrypt(str);
		ShortBlob blob = new ShortBlob(b);
		return blob;
	}

}
