package bg.crypt;

import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.ShortBlob;

	


	public class StringDeEncrypter {
		
		Cipher dcipher;
	
		private static final Logger log = Logger.getLogger(StringDeEncrypter.class.getName());


		public StringDeEncrypter(String passPhrase) {
			// Iteration count
		    int iterationCount = BgUtilCypher.iterationCount;

		    try {

		        KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), BgUtilCypher.salt, iterationCount);
		        SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

		        dcipher = Cipher.getInstance(key.getAlgorithm());

		        // Prepare the parameters to the cipthers
		        AlgorithmParameterSpec paramSpec = new PBEParameterSpec(BgUtilCypher.salt, iterationCount);

		        dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

		    } catch (Exception e) {
		        log.log(Level.WARNING,"Exception ",e);
		    }
		}


		

		public String decrypt(byte[] dec) throws Exception {
				// Decrypt
				if(dec==null){
					return null;
				}
			
		        byte[] utf8 = dcipher.doFinal(dec);
		        // Decode using utf-8
		        return new String(utf8, "UTF8");
		}


		public String decrypt(ShortBlob bPhoneNumber) throws Exception{
			return decrypt(bPhoneNumber.getBytes());
		}


		public String decrypt(Blob bComment) throws Exception {
			if(bComment == null){
				return "";
			}
			return decrypt(bComment.getBytes());
		}
}
