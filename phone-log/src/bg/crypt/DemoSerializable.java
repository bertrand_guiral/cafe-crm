package bg.crypt;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.google.gwt.dev.util.collect.HashMap;

public class DemoSerializable {

	
	public static void main(String[] a)throws Exception{
		HashMap<String, String> h = new HashMap<String, String>();
		h.put("a", "aa");
		byte[] bArray = toByte(h);
		HashMap  h2 = toMap(bArray);
		System.out.println("result "+h2.equals(h));
	}
	
	public static byte[] toByte(HashMap<? , ?> h) throws Exception{
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ObjectOutputStream oOutputStream = new ObjectOutputStream(byteArrayOutputStream);
		oOutputStream.writeObject(h);
		oOutputStream.close();
		byte[] bArray =  byteArrayOutputStream.toByteArray();
		return bArray;
	}
	
	
	public static HashMap toMap(byte[] b) throws Exception{
		
		ByteArrayInputStream bArrayInputStream = new ByteArrayInputStream(b);
		ObjectInputStream objectInputStream = new ObjectInputStream(bArrayInputStream);
		Object o = objectInputStream.readObject();
		HashMap h = (HashMap) o;
		return h;
	}
	
	
}
