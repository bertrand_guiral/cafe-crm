package bg.phonelog.client.delegates;

/*
 * Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */


import java.util.Date;
import java.util.List;

import bg.phonelog.client.UtilPhoneLog;
import bg.phonelog.client.crypt.Crypto;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.HasData;

/**
 * The data source for contact information used in the sample.
 */
public class DelegatesDataBase {


 
  /**
   * The singleton instance of the database.
   */
  private static DelegatesDataBase instance;
  


  /**
   * Get the singleton instance of the contact database.
   *
   * @return the singleton instance
   */
  public static DelegatesDataBase getInstance() {
    if (instance == null) {
      instance = new DelegatesDataBase();
    }
    return instance;
  }

  /**
   * The provider that holds the list of contacts in the database.
   */
  private DelegatesDataProvider listDataProvider = DelegatesDataProvider.getInstance();

 
  /**
   * Construct a new contact database.
   */
  private DelegatesDataBase() {
  	 
 }

  /**
   * Add a display to the database. The current range of interest of the display
   * will be populated with data.
   *
   * @param display a {@Link HasData}.
   */
  public void addDataDisplay(HasData<Delegate> display) {
    listDataProvider.addDataDisplay(display);
  }

 

  public DelegatesDataProvider getDataProvider() {
    return listDataProvider;
  }


  public void deleteDegates(final List<Delegate> list) {
	//this.listDataProvider.getList().removeAll(list);
	this.listDataProvider.removeAll_Bg(list);
	
	AsyncCallback<Void>  acb = new AsyncCallback<Void>() {

		@Override
		public void onFailure(Throwable e) {
			Window.alert("failure deleting delegates 321 : "+e.getMessage());
		}

		@Override
		public void onSuccess(Void result) {
			Window.alert("Succes  deleting delegates "+list.size());
		}
	};
	UtilPhoneLog.greetingService.deleteDelegates(list,acb );
	
}



public void refreshList() {
	if (this.getDataProvider().getList().size() == 0){
		this.getDataProvider().fetchData();
	}
	
}

public void refreshList(Date upToDate){
	String pass = Crypto.getText();
	UtilPhoneLog.greetingService.getListDelegates( new AsyncCallback<List<Delegate>>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Refresh :failure 789 "+caught.getMessage());
		}

		@Override
		public void onSuccess(List<Delegate> result) {
			Window.alert("Refresh list delegates : result.size"+result.size());
			insertUpDate(result);
		}
	});
}
  
private void insertUpDate(List<Delegate> result) {
	this.getDataProvider().getList().addAll(0, result);
	ListDelegateUI.getInstance() .refresh();
}

public boolean contains(Delegate email){
	return this.listDataProvider.getList().contains(email);
}

public void add(Delegate delegate) {
	this.listDataProvider.add(delegate);
} 

}