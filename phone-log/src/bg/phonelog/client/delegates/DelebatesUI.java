package bg.phonelog.client.delegates;

import bg.phonelog.client.UtilPhoneLog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class DelebatesUI extends Composite {

	private static DelebatesUI instance;

	private static DelebatesUIUiBinder uiBinder = GWT.create(DelebatesUIUiBinder.class);

	interface DelebatesUIUiBinder extends UiBinder<Widget, DelebatesUI> {
	}

	public DelebatesUI() {
		initWidget(uiBinder.createAndBindUi(this));
		this.panel1.add(ListDelegateUI.getInstance());
	}

	@UiField
	TextBox textBoxAdress;

	@UiField
	Button buttonAdd;

	@UiField
	VerticalPanel panel1;

	@UiHandler("buttonAdd")
	void onClick(ClickEvent e) {
		String emails = textBoxAdress.getText();

		String[] s = emails.split("[ ;,]");
		for (String email : s) {
			if (email.length() > 0) {
				addSimpleEmail(email);
			}
		}
		InfosFacture.getInstance().refreshAmont();
	}

	private void addSimpleEmail(final String email) {

		if (isEmailAdress(email)) {
			Delegate delegate = new Delegate(email,null);
			if (DelegatesDataBase.getInstance().contains(delegate)) {
				Window.alert(" " + email + " is already in list ");
			} else {
				DelegatesDataBase.getInstance().add(delegate);
				DelegatesDataBase.getInstance().refreshList();
				UtilPhoneLog.greetingService.addEmailDelegate(email, new AsyncCallback<Void>() {

					@Override
					public void onSuccess(Void result) {

					}

					@Override
					public void onFailure(Throwable e) {
						Window.alert("Exception 872 addEmail failure " + e.getMessage());
					}
				});
			}
		} else {
			Window.alert(" " + email + " must be a valid email ");
		}
	}

	private boolean isEmailAdress(String email) {
		if (email == null) {
			return false;
		} else if (email.length() < 5) {
			return false;
		} else if (email.indexOf("@") < 0) {
			return false;
		}
		return true;
	}

	public static DelebatesUI getInstance() {
		if (instance == null) {
			instance = new DelebatesUI();
		}
		return instance;
	}

}
