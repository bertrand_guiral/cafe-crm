package bg.phonelog.client.delegates;

import java.util.ArrayList;
import java.util.List;

import bg.phonelog.client.UtilPhoneLog;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;

public class DelegatesDataProvider extends ListDataProvider<Delegate>{

	private static DelegatesDataProvider instance;
	
	private DelegatesDataProvider() {
		fetchData();
		
	}
	
	@Override
	protected void onRangeChanged(HasData<Delegate> display) {
		 System.out.println("onRangeChanged "+display.getVisibleItemCount());
		 System.out.println("onRangeChanged "+display.getRowCount());
		// System.out.println("onRangeChanged "+display.);
				super.onRangeChanged(display);
		
		final int start = display.getVisibleRange().getStart();
		display.setVisibleRange(start, start+50);
		
        int length = display.getVisibleRange().getLength();
        System.out.println("onRangeChanged start "+start+" |  length : "+length+" | list.size : "+this.getList().size());
        if(start+3* length > this.getList().size()){
        	fetchData();
        }
      }

	public void fetchData() {
		final  int index =  this.getList().size();
	     AsyncCallback<List<Delegate>> callback = new AsyncCallback<List<Delegate>>() {
	          @Override
	          public void onFailure(Throwable e) {
	            Window.alert("Failure fetch Delegate Data 1113 !!! "+e.getClass()+"  "+e.getMessage());
	          }
	          @Override
	          public void onSuccess(List<Delegate> result) {
	        	  removeAll_Bg(result);
	        	  addItems(index,result);
	          }
	        };
	        // The remote service that should be implemented
	     
	     
	        UtilPhoneLog.greetingService.getListDelegates( callback);
	   }

    private void addItems(int index, List<Delegate> result){
    	System.out.println("AddItem "+result.size());
    	if(index == getList().size()){
    		getList().addAll(result);
    	}
    	List<Delegate> list = getList();
    	InfosFacture.getInstance().refreshAmont();
    }
	public void removeAll_Bg(List<Delegate> list) {
		this.getList().removeAll(list);
	}

	public void add(Delegate delegate) {
		List<Delegate> l = new ArrayList<Delegate>();
		l.add(delegate);
		getList().addAll(l);
	}

	public static DelegatesDataProvider getInstance() {
		if (instance == null){
			instance = new DelegatesDataProvider();
		}
		return instance;
	}
	 
}
