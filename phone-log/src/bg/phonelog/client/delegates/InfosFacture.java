package bg.phonelog.client.delegates;

import java.util.Date;
import java.util.List;

import bg.phonelog.client.UtilPayPal;
import bg.phonelog.client.UtilPhoneLog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class InfosFacture extends Composite {

	private String emailUser;
	private static InfosFactureUiBinder uiBinder = GWT.create(InfosFactureUiBinder.class);

	interface InfosFactureUiBinder extends UiBinder<Widget, InfosFacture> {
	}

	public InfosFacture() {
		initWidget(uiBinder.createAndBindUi(this));
		price_a_month.setText("" + InfosFacturesGroup.PRICE);
		setBuutonPAypal();
		refresh();

	}

	@UiField
	Label label_dateMax;

	@UiField
	Label label_nbDelegates;

	@UiField
	Label label_nbDelegatesAPayer;

	@UiField
	Label amount;

	@UiField
	Label price_a_month;

	@UiField
	VerticalPanel vPanel;

	public void refresh() {
		label_nbDelegates.setText(""+this.getNbUtilisateurs());
		UtilPhoneLog.greetingService.getInfosFacturesGroup(new AsyncCallback<InfosFacturesGroup>() {

			@Override
			public void onSuccess(InfosFacturesGroup i) {
				Date  dMax = i.getDateMaxPaiementMasse();
				label_dateMax.setText("" + DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT).format(dMax));
				emailUser = i.getEmailUser();

			}

			@Override
			public void onFailure(Throwable e) {
				Window.alert("ERROR 3456 " + e.getMessage());
			}
		});

	}

	private static InfosFacture instance;

	public static InfosFacture getInstance() {
		if (instance == null) {
			instance = new InfosFacture();
		}
		return instance;
	}

	int nbAbonnementAPayer =0;
	public void refreshAmont() {
		List<Delegate> list = DelegatesDataProvider.getInstance().getList();
		int nbAPayer =0;
		for(Delegate d : list){
			Date dateAbonnementOK = d.getDateAbonnementOK();
			if (d.isDateDepasse()){
				nbAPayer++;
			}
			
		}
		this.nbAbonnementAPayer = nbAPayer;
		this.label_nbDelegates.setText("" + list.size());
		this.label_nbDelegatesAPayer.setText("" + nbAPayer);
		double montant = getMontant();
		this.amount.setText("" + montant);
	}
	
	

	private int getNbUtilisateurs() {
		List<Delegate> list = DelegatesDataProvider.getInstance().getList();
		if (list == null){
			return 0;
		}
		int nb = list.size();
		return nb;
	}
	
	private double getMontant() {
		double montant = this.nbAbonnementAPayer * InfosFacturesGroup.PRICE;
		return montant;
	}

	/**
	 * 
	 <form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr"
	 * method="post"> <input type="hidden" name="cmd" value="_xclick"> <input
	 * type="hidden" name="business" value="bertrand.guiral.business@gmail.com">
	 * <input type="hidden" name="currency_code" value="USD"> <input
	 * type="hidden" name="item_name" value="Abonnement Phone Log"> <input
	 * type="hidden" name="amount" value="12.99"> <input type="hidden"
	 * name="notify_url" value="https://phone-log.appspot.com/paypal">
	 * 
	 * <input type="hidden" name="no_shipping" value="1"> <input type="hidden"
	 * name="return"
	 * value="https://phone-log.appspot.com/controller?action=displayJournal">
	 * <input type="hidden" name="cancel_return"
	 * value="https://phone-log.appspot.com/">
	 * 
	 * <input type="image"
	 * src="http://www.paypalobjects.com/fr_XC/i/btn/btn_buynow_LG.gif"
	 * border="0" name="submit"
	 * alt="Make payments with PayPal - it's fast, free and secure!"> </form>
	 */

	static final String ACTION_PAYPAL = "https://www.paypal.com/cgi-bin/webscr";
	static final String BUSINESS = "bertrand.guiral@gmail.com";

	private void setBuutonPAypal() {
		Image imagePaypal = new Image("/images/paypal/btn_buynowCC_LG.gif");
		vPanel.add(imagePaypal);
		try {

			imagePaypal.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					System.out.println("submit bg !!!! ");
					getformPaypal().submit();
				}
			});
			// vp.add(submit);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	
	private FormPanel getformPaypal() {
		FormPanel form = new FormPanel("_self");
		VerticalPanel vp = new VerticalPanel();
		
		vp.add(new Hidden("cmd", "_xclick"));
		
		vp.add(new Hidden("currency_code", "EUR"));
		vp.add(new Hidden("item_name", "Phone-Log "+getNbUtilisateurs()+"  Accounts"));
		vp.add(new Hidden("amount", ""+getMontant()));
		// vp.add(new
		// Hidden("notify_url","https://phone-log.appspot.com/paypal"));
		vp.add(new Hidden("no_shipping", "1"));
		vp.add(new Hidden("return", "https://phone-log.appspot.com/controller?action=displayJournal"));
		vp.add(new Hidden("cancel_return", "https://phone-log.appspot.com/controller?action=displayJournal"));
		vp.add(new Hidden("option_selection1", "" + emailUser));
		vp.add(new Hidden("item_number", "Phone-Log "+emailUser));
		vp.add(new Hidden("custom", ""+this.emailUser));
		vp.add(new Hidden("transaction_subject", "transaction_subjectAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBB"));
		if(UtilPayPal.isPaypalDebug){
			vp.add(new Hidden("business", UtilPayPal.BUSINESS_SANDBOX));
			form.setAction(UtilPayPal.ACTION_PAYPAL_SANDBOX);
		}else {
			vp.add(new Hidden("business", UtilPayPal.BUSINESS_REAL));
			form.setAction(UtilPayPal.ACTION_PAYPAL_REAL);
		}
		
		form.setMethod(FormPanel.METHOD_POST);
		form.add(vp);
		return form;
	}

	
}
