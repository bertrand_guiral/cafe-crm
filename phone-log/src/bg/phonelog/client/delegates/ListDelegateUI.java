package bg.phonelog.client.delegates;



import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import bg.phonelog.client.calls.JournalAppels;
import bg.phonelog.client.contacts.demo.ContentWidget;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionModel;

public class ListDelegateUI extends ContentWidget  {

	 interface Binder extends UiBinder<Widget, ListDelegateUI> {
	  }
	private static ListDelegateUI instance;
	
	private static ListDelegateUIUiBinder uiBinder = GWT.create(ListDelegateUIUiBinder.class);

	interface ListDelegateUIUiBinder extends UiBinder<Widget, ListDelegateUI> {
	}

	public ListDelegateUI() {
		super("List Delegates","" ,false,"", "ContactDatabase.java", "CwCellTable.ui.xml");
	}

	public static ListDelegateUI getInstance() {
		if (instance == null){
			instance = new ListDelegateUI();
		}
		return instance;
	}

	 @UiField(provided = true)
	  CellTable<Delegate> cellTable;

	  /**
	   * The pager used to change the range of data.
	   */

	  @UiField(provided = true)
	  SimplePager  pager;
	  
	  @UiField
	  Button buttonDelete;
	

	  @Override
	  public boolean hasMargins() {
	    return false;
	  }

	  /**
	   * Initialize this example.
	   */

	  @Override
	  public Widget onInitialize() {
	  
	// Create a CellTable.

	    // Set a key provider that provides a unique key for each contact. If key is
	    // used to identify contacts when fields (such as the name and address)
	    // change.
	    cellTable = new CellTable<Delegate>(60,Delegate.KEY_PROVIDER);
	    //cellTable.setVisibleRange(1,60);
	    //cellTable.setPageSize(60);
	    //cellTable.setRowCount(60,false);
	    cellTable.setWidth("100%", true);
	   
	    //cellTable.setRowCount(50, true);Ne marche pas
	    // Attach a column sort handler to the ListDataProvider to sort the list.
	    final ListHandler<Delegate> sortHandler = new ListHandler<Delegate>(DelegatesDataBase.getInstance().getDataProvider().getList());
	    cellTable.addColumnSortHandler(sortHandler);

	    // Create a Pager to control the table.
	    SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
	    pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
	    
	    pager.setDisplay(cellTable);
	  
	    // Add a selection model so we can select cells.
	    final SelectionModel<Delegate> selectionModel = new MultiSelectionModel<Delegate>( Delegate.KEY_PROVIDER);
	    cellTable.setSelectionModel(selectionModel,  DefaultSelectionEventManager.<Delegate> createCheckboxManager());
	    
	  
	    // Initialize the columns.
	    initTableColumns_(selectionModel,sortHandler);

	    // Add the CellList to the adapter in the database.
	    DelegatesDataBase.getInstance().addDataDisplay(cellTable);

	    // Create the UiBinder.
	    Binder uiBinder = GWT.create(Binder.class);
	    Widget widget = uiBinder.createAndBindUi(this);
	    buttonDelete.setText("Delete");
	    buttonDelete.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				List<Delegate> listAll  = cellTable.getVisibleItems();
				List<Delegate> listSelected  = new ArrayList<Delegate>();
				
				for(Delegate delegate : listAll){
					if(selectionModel.isSelected(delegate)){
						listSelected.add(delegate);
					}
				}
				String s = ""+listSelected.size()+" messages deleting";
				boolean b = Window.confirm(s);
				
				if (b) {
					DelegatesDataBase.getInstance().deleteDegates(listSelected);
				}
				
				
			}
		});
	    return widget;
	  }

	  @Override
	  protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(JournalAppels.class, new RunAsyncCallback() {

	      public void onFailure(Throwable caught) {
	        callback.onFailure(caught);
	      }

	      public void onSuccess() {
	        callback.onSuccess(onInitialize());
	      }
	    });
	  }

	  /**
	   * Add the columns to the table.
	   */

	      private void initTableColumns_( final SelectionModel<Delegate> selectionModel, ListHandler<Delegate> sortHandler) {
		  

		  // Checkbox column. This table will uses a checkbox column for selection.
	    // Alternatively, you can call cellTable.setSelectionEnabled(true) to enable
	    // mouse selection.
		
	    Column<Delegate, Boolean> checkColumn = new Column<Delegate, Boolean>(
	        new CheckboxCell(true, false)) {
	      @Override
	      public Boolean getValue(Delegate object) {
	        // Get the value from the selection model.
	        return selectionModel.isSelected(object);
	      }
	    };
	   // cellTable.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
	    cellTable.addColumn(checkColumn, "Del");
	    // Age
	 
	   // Duration ------------------------------------


	  // Number ------------------------------------------------------------.
	   
	    // Number User
	   
	 
	    // Contact contact name.
	    Column<Delegate, String> contactNameColumn = new Column<Delegate, String>(
	        new EditTextCell()) {
	      @Override
	      public String getValue(Delegate object) {
	        return object.getEmail();
	      }
	    };
	    contactNameColumn.setSortable(true);
	    sortHandler.setComparator(contactNameColumn, new Comparator<Delegate>() {
	      public int compare(Delegate o1, Delegate o2) {
	        return o1.getEmail().compareTo(o2.getEmail());
	      }
	    });
	    cellTable.addColumn(contactNameColumn, "E-Mail");
	    contactNameColumn.setFieldUpdater(new FieldUpdater<Delegate, String>() {
	      public void update(int index, Delegate object, String value) {
	        // Called when the user changes the value.
	        object.setEmail(value);
	      }
	    });
	    //////////////////////////////////////
	    TextColumn<Delegate> columnPaid = new TextColumn<Delegate>() {
		      @Override
		      public String getValue(Delegate delegate_) {
		        return ""+delegate_.getPaid();
		      }
		    };
		    columnPaid.setSortable(true);
		    cellTable.addColumn(columnPaid, "Paid");
		    
	    ////////////////////////////////////////
	    cellTable.setColumnWidth(checkColumn, 10, Unit.PCT);
	    cellTable.setColumnWidth(contactNameColumn, 30, Unit.PCT);
	    cellTable.setColumnWidth(columnPaid, 30, Unit.PCT);
		   
	  }
	  
	 public void refresh() {
		 this.cellTable.redraw();
	 }
	    	      

}
