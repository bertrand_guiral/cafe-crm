package bg.phonelog.client.delegates;

import java.io.Serializable;
import java.util.Date;

import com.google.gwt.view.client.ProvidesKey;

/**
 * Information about a contact.
 */
public class Delegate implements Comparable<Delegate>, Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The key provider that provides the unique ID of a contact.
	 */
	public static final ProvidesKey<Delegate> KEY_PROVIDER = new ProvidesKey<Delegate>() {

		public Object getKey(Delegate item) {
			return item == null ? null : item.getEmail();
		}
	};

	private String email = "bgName@dot.com";

	private Date dateAbonnementOK;
	public Delegate() {

	}

	public Delegate(String email2, Date dateAbonnementOK) {
		this.email = email2;
		this.dateAbonnementOK = dateAbonnementOK;
	}

	public int compareTo(Delegate o) {
		return (o == null || o.email == null) ? -1 : -o.email.compareTo(email);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Delegate) {
			return email.equals(((Delegate) o).email);
		}
		return false;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String contactName) {
		this.email = contactName;
	}

	public Date getDateAbonnementOK() {
		return dateAbonnementOK;
	}

	public void setDateAbonnementOK(Date dateAbonnementOK) {
		this.dateAbonnementOK = dateAbonnementOK;
	}

	public String getPaid() {
		if(dateAbonnementOK == null){
			return "New";
		}else if (isDateDepasse()){
			return "To Pay";
		}
		return "Paid";
	}
	
	static long TIME_DELTA_HOUR = 6 * 60 *60 * 1000;
	

	public static boolean isDateDepasse(Date dateAbonnementOK) {
		if (dateAbonnementOK==null){
			return true;
		}
		long limite = dateAbonnementOK.getTime() + TIME_DELTA_HOUR ;
		long now = (new Date()).getTime();
		return now > limite;
	}

	public boolean isDateDepasse() {
		
		return isDateDepasse(this.dateAbonnementOK);
	}

}
