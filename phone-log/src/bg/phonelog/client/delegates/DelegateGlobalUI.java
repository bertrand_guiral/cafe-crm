package bg.phonelog.client.delegates;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class DelegateGlobalUI extends Composite {

	private static DelegateGlobalUIUiBinder uiBinder = GWT.create(DelegateGlobalUIUiBinder.class);

	interface DelegateGlobalUIUiBinder extends UiBinder<Widget, DelegateGlobalUI> {
	}

	public DelegateGlobalUI() {
		initWidget(uiBinder.createAndBindUi(this));
		vPAnel.add(InfosFacture.getInstance());
		vPAnel.add(DelebatesUI.getInstance());
		
	}

	@UiField
	VerticalPanel vPAnel;

	
	
	private static DelegateGlobalUI instance;

	public static Widget getInstance()  {
		if (instance ==null){
			instance = new DelegateGlobalUI();
		}
		return instance;
	}

	

}
