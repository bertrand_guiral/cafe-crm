package bg.phonelog.client.delegates;

import java.io.Serializable;
import java.util.Date;

public class InfosFacturesGroup implements Serializable {

	Date dateMaxPaiementMasse;
	
	public static double PRICE = 2.0d;
	
	double amount;
	String emailUser;

	public InfosFacturesGroup() {

	}

	public Date getDateMaxPaiementMasse() {
		return dateMaxPaiementMasse;
	}

	
	public void setDateMaxPaiementMasse(Date dateMax) {
		this.dateMaxPaiementMasse = dateMax;
	}

	

	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getEmailUser() {
		return emailUser;
	}

	public void setEmailUser(String emailUser) {
		this.emailUser = emailUser;
	}

}
