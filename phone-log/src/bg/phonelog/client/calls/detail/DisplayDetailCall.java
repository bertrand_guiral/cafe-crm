package bg.phonelog.client.calls.detail;

import bg.phonelog.client.UtilPhoneLog;
import bg.phonelog.client.alert.DisplayAlert;
import bg.phonelog.client.calls.CallInfo;
import bg.phonelog.client.calls.JournalAppels;
import bg.phonelog.client.crypt.Crypto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

public class DisplayDetailCall extends Composite  {

	JournalAppels journalAppels;
	
	DialogBox dialogBoxDetail ;
	private static DisplayDetailCallUiBinder uiBinder = GWT
			.create(DisplayDetailCallUiBinder.class);

	interface DisplayDetailCallUiBinder extends
			UiBinder<Widget, DisplayDetailCall> {
	}

	public DisplayDetailCall(JournalAppels journalAppels) {
		this.journalAppels= journalAppels;
		initWidget(uiBinder.createAndBindUi(this));
		init();
	}
	
	private void init() {
		this.dialogBoxDetail = new DialogBox();
		this.dialogBoxDetail.add(this);
			   // this.dialogBoxDetail.add(new Label("aaaaa"));
		this.dialogBoxDetail.hide();
		this.buttonCancel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				dialogBoxDetail.hide();
			}
		});
		this.buttonContactGmail.addClickHandler(new ClickHandler() {
			//https://www.google.com/contacts/#contacts/search/0682426361
			@Override
			public void onClick(ClickEvent event) {
				Window.open("https://www.google.com/contacts/#contacts/search/"+getContactNameOrNumber(),"_blank","");
			}

			
		});
		
		this.buttonHistoric.addClickHandler(new ClickHandler() {
			//https://www.google.com/contacts/#contacts/search/0682426361
			@Override
			public void onClick(ClickEvent event) {
				showHistoric_();
				
			}
		});
	}
	
	String getNumberCanonique(){
		String number = this.callInfo.getPhoneNumber();
		if (number.startsWith("+")){
			number = "%2B"+number.substring(1);
		}
		return number;
	}
	
	String getContactName(){
		String contactname = this.callInfo.getContactName();
		if (contactname == null){
			contactname="";
		}
		return contactname;
	}
	
	private String getContactNameOrNumber() {
		String r = getContactName();
		if (r.length()==0){
			r = getNumberCanonique();
		}
		return r;
	}

	@UiField
	Button buttonOK;
	
	@UiField
	Button buttonCancel;
	
	@UiField
	InlineLabel labelNumber;
	
	@UiField
	InlineLabel labelContactName;
	
	@UiField
	InlineLabel labelDate;
	
	@UiField
	InlineLabel labelAge;
	
	@UiField
	TextArea textAreaComment;
	
	@UiField
	Button buttonContactGmail;
	
	@UiField
	Button buttonHistoric;
	
	
	
	@UiHandler("buttonOK")
	void onClick(ClickEvent e) {
		this.dialogBoxDetail.hide();
		this.callInfo.setComment(this.textAreaComment.getText());
		this.journalAppels.refresh();
		String pass = Crypto.getText();
		UtilPhoneLog.greetingService.updateCallInfo(pass,this.callInfo, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				
			}
			
			@Override
			public void onFailure(Throwable e) {
				Window.alert("Echec update 343 "+e.getMessage());
			}
		});
	}
	

	
	
	CallInfo callInfo;
	public void setCallInfo(CallInfo callInfo){
		this.callInfo=callInfo;
		update();
	}
	private void update() {
		this.labelNumber.setText(this.callInfo.getPhoneNumber());
		this.labelContactName.setText(this.callInfo.getContactName());
		
		this.labelAge.setText(this.callInfo.getAge());
		this.labelDate.setText(""+this.callInfo.getDate());
		this.textAreaComment.setText(this.callInfo.getComment());
		
	}

	public void showRelativeTo___( UIObject ui) {
		this.dialogBoxDetail.showRelativeTo(ui);
	}

	public DialogBox getDialogBoxDetail() {
		return dialogBoxDetail;
	}

	public void show() {
	  //	this.dialogBoxDetail.show();
		this.dialogBoxDetail.setPopupPositionAndShow(new PopupPanel.PositionCallback() {
			
	          public void setPosition(int offsetWidth, int offsetHeight) {
	        	  
	              int left = (Window.getClientWidth() - offsetWidth) / 3;
	              int top = Window.getScrollTop()+(Window.getClientHeight() - offsetHeight) / 3;
	              dialogBoxDetail.setPopupPosition(left, top);
	            }
	          });
	}
	
	private void showHistoric_() {
		final String phoneNumber = this.callInfo.getPhoneNumber();
		final String contactNAme = this.callInfo.getContactName();
		DisplayAlert.showHistoric2(phoneNumber, contactNAme, buttonHistoric);
	}
	
}
