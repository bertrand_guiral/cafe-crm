package bg.phonelog.client.calls;

import java.util.List;

import bg.phonelog.client.UtilPhoneLog;
import bg.phonelog.client.crypt.Crypto;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;

public class BgListDataProvider extends ListDataProvider<CallInfo>{

	
	public BgListDataProvider() {
		fetchData();
	}
	
	@Override
	protected void onRangeChanged(HasData<CallInfo> display) {
		 System.out.println("onRangeChanged "+display.getVisibleItemCount());
		 System.out.println("onRangeChanged "+display.getRowCount());
		// System.out.println("onRangeChanged "+display.);
				super.onRangeChanged(display);
		
		final int start = display.getVisibleRange().getStart();
		display.setVisibleRange(start, start+50);
		
        int length = display.getVisibleRange().getLength();
        System.out.println("onRangeChanged start "+start+" |  length : "+length+" | list.size : "+this.getList().size());
        if(start+3* length > this.getList().size()){
        	fetchData();
        }
      }

	public void fetchData() {
		final int length = 100;
		final  int index =  this.getList().size();
	    String pass = Crypto.getText();
		 AsyncCallback<List<CallInfo>> callback = new AsyncCallback<List<CallInfo>>() {
	          @Override
	          public void onFailure(Throwable e) {
	            Window.alert("Failure fetch Data 113 !!! "+e.getClass()+"  "+e.getMessage());
	          }
	          @Override
	          public void onSuccess(List<CallInfo> result) {
	        	  addItems(index,result);
	          }
	        };
	        // The remote service that should be implemented
	     
	     
	        UtilPhoneLog.greetingService.getListCallInfo(pass,index, length, callback);
	   }

    private void addItems(int index, List<CallInfo> result){
    	System.out.println("AddItem "+result.size());
    	if(index == getList().size()){
    		getList().addAll(result);
    	}
    }
	public void removeAll_Bg(List<CallInfo> list) {
		this.getList().removeAll(list);
	}
	 
}
