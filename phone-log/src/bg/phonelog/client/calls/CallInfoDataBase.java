package bg.phonelog.client.calls;

/*
 * Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bg.phonelog.client.UtilPhoneLog;
import bg.phonelog.client.crypt.Crypto;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.HasData;

/**
 * The data source for contact information used in the sample.
 */
public class CallInfoDataBase {


 
  /**
   * The singleton instance of the database.
   */
  private static CallInfoDataBase instance;
  


  /**
   * Get the singleton instance of the contact database.
   *
   * @return the singleton instance
   */
  public static CallInfoDataBase getInstance() {
    if (instance == null) {
      instance = new CallInfoDataBase();
    }
    return instance;
  }

  /**
   * The provider that holds the list of contacts in the database.
   */
  private BgListDataProvider listDataProvider = new BgListDataProvider();

 
  /**
   * Construct a new contact database.
   */
  private CallInfoDataBase() {
  	 
 }

  /**
   * Add a display to the database. The current range of interest of the display
   * will be populated with data.
   *
   * @param display a {@Link HasData}.
   */
  public void addDataDisplay(HasData<CallInfo> display) {
    listDataProvider.addDataDisplay(display);
  }

 

  public BgListDataProvider getDataProvider() {
    return listDataProvider;
  }


  public void deleteCallInfo(List<CallInfo> list) {
	//this.listDataProvider.getList().removeAll(list);
	this.listDataProvider.removeAll_Bg(list);
	List<Long> listId = new ArrayList<Long>();
	for(CallInfo callInfo : list){
		listId.add(callInfo.getId());
	}
	String pass = Crypto.getText();
	UtilPhoneLog.greetingService.deleteCallInfo(pass,listId, new AsyncCallback<Void>() {

		@Override
		public void onFailure(Throwable e) {
			Window.alert("failure deleting messages 321 : "+e.getMessage());
		}

		@Override
		public void onSuccess(Void result) {
			if (UtilPhoneLog.isDebug()){
			   Window.alert("Succes  deleting messages ");
			}
		}
	});
	
}



public void refreshList() {
	if (this.getDataProvider().getList().size() == 0){
		this.getDataProvider().fetchData();
	}else {
		Date upToDate = this.getDataProvider().getList().get(0).getDate();
		refreshList(upToDate);	  
	}
	
}

public void refreshList(Date upToDate){
	String pass = Crypto.getText();
	UtilPhoneLog.greetingService.refreshListCallInfo(pass,upToDate, new AsyncCallback<List<CallInfo>>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Refresh :failure 789 "+caught.getMessage());
		}

		@Override
		public void onSuccess(List<CallInfo> result) {
			//Window.alert("Refresh refreshListCallInfo : result.size"+result.size());
			insertUpDate(result);
		}
	});
}
  
private void insertUpDate(List<CallInfo> result) {
	this.getDataProvider().getList().addAll(0, result);
		JournalAppels.getInstance() .refresh();
	
} 

}