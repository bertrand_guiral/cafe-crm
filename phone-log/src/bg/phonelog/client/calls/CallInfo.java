package bg.phonelog.client.calls;

import java.io.Serializable;
import java.util.Date;

import bg.phonelog.client.UtilPhoneLog;

import com.google.gwt.view.client.ProvidesKey;

/**
 * Information about a contact.
 */
public  class CallInfo implements Comparable<CallInfo> , Serializable{

 
	private static final long serialVersionUID = 1L;

/**
   * The key provider that provides the unique ID of a contact.
   */
  public static final ProvidesKey<CallInfo> KEY_PROVIDER = new ProvidesKey<CallInfo>() {
	  
    public Object getKey(CallInfo item) {
      return item == null ? null : item.getId();
    }
  };

  private static int nextId = 0;

 // private String address;
 // private Date birthday;
 // private Category category;
 // private String firstName;
  private  Long id;
 // private String lastName;
  
  private String phoneNumber="0000000";
  private String phoneUser;
  private String contactName="bgName";
  private String typeCall = "1";
  private String comment="commentBg";
  private Date date = new Date();
  private long duration;
  private String messageSms;

  public CallInfo() {
   // this.id = nextId;
    nextId++;
    
  }

  public int compareTo(CallInfo o) {
    return (o == null || o.phoneNumber == null) ? -1
        : -o.phoneNumber.compareTo(phoneNumber);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof CallInfo) {
      return id == ((CallInfo) o).id;
    }
    return false;
  }



  
  
 
  /**
   * @return the unique ID of the contact
   */
  public Long getId() {
    return this.id;
  }



public String getPhoneNumber() {
	return phoneNumber;
}

public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
}

public String getPhoneUser() {
	return phoneUser;
}

public void setPhoneUser(String phoneUser) {
	this.phoneUser = phoneUser;
}

public String getTypeCall() {
	return typeCall;
}

public void setTypeCall(String typeCall) {
	this.typeCall = typeCall;
}

public String getComment() {
	return comment;
}

public void setComment(String comment) {
	this.comment = comment;
}

public String getContactName() {
	return contactName;
}

public void setContactName(String contactName) {
	this.contactName = contactName;
}

public Date getDate() {
	return date;
}

public void setDate(Date date) {
	this.date = date;
}

public void setId(Long id) {
	this.id = id;
}

public Long getDuration() {
	return duration;
}

public void setDuration(long duration) {
	this.duration = duration;
}

public String getAge() {
	// TODO Auto-generated method stub
	return UtilPhoneLog.getAge(this.date);
}

public boolean isMessage() {
	if (this.typeCall== null){
		return false;
	}
	if (this.typeCall.equals("101")){
		return true;
	}
	if (this.typeCall.equals("102")){
		return true;
	}
	return false;
}

public String getMessageSms() {
	return messageSms;
}

public void setMessageSms(String messageSms) {
	this.messageSms = messageSms;
}

public String getCommentOrMessage() {
	if (comment== null || comment.length()==0){
		return getMessageSmsNormal();
	}else {
		return getComment();
	}
}

private String getMessageSmsNormal() {
	if (messageSms == null){
		return "";
	}
	return messageSms.trim().replace("\n", " ").replace("\t", " ");
}


  
}
