package bg.phonelog.client.calls;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import bg.phonelog.client.UtilPhoneLog;
import bg.phonelog.client.calls.detail.DisplayDetailCall;
import bg.phonelog.client.contacts.demo.ContentWidget;
import bg.phonelog.client.crypt.Crypto;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ImageCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionModel;

/**
 * Example file.
 */

public class JournalAppels extends ContentWidget {

	private static JournalAppels instance;

	public static DateTimeFormat simpleDateFormat2;

	/**
	 * The UiBinder interface used by this example.
	 */

	interface Binder extends UiBinder<Widget, JournalAppels> {
	}

	/**
	 * The constants used in this Content Widget.
	 */

	public static JournalAppels getInstance() {
		if (instance == null) {
			instance = new JournalAppels();
		}
		return instance;
	}

	/**
	 * The main CellTable.
	 */

	@UiField(provided = true)
	CellTable<CallInfo> cellTable;

	@UiField
	Button buttonExcel;
	/**
	 * The pager used to change the range of data.
	 */

	@UiField(provided = true)
	SimplePager pager;

	@UiField
	Button buttonDelete;

	private DisplayDetailCall displayDetail_;

	/**
	 * An instance of the constants.
	 */

	private final BgXwConstants constants;

	private JournalAppels() {
		this(new BgXwConstants());
		instance = this;
	}

	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public JournalAppels(BgXwConstants constants) {
		super("List Calls", "", false, "ContactDatabase.java", "CwCellTable.ui.xml");
		this.constants = constants;
		displayDetail_ = new DisplayDetailCall(this);

	}

	@Override
	public boolean hasMargins() {
		return false;
	}

	/**
	 * Initialize this example.
	 */

	@Override
	public Widget onInitialize() {
		// Create a CellTable.

		// Set a key provider that provides a unique key for each contact. If
		// key is
		// used to identify contacts when fields (such as the name and address)
		// change.
		cellTable = new CellTable<CallInfo>(60, CallInfo.KEY_PROVIDER);
		// cellTable.setVisibleRange(1,60);
		// cellTable.setPageSize(60);
		// cellTable.setRowCount(60,false);
		cellTable.setWidth("100%", true);

		// cellTable.setRowCount(50, true);Ne marche pas
		// Attach a column sort handler to the ListDataProvider to sort the
		// list.
		final ListHandler<CallInfo> sortHandler = new ListHandler<CallInfo>(CallInfoDataBase.getInstance().getDataProvider().getList());
		cellTable.addColumnSortHandler(sortHandler);

		// Create a Pager to control the table.
		SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
		pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);

		pager.setDisplay(cellTable);

		// Add a selection model so we can select cells.
		final SelectionModel<CallInfo> selectionModel = new MultiSelectionModel<CallInfo>(CallInfo.KEY_PROVIDER);
		cellTable.setSelectionModel(selectionModel, DefaultSelectionEventManager.<CallInfo> createCheckboxManager());

		// Initialize the columns.
		initTableColumns_(selectionModel, sortHandler);

		// Add the CellList to the adapter in the database.
		CallInfoDataBase.getInstance().addDataDisplay(cellTable);

		// Create the UiBinder.
		Binder uiBinder = GWT.create(Binder.class);
		Widget widget = uiBinder.createAndBindUi(this);
		buttonDelete.setText("Delete");
		buttonDelete.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				List<CallInfo> listAll = cellTable.getVisibleItems();
				List<CallInfo> listSelected = new ArrayList<CallInfo>();

				for (CallInfo callInfo : listAll) {
					if (selectionModel.isSelected(callInfo)) {
						listSelected.add(callInfo);
					}
				}
				String s = "" + listSelected.size() + " messages deleting";
				boolean b = Window.confirm(s);

				if (b) {
					CallInfoDataBase.getInstance().deleteCallInfo(listSelected);
				}

			}
		});
		buttonExcel.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				excelProcess();
			}
		});
		return widget;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(JournalAppels.class, new RunAsyncCallback() {

			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	/**
	 * Add the columns to the table.
	 */

	private void initTableColumns_(final SelectionModel<CallInfo> selectionModel, ListHandler<CallInfo> sortHandler) {

		// Checkbox column. This table will uses a checkbox column for
		// selection.
		// Alternatively, you can call cellTable.setSelectionEnabled(true) to
		// enable
		// mouse selection.

		Column<CallInfo, Boolean> checkColumn = new Column<CallInfo, Boolean>(new CheckboxCell(true, false)) {
			@Override
			public Boolean getValue(CallInfo object) {
				// Get the value from the selection model.
				return selectionModel.isSelected(object);
			}
		};
		// cellTable.addColumn(checkColumn,
		// SafeHtmlUtils.fromSafeConstant("<br/>"));
		cellTable.addColumn(checkColumn, "Del");
		// Age
		Column<CallInfo, String> ageColumn_ = new Column<CallInfo, String>(new TextCell()) {
			@Override
			public String getValue(CallInfo object) {
				return object.getAge();
			}
		};
		ageColumn_.setSortable(false);

		cellTable.addColumn(ageColumn_, "");

		// Date ------------------------------------
		Column<CallInfo, String> dateColumn_ = new Column<CallInfo, String>(new TextCell()) {
			@Override
			public String getValue(CallInfo object) {
				Date date = object.getDate();
				return "" + DateTimeFormat.getShortDateFormat().format(date) + " " + DateTimeFormat.getShortTimeFormat().format(date);
			}
		};
		dateColumn_.setSortable(true);
		sortHandler.setComparator(dateColumn_, new Comparator<CallInfo>() {
			public int compare(CallInfo o1, CallInfo o2) {
				return o1.getDate().compareTo(o2.getDate());
			}
		});
		cellTable.addColumn(dateColumn_, constants.cwCellTableColumnDate());
		// Duration ------------------------------------
		Column<CallInfo, String> durationColumn = new Column<CallInfo, String>(new TextCell()) {
			@Override
			public String getValue(CallInfo object) {
				return "" + object.getDuration() + " s";
			}
		};
		durationColumn.setSortable(true);
		sortHandler.setComparator(durationColumn, new Comparator<CallInfo>() {
			public int compare(CallInfo o1, CallInfo o2) {
				return o1.getDuration().compareTo(o2.getDuration());
			}
		});
		cellTable.addColumn(durationColumn, constants.cwCellTableColumnDuration());
		// Number ------------------------------------------------------------.
		Column<CallInfo, String> remoteNumberColumn = new Column<CallInfo, String>(new TextCell()) {
			@Override
			public String getValue(CallInfo object) {
				return object.getPhoneNumber();
			}
		};
		remoteNumberColumn.setSortable(true);
		sortHandler.setComparator(remoteNumberColumn, new Comparator<CallInfo>() {
			public int compare(CallInfo o1, CallInfo o2) {
				return o1.getPhoneNumber().compareTo(o2.getPhoneNumber());
			}
		});
		cellTable.addColumn(remoteNumberColumn, constants.cwCellTableColumnNumber());

		// Number User
		Column<CallInfo, String> numberUserColumn = new Column<CallInfo, String>(new TextCell()) {
			@Override
			public String getValue(CallInfo object) {
				return object.getPhoneUser();
			}
		};
		numberUserColumn.setSortable(true);
		sortHandler.setComparator(numberUserColumn, new Comparator<CallInfo>() {
			public int compare(CallInfo o1, CallInfo o2) {
				return o1.getPhoneUser().compareTo(o2.getPhoneUser());
			}
		});
		cellTable.addColumn(numberUserColumn, constants.cwCellTableColumnNumberUser());

		// Contact contact name.
		Column<CallInfo, String> contactNameColumn = new Column<CallInfo, String>(new EditTextCell()) {
			@Override
			public String getValue(CallInfo object) {
				return object.getContactName();
			}
		};
		contactNameColumn.setSortable(true);
		sortHandler.setComparator(contactNameColumn, new Comparator<CallInfo>() {
			public int compare(CallInfo o1, CallInfo o2) {
				return o1.getContactName().compareTo(o2.getContactName());
			}
		});
		cellTable.addColumn(contactNameColumn, constants.cwCellTableColumnContact());
		contactNameColumn.setFieldUpdater(new FieldUpdater<CallInfo, String>() {
			public void update(int index, CallInfo object, String value) {
				// Called when the user changes the value.
				object.setContactName(value);
			}
		});

		// type

		ImageCell typeCell = new ImageCell();

		Column<CallInfo, String> typeColumn = new Column<CallInfo, String>(typeCell) {
			@Override
			public String getValue(CallInfo callInfo) {
				String s = UtilPhoneLog.getTypeImage(callInfo.getTypeCall());
				// "/images/call_"+object.getTypeCall()+".png";
				return s;
			}
		};
		cellTable.addColumn(typeColumn, "");

		// Button detail
		final ButtonCell buttonDetail = new ButtonCell();
		Column<CallInfo, String> buttonDetailColumn = new Column<CallInfo, String>(buttonDetail) {
			@Override
			public String getValue(CallInfo object) {
				return "Detail";
			}
		};
		buttonDetailColumn.setSortable(false);

		cellTable.addColumn(buttonDetailColumn, "");
		buttonDetailColumn.setFieldUpdater(new FieldUpdater<CallInfo, String>() {
			public void update(int index, CallInfo object, String value) {
				// Window.alert("detail "+object.getContactName());
				displayDetail_.setCallInfo(object);
				displayDetail_.show();
			}
		});

		// Comment.
		Column<CallInfo, String> commentColumn = new Column<CallInfo, String>(new EditTextCell()) {
			@Override
			public String getValue(CallInfo callinfo) {

				String comment;
				if (callinfo.isMessage() ){
					comment = callinfo.getMessageSms();
				}else {
					comment = callinfo.getComment();
				}
				if (comment == null) {
					comment = "";
				}
				return comment;
			}
		};
		commentColumn.setSortable(true);
		sortHandler.setComparator(commentColumn, new Comparator<CallInfo>() {
			public int compare(CallInfo o1, CallInfo o2) {
				return o1.getComment().compareTo(o2.getComment());
			}
		});
		cellTable.addColumn(commentColumn, constants.cwCellTableColumnComment());
		commentColumn.setFieldUpdater(new FieldUpdater<CallInfo, String>() {
			public void update(int index, CallInfo object, String value) {
				// Called when the user changes the value.
				object.setComment(value);
				String pass = Crypto.getText();
				UtilPhoneLog.greetingService.updateCallInfo(pass, object, new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable e) {
						Window.alert("Update Call Info Failure 567 " + e.getMessage());
					}

					@Override
					public void onSuccess(Void result) {
						if (UtilPhoneLog.isDebug()) {
							Window.alert("Update Call Info OK");
						}

					}
				});

			}
		});

		cellTable.setColumnWidth(checkColumn, 10, Unit.PCT);
		cellTable.setColumnWidth(dateColumn_, 50, Unit.PCT);
		cellTable.setColumnWidth(durationColumn, 18, Unit.PCT);
		cellTable.setColumnWidth(contactNameColumn, 30, Unit.PCT);
		cellTable.setColumnWidth(typeColumn, 12, Unit.PCT);
		cellTable.setColumnWidth(buttonDetailColumn, 17, Unit.PCT);
		cellTable.setColumnWidth(commentColumn, 60, Unit.PCT);
		cellTable.setColumnWidth(remoteNumberColumn, 34, Unit.PCT);
		cellTable.setColumnWidth(numberUserColumn, 34, Unit.PCT);
		cellTable.setColumnWidth(ageColumn_, 20, Unit.PCT);

	}

	public void refresh() {
		this.cellTable.redraw();
	}

	// fib initTableColumns

	private void excelProcess() {
		UtilPhoneLog.processExcell(CallInfoDataBase.getInstance().getDataProvider().getList());
	}
}