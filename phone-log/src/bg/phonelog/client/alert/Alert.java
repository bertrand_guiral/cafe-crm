package bg.phonelog.client.alert;

import java.io.Serializable;
import java.util.Date;

import bg.phonelog.client.UtilPhoneLog;

import com.google.gwt.http.client.URL;
import com.google.gwt.i18n.client.DateTimeFormat;

public class Alert implements Serializable{

	
	
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String emailUser;
	private String phonecall;
	private String contactName;
	private Date date;
	
	transient private String search;
	
	public Alert() {
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmailUser() {
		return emailUser;
	}
	public void setEmailUser(String emailUser) {
		this.emailUser = emailUser;
	}
	public String getPhonecall() {
		return phonecall;
	}
	public void setPhonecall(String phonecall) {
		this.phonecall = phonecall;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public Date getDate() {
		return date;
	}
	
	public String getDateStr() {
		return DateTimeFormat.getShortDateFormat().format(date)+" - "+DateTimeFormat.getShortTimeFormat().format(date);
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	

	private String getAge() {
		return UtilPhoneLog.getAge(this.date);
	}
	
	public String getSearch() {
		if (search== null){
			return URL.encode((this.phonecall+" "+this.contactName).trim());
		}else {
			return URL.encode(search);
		}
	}
	
	public String toString() {
		return ""+phonecall+" "+this.contactName+" "+this.getDateStr()+" "+this.getAge()+" ago";
	}
}
