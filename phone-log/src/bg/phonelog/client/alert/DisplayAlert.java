/**
 * 
 */
package bg.phonelog.client.alert;

import java.util.List;

import bg.phonelog.client.UtilPhoneLog;
import bg.phonelog.client.alert.historic.DisplayHistoric;
import bg.phonelog.client.calls.CallInfo;
import bg.phonelog.client.calls.CallInfoDataBase;
import bg.phonelog.client.crypt.Crypto;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Bertrand
 *
 */
public class DisplayAlert extends Composite implements HasText {

	@UiField
	Button buttonRefresh;

	@UiField
	Button buttonHistoric;

	
	@UiField
	InlineLabel labelAlert;
	
	@UiField
	InlineLabel labelSearch;
	
	
	private static DisplayAlertUiBinder uiBinder = GWT.create(DisplayAlertUiBinder.class);

	
	interface DisplayAlertUiBinder extends UiBinder<Widget, DisplayAlert> {
	}
	
	private static DisplayAlert instance;

	public static DisplayAlert getInstance() {
		if (instance == null){
			new DisplayAlert();
		}
		return instance;
	}



	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	private DisplayAlert() {
		instance = this;
		initWidget(uiBinder.createAndBindUi(this));
		this.buttonRefresh.setText("Refresh");
		this.buttonHistoric.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				showHistoric_();
			}
		});
		displayAlert(null);
		//refresh();
	}

	
	 
	public DisplayAlert(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));

		// Can access @UiField after calling createAndBindUi
		buttonRefresh.setText(firstName);
	}

	@UiHandler("buttonRefresh")
	void onClick(ClickEvent e) {
		refresh();
	}
	


	public void setText(String text) {
		buttonRefresh.setText(text);
	}

	/**
	 * Gets invoked when the default constructor is called
	 * and a string is provided in the ui.xml file.
	 */
	public String getText() {
		return buttonRefresh.getText();
	}

	Alert alert;
	private void displayAlert(Alert alert_){
		this.alert = alert_;
		if (alert == null){
			this.labelAlert.setVisible(false);
			this.labelSearch.setVisible(false);
			this.buttonHistoric.setVisible(false);
			
		} else {
			this.labelAlert.setVisible(true);
			this.labelAlert.setText(""+alert);
			this.labelSearch.setVisible(true);
			this.buttonHistoric.setVisible(true);
		}
	}
	
	private void refresh() {
		try {
			refreshAlert();
			CallInfoDataBase.getInstance().refreshList();
			//Window.alert("Send refreshing done pass:  "+Crypto.getText() );
		} catch (Exception e) {
			Window.alert("Exception refreshing !!! 144 "+e.getMessage());
		}
	}
	public void refreshAlert() {
				UtilPhoneLog.greetingService.getAlert(getPass(),new AsyncCallback<Alert>() {
			@Override
			public void onSuccess(Alert result) {
				displayAlert(result);
			}
			
			@Override
			public void onFailure(Throwable e) {
				Window.alert("Fetching Last Phone Alert failure 345 "+e.getClass()+" "+e.getMessage());
				
			}
		});
	}
	
	private String getPass() {
		
		return Crypto.getText();
	}
	
	VerticalPanel vPanelHistoric = new VerticalPanel();
	
	private void showHistoric_() {
		final String phoneNumber = alert.getPhonecall();
		final String contactNAme = alert.getContactName();
		showHistoric2(phoneNumber, contactNAme, buttonHistoric);
	}
	
	public static void showHistoric2(final String phoneNumber, final String contactNAme,final Button button ){
		 String pass = Crypto.getText();
		UtilPhoneLog.greetingService.getHistoric(pass, phoneNumber,0, 50, new AsyncCallback<List<CallInfo>>() {

			@Override
			public void onFailure(Throwable e) {
				Window.alert("Exception fetching historic !! "+e.getMessage());
			}

			@Override
			public void onSuccess(List<CallInfo> list) {
				DisplayHistoric.getInstance().setListCallInfo(list, phoneNumber, contactNAme);
				DisplayHistoric.getInstance().showRelativeTo(button);
			}
		});
	}
}
