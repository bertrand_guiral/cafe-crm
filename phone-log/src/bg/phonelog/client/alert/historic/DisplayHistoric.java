package bg.phonelog.client.alert.historic;

import java.util.List;

import bg.phonelog.client.UtilPhoneLog;
import bg.phonelog.client.calls.CallInfo;

import com.google.gwt.cell.client.ImageCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;

public class DisplayHistoric extends Composite {

	
	ListDataProvider<CallInfo> dataProvider = new ListDataProvider<CallInfo>();

	private static DisplayHistoric instance = new DisplayHistoric();
	DialogBox dialogBoxDetail ;
	
	
	public static DisplayHistoric getInstance() {
		
		return instance;
	}

	private static DisplayHistoricUiBinder uiBinder = GWT.create(DisplayHistoricUiBinder.class);

	interface DisplayHistoricUiBinder extends UiBinder<Widget, DisplayHistoric> {
	}
	

	@UiField
	Button buttonCancel;

	@UiField
	Image buttonCancel2;

	@UiField
	InlineLabel labelTitre;
	
	@UiField
	VerticalPanel verticalPanel ;
	
	public DisplayHistoric() {
		initWidget(uiBinder.createAndBindUi(this));
		init();
		initTable();
	}
	
	private void init() {
		this.dialogBoxDetail = new DialogBox();
		this.dialogBoxDetail.add(this);
			   // this.dialogBoxDetail.add(new Label("aaaaa"));
		this.dialogBoxDetail.hide();
		this.buttonCancel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				dialogBoxDetail.hide();
				dataProvider.getList().clear();
			}
		});
		
		this.buttonCancel2.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialogBoxDetail.hide();
				dataProvider.getList().clear();
			}
		});
	}

	
	private void initTable() {
		CellTable<CallInfo> table = new CellTable<CallInfo>();
		// Create age column.
	    TextColumn<CallInfo> columnPhoneUser = new TextColumn<CallInfo>() {
	      @Override
	      public String getValue(CallInfo contact) {
	        return ""+contact.getPhoneUser();
	      }
	    };
	    columnPhoneUser.setSortable(true);
	    
		// Column Type
		 ImageCell typeCell = new ImageCell();
		 Column<CallInfo, String> typeColumn = new Column<CallInfo, String>(
		            typeCell) {
		          @Override
		          public String getValue(CallInfo object) {
		            String s = UtilPhoneLog.getTypeImage(object.getTypeCall());
		            		//"/images/call_"+object.getTypeCall()+".png";
		            return s;
		          }
		 };
		
		// Create age column.
	    TextColumn<CallInfo> columnDuration = new TextColumn<CallInfo>() {
	      @Override
	      public String getValue(CallInfo contact) {
	        return ""+contact.getDuration();
	      }
	    };
	    columnDuration.setSortable(true);
	    
	 // Create age column.
	    TextColumn<CallInfo> columnAge = new TextColumn<CallInfo>() {
	      @Override
	      public String getValue(CallInfo contact) {
	        return contact.getAge();
	      }
	    };
	    columnAge.setSortable(true);
	   
	    ////
	    TextColumn<CallInfo> columnComment = new TextColumn<CallInfo>() {
	        @Override
	        public String getValue(CallInfo contact) {
	          return contact.getComment();
	        }
	      };
	      
	      table.addColumn(columnAge, "Age");
	      table.addColumn(columnDuration, "Duration");
	      table.addColumn(columnPhoneUser,"");
	      table.addColumn(typeColumn, "Type");
	      
		  table.addColumn(columnComment, "Comment");

	      // Create a data provider.

	      // Connect the table to the data provider.
	      dataProvider.addDataDisplay(table);
	      verticalPanel.add(table);
	}
	

	public void setListCallInfo(List<CallInfo> list , String phoneNumber,String contactNAme) {
		
		this.labelTitre.setText(""+list.size()+" calls from "+phoneNumber+" "+contactNAme);
		this.dialogBoxDetail.setTitle("Historic "+phoneNumber+" "+contactNAme);
		
	      // Add the data to the data provider, which automatically pushes it to the
	      // widget.
	      dataProvider.getList().clear();
	      dataProvider.getList().addAll(list);
	}
	
	public void showRelativeTo( UIObject ui) {
		this.dialogBoxDetail.showRelativeTo(ui);
	}

}
