package bg.phonelog.client;

import java.util.Date;
import java.util.List;

import bg.phonelog.client.alert.Alert;
import bg.phonelog.client.calls.CallInfo;
import bg.phonelog.client.delegates.Delegate;
import bg.phonelog.client.delegates.InfosFacturesGroup;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	void greetServer(String input, AsyncCallback<String> callback) throws IllegalArgumentException;

	void getListCallInfo(String pass,int fromIndex,int length, AsyncCallback<List<CallInfo>> callback);

	void deleteCallInfo(String pass,List<Long> listId, AsyncCallback<Void> callback);

	void updateCallInfo(String pass,CallInfo callInfo, AsyncCallback<Void> callback);

	void getAlert(String pass,AsyncCallback<Alert> callback);

	void refreshListCallInfo(String pass,Date upToDate, AsyncCallback<List<CallInfo>> callback);

	void getHistoric(String pass, String phoneNumber, int fromIndex,
			int length, AsyncCallback<List<CallInfo>> callback);

	void addEmailDelegate(String email, AsyncCallback<Void> callback);

	
	void getListDelegates(AsyncCallback<List<Delegate>> callback);

	void deleteDelegates(List<Delegate> list, AsyncCallback<Void> callback);

	void getInfosFacturesGroup(AsyncCallback<InfosFacturesGroup> callback);

	
	
}
