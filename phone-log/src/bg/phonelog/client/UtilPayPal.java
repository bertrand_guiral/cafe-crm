package bg.phonelog.client;

public class UtilPayPal {

	public static final boolean isPaypalDebug = false;
	
	public static final String ACTION_PAYPAL_SANDBOX = "https://www.sandbox.paypal.com/cgi-bin/webscr";
	public static final String ACTION_PAYPAL_REAL = "https://www.paypal.com/cgi-bin/webscr";
	
	public static final String BUSINESS_SANDBOX = "bertrand.guiral.business@gmail.com";
	public static final String BUSINESS_REAL = "bertrand.guiral@gmail.com";
	
	                                        
}
