package bg.phonelog.client.crypt;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.Widget;

public class Crypto extends Composite  {

	private static CryptoUiBinder uiBinder = GWT.create(CryptoUiBinder.class);

	private  static Crypto instance ;
	
	
	public static Crypto getInstance() {
		if (instance == null){
			instance = new Crypto();
		}
		return instance;
	}





	interface CryptoUiBinder extends UiBinder<Widget, Crypto> {
	}

	private Crypto() {
		instance=this;	
		initWidget(uiBinder.createAndBindUi(this));
		
	}

	
	
	@UiField
	PasswordTextBox  textBox2;

	@UiField
	Button buttonLogin ;


	
	
	public Button getButtonLogin() {
		return buttonLogin;
	}




	public static  String getText() {
		if (instance == null){
			return "";
		}
		return instance.textBox2.getText();
	}
	
	

}
