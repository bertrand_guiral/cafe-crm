package bg.phonelog.client;

import bg.phonelog.client.alert.DisplayAlert;
import bg.phonelog.client.calls.JournalAppels;
import bg.phonelog.client.crypt.Crypto;
import bg.phonelog.client.delegates.DelegateGlobalUI;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Phone_log implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while " + "attempting to contact the server. Please check your network " + "connection and try again.";

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		RootPanel rootPanelCrypto = RootPanel.get("crypto");
		if (rootPanelCrypto != null) {
			final Crypto crypto = Crypto.getInstance();
			crypto.getButtonLogin().addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					crypto.getButtonLogin().setVisible(false);
					RootPanel.get("journal").add(JournalAppels.getInstance());
					RootPanel.get("alert").add(DisplayAlert.getInstance());
					DisplayAlert.getInstance().refreshAlert();
				}
			});
			rootPanelCrypto.add(crypto);
			JournalAppels.getInstance();
			DisplayAlert.getInstance();
		}
		RootPanel rootPanelDelegate = RootPanel.get("delegates");
		if (rootPanelDelegate != null){
			rootPanelDelegate.add(DelegateGlobalUI.getInstance());
			
		}
	}
}
