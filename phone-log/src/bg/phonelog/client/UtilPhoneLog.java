package bg.phonelog.client;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import bg.phonelog.client.calls.CallInfo;



import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.VerticalPanel;

public class UtilPhoneLog {

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	public static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	public static  String getAge(Date date) {
		long age =(System.currentTimeMillis() - date.getTime())/1000;
		if (age < 60){
			return "" + age +" s";
		}
		age = age/60;
		if (age <60){
			return age+" mn";
		}
		age = age/60;
		if (age < 48){
			return age +" h";
		}
		age =  age/24;
		return age+" d";
		
	}

	public static String getTypeImage(String typeCall) {		
		return "/images/call_"+typeCall+".png";
	}

	public static boolean isDebug() {
		return false;
	}

	public static void processExcell(List<CallInfo> list) {
		 //Window.alert("Excel process Debug -- list.size : "+list.size());
		 getform(list).submit();
			
	}
	static DateTimeFormat sdf = DateTimeFormat.getFormat("yyyy.MM.dd hh:mm:ss");
	private static  FormPanel getform(List<CallInfo> list) {
		FormPanel form = new FormPanel("_self");
		VerticalPanel vp = new VerticalPanel();
		vp.add(new Hidden("action", "excel"));
		int i=0;
		for(CallInfo call: list){
			vp.add(new Hidden("Date_"+i, ""+sdf.format(call.getDate())));
			vp.add(new Hidden("Comment_"+i, ""+com.google.gwt.http.client.URL.encode(call.getCommentOrMessage())));
			vp.add(new Hidden("ContactName_"+i, ""+com.google.gwt.http.client.URL.encode(call.getContactName())));
			vp.add(new Hidden("PhoneNumber_"+i, ""+com.google.gwt.http.client.URL.encode(call.getPhoneNumber())));
			vp.add(new Hidden("PhoneUser_"+i, ""+com.google.gwt.http.client.URL.encode(call.getPhoneUser())));
			vp.add(new Hidden("TypeCall_"+i, ""+call.getTypeCall()));
			vp.add(new Hidden("Duration_"+i, ""+call.getDuration()));
			i++;
		}
		vp.add(new Hidden("nb_lignes", ""+i));
		vp.add(new Hidden("colonne", "Date"));
		vp.add(new Hidden("colonne", "PhoneNumber"));
		vp.add(new Hidden("colonne", "PhoneUser"));
		vp.add(new Hidden("colonne", "Duration"));
		vp.add(new Hidden("colonne", "TypeCall"));
		vp.add(new Hidden("colonne", "ContactName"));
		vp.add(new Hidden("colonne", "Comment"));
		
		form.setAction(ACTION_EXCEL);
		form.setMethod(FormPanel.METHOD_POST);
		form.add(vp);
		return form;
	}

	static final String ACTION_EXCEL = "/phoneLog.csv";
	
	

}
