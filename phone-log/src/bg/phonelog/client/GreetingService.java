package bg.phonelog.client;

import java.util.Date;
import java.util.List;

import bg.phonelog.client.alert.Alert;
import bg.phonelog.client.calls.CallInfo;
import bg.phonelog.client.delegates.Delegate;
import bg.phonelog.client.delegates.InfosFacturesGroup;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	
	String greetServer(String name) throws IllegalArgumentException;
	
	List<CallInfo>  getListCallInfo(String pass, int fromIndex, int length) throws Exception;
	
	void deleteCallInfo(String pass,  List<Long> list) throws Exception;
	
	void updateCallInfo(String pass, CallInfo callInfo) throws Exception;;
	
	Alert getAlert(String pass ) throws Exception;
	
	List<CallInfo>  refreshListCallInfo(String pass,Date upToDate) throws Exception;
	
	List<CallInfo>  getHistoric(String pass,String phoneNumber ,int fromIndex,int length) throws Exception;
	
	void addEmailDelegate(String email) throws Exception;
	
	List<Delegate>  getListDelegates() throws Exception;
	
	void deleteDelegates(List<Delegate> list) throws Exception;
	
	InfosFacturesGroup getInfosFacturesGroup();

	
}
