package bg.phonelog.server.user;

import java.util.Collection;
import java.util.HashSet;
import java.util.StringTokenizer;

public class UtillData {

	public static Collection<String> toCollection(String s){
		Collection<String> c = new HashSet<String>();
		if (s==null){
			return c;
		}
		StringTokenizer st = new StringTokenizer(s,";");
		while(st.hasMoreTokens()){
			c.add(st.nextToken().trim());
		}
		return c;
	}

	public static String toString(Collection<String> listEmailDelegue) {
		String s ="";
		for(String e : listEmailDelegue){
			s += e+";";
		}
		return s;
	}
}
