package bg.phonelog.server.user;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import bg.phonelog.server.util.UtilPersistence;


public class UserFactory {

	private static UserFactory instance = new UserFactory();

	public static UserFactory getInstance() {
		return instance;
	}
	public User_SS2 getUser_SS(String email, String password){
		EntityManager em = UtilPersistence.createEntityManager();
		User_SS2 ad  = getUser_SS(em, email,password);
		em.close();
		return ad;

	}
	
	public User_SS2 getUser_SS(EntityManager em , String email, String password){
		Query query =  em.createQuery("select t from User_SS2 t where t.email = :email AND t.password = :password");
		query.setParameter("email", email.toLowerCase().trim());
		query.setParameter("password", password);
		List<User_SS2>  list = query.getResultList();
		if (list.size()==0){
			return null;
		}
		User_SS2 ad = list.get(0);
		return ad;

	}
	
	public User_SS2 getUser_SS( String email){
		EntityManager em = UtilPersistence.createEntityManager();
		User_SS2 ad  = getUser_SS(em, email);
		em.close();
		return ad;
	}
	public User_SS2 getUser_SS(EntityManager em , String email){
		Query query =  em.createQuery("select t from User_SS2 t where t.email = :email ");
		query.setParameter("email", email.trim().toLowerCase());
		List<User_SS2>  list = query.getResultList();
		if (list.size()==0){
			return null;
		}
		User_SS2 ad = list.get(0);
		return ad;

	}
	
	public void register(User_SS2 user_SS) throws Exception{
		List<Delegate_SS> list = DelegateFactory.getInstance().getListDelegatesByEmail(user_SS.getEmail());
		createUser(user_SS,list);
		
	}
	
	private void createUser(User_SS2 user_SS, List<Delegate_SS> listDelegates) throws Exception{
		EntityManager em = UtilPersistence.createEntityManager();
		try {
			String email= user_SS.getEmail();
			if (null == getUser_SS(em, email)) {
				if (listDelegates == null){
					
				}else {
					for(Delegate_SS d : listDelegates){
						Date dateOK = d.getDateAbonnementOK();
						user_SS.setDateAbonnementOK(dateOK,User_SS2.PAYMENT_BY_DELEGATE);
						user_SS.setDatePaiementAbonnementDelegate(d.getDatePaiement());
					}
				}
				em.persist(user_SS);
				
			} else {
				throw new Exception(" Email : "+email+" already registered !! " );
			}
		} finally{
			em.close();
		}
		
		
	}


	public void updateUser(User_SS2 user_SS) throws Exception{
		EntityManager em = UtilPersistence.createEntityManager();
		try {
			em.merge(user_SS);
			em.persist(user_SS);			
		} finally{
			em.close();
		}
		
		
	}
	
	public String doCleanAbonnementDebug() {
		String s ="doCleanAbonnementDebug";
		s +="<ul>";
		s +=  doCleanAbonnementDebug("bertrand.guiral@gmail.com");
		s += doCleanAbonnementDebug("bertrand.guiral.2@gmail.com");
		s += doCleanAbonnementDebug("bertrand.guiral.3@gmail.com");
		s += doCleanAbonnementDebug("bertrand.guiral.4@gmail.com");
		s +="</ul>";
		
		return s;
	}
	private String doCleanAbonnementDebug( String email) {
		EntityManager em = UtilPersistence.createEntityManager();
		em.getTransaction().begin();
		
		String s="  "+email;
		User_SS2 user = UserFactory.getInstance().getUser_SS(em, email) ;
		s+=" "+user;
		if (user != null){
			user.setDateAbonnementOK(null);
			user.setDatePaiementAbonnementDelegate(null);
			em.persist(user);
		}
		em.getTransaction().commit();
		em.close();
		s += DelegateFactory.getInstance().doCleanAbonnement( email);
		return "<li> "+s+"</li>";
	}
	
	
	public List<User_SS2> getListUsers(EntityManager em) {
		Query query =  em.createQuery("select from User_SS2");
		List<User_SS2>  list = query.getResultList();
		return list;
	}
	
	public String utilMigration() {
		EntityManager em = UtilPersistence.createEntityManager();
		String s="";
		Query query =  em.createQuery("select from User_SS");
		List<User_SS>  list = query.getResultList();
		s+= "List "+list.size();
		for(User_SS u : list){
			User_SS2 uNew = u.get_U_New();
			//em.persist(uNew);
		}
		em.close();
		return s;
	}
	
}
