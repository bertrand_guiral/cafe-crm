package bg.phonelog.server.user;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;



/**
 * Un administrateur possede un compte "Mon Timbre en Ligne" . Il peut deleguer des droits a des collaborateurs qui pourront utiliser son compte "Mon timbre en ligne"
 * @author Bertrand
 *
 */ 
@Entity
public class User_SS implements Serializable {

	private static final long serialVersionUID = 4L;
	private static final long DUREE_PERIODE_ESSAI_jours = 5;
	private static final long DUREE_PERIODE_ESSAI_ms = DUREE_PERIODE_ESSAI_jours*24*60*60*1000;
	private static final long DUREE_DAY_ms = 24l*60l*60l*1000l;
	private static final long DUREE_MOIS_ms = 30*DUREE_DAY_ms;
	public static final int PAYMENT_BY_ABONNEMENT = 1;
	public static final int PAYMENT_BY_DELEGATE = 2; 
	
	
	/*
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Key id;
	*/
	
	@Id
	private String email;
	
	private String nom;
	
	private String password;
	
	
	@Transient
	private int statusPayment;
	@Transient
	private String statusLabel;
	@Transient
	private boolean displayPaypal = true;
	
	
	private Date dateAbonnementOK ;
	private Date datePaiementAbonnementDelegate;
	
	private Integer type_payment = 0;

	private Date date = new Date();
	private Boolean daylyEmailable2 = false;
	
	public User_SS() {
		super();
	}
	

	public User_SS(String email,  String nom, String password) {
		super();
		this.email = email;
		this.nom = nom;
		this.password = password;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email.toLowerCase();
	}



	

	
	public String toString(){
		return ""+this.email+" "+nom+" "+"  dateAbonnementOK :"+dateAbonnementOK;
	}


	

	public Date getDateAbonnementOK() {
		return dateAbonnementOK;
	}


	public void setDateAbonnementOK(Date dateAbonnementOK) {
		this.dateAbonnementOK = dateAbonnementOK;
	}


	public int getStatusPayment() {
		
		return statusPayment;
	}


	
	public String getStatusLabel() {		
		return getStatusLabel(getStatusPayment());
	}


	private String getStatusLabel(int statusPayment2) {
		return "Statu "+getStatusPayment();
	}


	public boolean isDisplayPaypal() {
		return !isAbonnementOK();
	}
	
	public boolean isDisplayPhoneLog() {
		if (isAbonnementOK()){
			return true;
		}else if (isPeriodeEssai()){
			return true;
		}else {
			return false;
		}
	}
	
	
	

	

	public boolean isAbonnementOK(){
		if ( this.dateAbonnementOK == null) {
			return false;
		}
		return this.dateAbonnementOK.after(new Date());
	}
	
	public boolean isPeriodeEssai(){
		long timeEndPeriodeEssai =  this.date.getTime()+DUREE_PERIODE_ESSAI_ms;
		Date dateEndPeriodEssai = new Date(timeEndPeriodeEssai);
		return dateEndPeriodEssai.after(new Date());
	}

	public Date getDateMaxPaiementMasse() {
		if (datePaiementAbonnementDelegate  == null){
			datePaiementAbonnementDelegate = new Date();
		}
		Date d = new Date(datePaiementAbonnementDelegate.getTime()+DUREE_MOIS_ms);
		Calendar c = GregorianCalendar.getInstance();
		c.setTime(datePaiementAbonnementDelegate);
		c.add(Calendar.MONTH, 1);
		
		return c.getTime();
	}
	
	
	
	
	public Date getDatePaiementAbonnementDelegate() {
		return datePaiementAbonnementDelegate;
	}


	public void setDatePaiementAbonnementDelegate(Date datePaiementAbonnementDelegate) {
		this.datePaiementAbonnementDelegate = datePaiementAbonnementDelegate;
	}


	public void setDateAbonnementOK(Date dateAbonnementOK2, int paymentByAbonnement) {
		this.setDateAbonnementOK(dateAbonnementOK2);
		this.type_payment = paymentByAbonnement;
	}


	public Integer getType_payment() {
		return type_payment;
	}


	public void setType_payment(int type_payment) {
		this.type_payment = type_payment;
	}


	
	public boolean isDaylyEmailable2() {
		if (this.daylyEmailable2 == null){
			return false;
		}
		return daylyEmailable2;
	}


	public void setDaylyEmailable2(boolean daylyEmailable) {
		this.daylyEmailable2 = daylyEmailable;
	}


	public User_SS2 get_U_New() {
		User_SS2 u = new User_SS2();
		u.setNom(nom);
		u.setEmail(email);
		u.setDate(date);
		u.setDateAbonnementOK(dateAbonnementOK);
		u.setPassword(password);
		u.setType_payment(type_payment);
		return u;
	}


};
