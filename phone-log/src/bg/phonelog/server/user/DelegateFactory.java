package bg.phonelog.server.user;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import bg.phonelog.server.util.UtilPersistence;

public class DelegateFactory {

	private static DelegateFactory instance = new DelegateFactory();
	private static final Logger log = Logger.getLogger(DelegateFactory.class.getName());

	public static DelegateFactory getInstance() {
		return instance;
	}
	
	public List<Delegate_SS>  getListDelegates(String emailAdmin){
		EntityManager em = UtilPersistence.createEntityManager();
		List<Delegate_SS>  list = getListDelegates(em,emailAdmin);
		list.size();
		em.close();
		return list;
	}

	private List<Delegate_SS> getListDelegates(EntityManager em, String emailAdmin) {
		Query query =  em.createQuery("select t from Delegate_SS t where t.emailAdmin = :emailAdmin");
		query.setParameter("emailAdmin", emailAdmin.toLowerCase().trim());
		List<Delegate_SS>  list = query.getResultList();
		return list;
	}
	
	public void persist(Delegate_SS bfc) throws Exception{
		EntityManager em = UtilPersistence.createEntityManager();
		em.persist(bfc);
		em.close();
	}
	
	public List<Delegate_SS>  getListDelegatesByEmail(String email){
		EntityManager em = UtilPersistence.createEntityManager();
		List<Delegate_SS>  list = getListDelegatesByEmail(em,email);
		em.close();
		return list;
	}
	
	private List<Delegate_SS> getListDelegatesByEmail(EntityManager em, String emailDelegate) {
		Query query =  em.createQuery("select t from Delegate_SS t where t.emailDelegate = :emailDelegate");
		query.setParameter("emailDelegate", emailDelegate.toLowerCase().trim());
		List<Delegate_SS>  list = query.getResultList();
		return list;
	}
	
	public Date getTimeAbonnementOkMax(EntityManager em, String emailDelegate){
		List<Delegate_SS> list =  getListDelegatesByEmail( em,  emailDelegate) ;
		Date date = null;
		if (list == null){
			
		}else {
			for(Delegate_SS d :list){
				Date dateDelegate;
				if(d.getDatePaiement()==null){
					dateDelegate = d.getDateCreation();
				}else {
					dateDelegate = d.getDatePaiement();
				}
				date = max(date,dateDelegate);
			}
		}
		Date dateMax = add(date, Calendar.MONTH,1);
		return dateMax;
	}

	public static  Date add(Date date, int field, int amount) {
		if (date == null){
			return null;
		}
		Calendar c = GregorianCalendar.getInstance();
		c.setTime(date);
		c.add(field, amount);
		return c.getTime();
	} 

	private Date max(Date date1, Date date2) {
		if (date1 == null){
			return date2;
		}
		if (date2 == null){
			return date1;
		}
		if (date1.getTime() > date2.getTime()){
			return date1;
		}else {
			return date2;
		}
	}

	public void delete(String email, String email2) {
		EntityManager em = UtilPersistence.createEntityManager();
		em.getTransaction().begin();
		delete(em, email, email2);
		em.getTransaction().commit();
		em.close();
	}

	private void delete(EntityManager em, String emailAdmin, String emailDelegate) {
		//Delegate_SS d = new Delegate_SS(emailAdmin, emailDelegate, new Date());
		//em.merge(d);
		Delegate_SS  d = getDelegate2(em, emailAdmin, emailDelegate);
		if (d != null){
			em.remove(d);
		}
	}

	private Delegate_SS getDelegate2(EntityManager em, String emailAdmin, String emailDelegate) {
		Query query =  em.createQuery("select t from Delegate_SS t where t.emailAdmin = :emailAdmin and  t.emailDelegate = :emailDelegate");
		query.setParameter("emailAdmin", emailAdmin.toLowerCase().trim());
		query.setParameter("emailDelegate", emailDelegate.toLowerCase().trim());
		List<Delegate_SS>  list = query.getResultList();
		if (list == null){
			return null;
		}else if (list.size()== 0){
			return null;
		}else {
			return list.get(0);
		}
		
	}
	
	

	public String  doCleanAbonnement( String email) {
		String s ="Delegate  <ul>";
		int i=0;
		 try {
			List<Delegate_SS>  list1 = getListDelegates( email);
			 for (Delegate_SS d :list1){
				 s+= "<li> "+d+"</li>";
				 delete(d.getEmailAdmin(), d.getEmailDelegate());
				 i++;
			 }
			 
			 List<Delegate_SS>  list2 = getListDelegatesByEmail( email);
			 for (Delegate_SS d :list2){
				 s+= "<li> "+d+"</li> ";
				 delete(d.getEmailAdmin(), d.getEmailDelegate());
				 i++;
			 }
		} catch (Exception e) {
			log.log(Level.SEVERE, "DelegateFactory.doCleanAbonnement "+email,e);
			s+= "EXCEPTION "+e.getMessage();
		}
		 s += "</ul>";
		 s+= " : "+i+ " Delegates Removed ";
		 
		return s;
	}

	public void setPaiementDate(User_SS2 user, Date date, Date dateAbonnementOK) {
		EntityManager em = UtilPersistence.createEntityManager();
		List<Delegate_SS> list = getListDelegates(em,user.getEmail());
		for(Delegate_SS dd :list){
			dd.setDatePaiement(date);
			
			dd.setDateAbonnementOK(dateAbonnementOK);
			User_SS2 user_delegate = UserFactory.getInstance().getUser_SS(dd.getEmailDelegate());
			if (user_delegate != null){
				user_delegate.setDateAbonnementOK(dateAbonnementOK,User_SS2.PAYMENT_BY_DELEGATE);
				em.merge(user_delegate);
				em.persist(user_delegate);
			}
			em.merge(dd);
			em.persist(dd);
		}
		em.close();
	}

	
	private boolean isAbonnementOK(String email) {
		this.getListDelegatesByEmail(email);
		return false;
	}

	
	


}
