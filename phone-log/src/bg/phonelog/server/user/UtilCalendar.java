package bg.phonelog.server.user;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class UtilCalendar {

	public static void main(String[] s){
		System.out.println(getNextMonth_First());
		System.out.println(getNextOfMonth(25));
	}
	public static Date getNextMonth_First() {
		Calendar c = GregorianCalendar.getInstance();
		c.add(Calendar.MONTH, 1);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY,0);
		c.set(Calendar.MINUTE,0);
		c.set(Calendar.SECOND,0);
		
		Date d = c.getTime();
		return d;
	}
	
	public static Date getNextOfMonth(int day) {
		Calendar c = GregorianCalendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, day);
		Date d = c.getTime();
		return d;
	}
}
