package bg.phonelog.server.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Delegate_SS implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String emailAdmin;
	private String emailDelegate;
	private Date dateCreation;
	private Date datePaiement;
	private Date dateAbonnementOK;
	
	
	public Delegate_SS() {
		super();
	}
	
	public Delegate_SS(String emailAdmin, String emailDelegate) {
		new Delegate_SS(emailAdmin,emailDelegate,new Date());
	}
		
	
	
	public Delegate_SS(String emailAdmin, String emailDelegate, Date dateCreation) {
		super();
		this.emailAdmin = emailAdmin;
		this.emailDelegate = emailDelegate;
		this.dateCreation = dateCreation;
		updateId();
	}
	
	private void updateId() {
		if (this.id==null){
			id = this.emailAdmin+";"+emailDelegate;
		}
	}


	public String getEmailAdmin() {
		return emailAdmin;
	}
	public void setEmailAdmin(String emailAdmin) {
		this.emailAdmin = emailAdmin;
	}
	public String getEmailDelegate() {
		return emailDelegate;
	}
	public void setEmailDelegate(String emailDelegate) {
		this.emailDelegate = emailDelegate;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public Date getDatePaiement() {
		return datePaiement;
	}
	public void setDatePaiement(Date datePaiement) {
		this.datePaiement = datePaiement;
	}



	



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Delegate_SS [id=" + id + ", emailAdmin=" + emailAdmin + ", emailDelegate=" + emailDelegate + ", dateCreation=" + dateCreation + ", datePaiement=" + datePaiement + "]";
	}

	public Date getDateAbonnementOK() {
		return dateAbonnementOK;
	}

	public void setDateAbonnementOK(Date dateAbonnementOK) {
		this.dateAbonnementOK = dateAbonnementOK;
	}

	

	
	

	

}
