package bg.phonelog.server;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.phonelog.server.call.BeanUtil;
import bg.phonelog.server.user.UserFactory;
import bg.phonelog.server.user.User_SS2;
import bg.phonelog.server.util.UtilMail;

public class FilterLogin implements  Filter{

	private static final Logger log = Logger.getLogger(FilterLogin.class.getName());

	@Override
	public void destroy() {
		
	}

	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
	
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		String action = request.getParameter("action");
		if (action == null){
			action ="";
		}
		log.warning(" doFilter.action : "+action);
		BeanUtil beanUtil = (BeanUtil) req.getAttribute("util");
		if (beanUtil == null){
			beanUtil = new BeanUtil();
			req.setAttribute("util",beanUtil);
			beanUtil.setLog("");
		}
		String page_target = "page_login.jsp";
		if (action.equals("login")){
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			User_SS2 user = UserFactory.getInstance().getUser_SS(email, password);
			beanUtil.setEmail(email);
			if(user == null){
				beanUtil.setLog("No user for this password and email");
				log.warning(" No user for this password and email: "+email);
			}else {
				beanUtil.setLog("login ok Welcome "+user.getNom()+" "+user.getEmail());
				request.getSession().setAttribute("user", user);
				req.setAttribute("connected",""+ (user!=null));
				req.setAttribute("user", user);				
				page_target = "/controller?action=displayJournal";
				log.warning(" login ok : "+email+" | page_target :"+page_target);
			}
		}else if (action.equals("passwordForgottenRequest")){
			page_target = "page_passwordforgotten.jsp";
		}else if (action.equals("passwordforgotten")){
			String email = request.getParameter("email");
			User_SS2 user = UserFactory.getInstance().getUser_SS( email);
			if(user == null){
				beanUtil.setLog("No User known for email: "+email);
				page_target = "page_passwordforgotten.jsp";
			}else {
				UtilMail.sendMessage("Phone Log", "Your password :"+user.getPassword(), email);
				beanUtil.setLog("A message has been send to the email: "+email+". Check yours mails");
			}
		}else if (action.equals("logout")) {
				request.getSession().invalidate();
				req.setAttribute("connected",""+ false);
				req.setAttribute("user", null);
				
				page_target = "page_welcome.jsp";
		}else if (action.equals("registry")){
			String email = request.getParameter("email");
			String nickname = request.getParameter("nickname");
			beanUtil.setEmail(email);
			beanUtil.setNickname(nickname);
			String password = request.getParameter("password");
			String password2 = request.getParameter("password2");
			
			CheckPassword checkPassword = new CheckPassword(email,password, password2);
			
			beanUtil.setLog("checkPassword "+checkPassword.isOK);
			if (checkPassword.isOK){
				try {
					User_SS2 user = new User_SS2(email, nickname, password);
					UserFactory.getInstance().register(user);
					page_target = "/controller?action=displayJournal";
					beanUtil.setLog("Registry ok");
				} catch (Exception e) {
					beanUtil.setLog(""+e.getMessage());
					page_target = "page_registry.jsp";
				}				
			}else {
				beanUtil.setLog(checkPassword.comment);
				page_target ="page_registry.jsp";
			}
			
		}else if (action.equals("registryReRequest")){
			page_target ="page_registry.jsp";
		}else if (action.equals("loginRequest")){
			page_target ="page_login.jsp";
		}
		log.warning("login done page_target : "+page_target);
		RequestDispatcher rd = request.getRequestDispatcher(page_target);
		rd.forward(request, response);
	}
}


class CheckPassword{
	public CheckPassword(String email, String password1, String password2) {
		init(email,password1,password2);
	}
	public CheckPassword(User_SS2 user, String password, String newPassword_1, String newPassword_2) {
		this(user.getEmail(),newPassword_1,newPassword_2);
		if(!user.getPassword().equals(password)){
			isOK = false;
			this.comment+=" Password not ok";
		}
	}
	boolean isOK = true;
	String comment ="";
	
	private void init(String email, String password1, String password2){
		if(email==null){
			isOK=false;
			comment +=" No email";
			return;
		}
		if (email.indexOf("@")< 2){
			isOK = false;
			comment +=" Email must be e-mail !!";
		}
		if(password1==null){
			isOK=false;
			comment +=" Password 1 is null;";
			return;
		}
		if(password2==null){
			isOK=false;
			comment +=" Password 2 is null;";
			return;
		}
		if(!password1.equals(password2)){
			isOK=false;
			comment +=" Passwords are not the identical;";
		}
		if (password1.length()<5){
			isOK=false;
			comment +=" Password must have more than 6 characters";
		}
		if (password1.length()>25){
			isOK=false;
			comment +=" Password must have less than 6 characters";
		}
	}
}
