package bg.phonelog.server;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import bg.phonelog.server.call.BeanUtil;
import bg.phonelog.server.user.UserFactory;
import bg.phonelog.server.user.User_SS2;

public class FilterOne implements  Filter{

	public FilterOne()  {
		
	}
	
	private static final Logger log = Logger.getLogger(ServletController.class.getName());


	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		//HttpServletRequest request = (HttpServletRequest)req;
		updateUser(request);
		User_SS2 user = (User_SS2) request.getSession().getAttribute("user");
		BeanUtil beanUtil = new BeanUtil(user);
		req.setAttribute("user", user);
		
		req.setAttribute("connected",""+ (user!=null));
		req.setAttribute("href_logout","/login?action=logout");
		req.setAttribute("util", beanUtil);
		chain.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		ResourceBundle.getBundle("bg/phonelog/BgStrings");
	}
	
	private void updateUser(HttpServletRequest request) {
		// Il faut mettre a jour le user_ss car les infos d'abonnement peuvent etre obsolete dans la session
		User_SS2 user_old = (User_SS2) request.getSession().getAttribute("user");
		User_SS2 user = null;
		if (user_old != null){
			user = UserFactory.getInstance().getUser_SS(user_old.getEmail());
			request.setAttribute("user", user);
			request.getSession().setAttribute("user", user);
		}
		log.warning("FilterOne update  user :  "+user);
	}

	
}
