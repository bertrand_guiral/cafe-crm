package bg.phonelog.server.call;

import java.text.SimpleDateFormat;
import java.util.Date;



public class Comment {

	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss" );
	String text;
	Date date = new Date();
	private String dateStr;
	public Comment() {
		
	}
	
	
	public Comment(String text) {
		super();
		this.text = text;
	}


	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}


	public Date getDate() {
		return date;
	}


	public String getDateStr() {
		return simpleDateFormat.format(date);
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	

}
