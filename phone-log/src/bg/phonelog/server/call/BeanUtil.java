package bg.phonelog.server.call;

import java.util.List;

import bg.phonelog.client.UtilPayPal;
import bg.phonelog.server.user.DelegateFactory;
import bg.phonelog.server.user.Delegate_SS;
import bg.phonelog.server.user.User_SS2;



public class BeanUtil {

	
	private String nickname;
	private String email;
	private String business;
	
	private String log;
	@Deprecated
	private List<Delegate_SS>  listDelegate;
	private String urlPaypal;
	public BeanUtil() {
	}

	public BeanUtil(User_SS2 user) {
		if(user != null){
			this.nickname= user.getNom();
			this.email = user.getEmail();
		}
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}
	
	
	int nbDelegates=-1;
	public int getNbDelegates______() {
	
		return -17;
	}

	public List<Delegate_SS> getListDelegate_____() {
		if (listDelegate == null){
			listDelegate=DelegateFactory.getInstance().getListDelegates(email);
		}
		return listDelegate;
	}

	private String priceDelegates____;
	
	public double getPriceDelegates_____() {
		return 5d;
	}

	public double getPriceAMonth() {
		return 2.0d;
	}

	public String getUrlPaypal() {
		if (UtilPayPal.isPaypalDebug){
			return UtilPayPal.ACTION_PAYPAL_SANDBOX;
		}else {
			return UtilPayPal.ACTION_PAYPAL_REAL;
		}
	}

	public String getBusiness() {
		if (UtilPayPal.isPaypalDebug){
			return UtilPayPal.BUSINESS_SANDBOX;
		}else {
			return UtilPayPal.BUSINESS_REAL;
		}
	}

	
	
	
}
