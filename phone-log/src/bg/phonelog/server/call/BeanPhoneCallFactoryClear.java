package bg.phonelog.server.call;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import bg.crypt.StringDeEncrypter;
import bg.crypt.StringEncrypter;
import bg.phonelog.server.util.UtilPersistence;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.ShortBlob;

public class BeanPhoneCallFactoryClear {

	public static final int MAX_RESULT = 100;

	private static final Logger log = Logger.getLogger(BeanPhoneCallFactoryClear.class.getName());

	public BeanPhoneCallFactoryClear(String passPhrase) {
		if (this.passPhrase == null){
			passPhrase ="";
		}
		this.passPhrase = passPhrase;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#persist(bg.phonelog.server
	 * .call.BeanPhoneCall)
	 */
	public void persist(BeanPhoneCall bfc) throws Exception {
		BeanPhoneCallEntityCrypted2 entity = bfc.getBeanPhoneCallEntityCrypted(this.getEncrypter());
		EntityManager em = UtilPersistence.createEntityManager();
		em.persist(entity);
		em.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#getBeanPhoneCall(java.lang
	 * .String)
	 */

	public BeanPhoneCall getBeanPhoneCall(String email) throws Exception {
		EntityManager em = UtilPersistence.createEntityManager();
		BeanPhoneCall bpc = getBeanPhoneCall(em, email);
		em.close();
		return bpc;
	}

	private List<BeanPhoneCallEntityCrypted2> getListBeanPhoneCall(EntityManager em, String email, int fromIndex_, int length) {
		String queryStr;
		queryStr = "select t from BeanPhoneCallEntityCrypted2 t where t.email = :email order by t.date DESC";

		Query query = em.createQuery(queryStr);
		int fromIndex = fromIndex_;
		if (fromIndex < 0) {
			fromIndex = 0;
		}

		query.setFirstResult(fromIndex);
		query.setMaxResults(length);

		query.setParameter("email", email.toLowerCase().trim());
		List<BeanPhoneCallEntityCrypted2> list = query.getResultList();
		if (list.size() == 0) {
			return new ArrayList<BeanPhoneCallEntityCrypted2>();
		}
		log.log(Level.INFO, "bg2 getListBeanPhoneCall2 fromIndex : " + fromIndex_ + "  length :" + length + "  return list.size :" + list.size());
		return list;

	}

	private BeanPhoneCall getBeanPhoneCall(EntityManager em, String email) throws Exception {
		String queryStr;
		queryStr = "select t from BeanPhoneCallEntityCrypted2 t where t.email = :email ";
		queryStr = "select t from BeanPhoneCallEntityCrypted2 t where t.email = :email order by t.date DESC";
		Query query = em.createQuery(queryStr);
		// query.ddSort("lastName", SortDirection.ASCENDING);
		query.setMaxResults(2);
		query.setParameter("email", email.toLowerCase().trim());
		List<BeanPhoneCallEntityCrypted2> list = query.getResultList();
		if (list.size() == 0) {
			return null;
		}
		BeanPhoneCall ad = list.get(0).getBeanPhoneCall(getDecrypter());
		return ad;

	}

	String passPhrase;
	StringDeEncrypter decrypter;
	StringEncrypter encrypter;

	private StringEncrypter getEncrypter() {
		if (encrypter == null) {
			encrypter = new StringEncrypter(passPhrase);
		}
		return encrypter;
	}

	private StringDeEncrypter getDecrypter() {
		if(passPhrase == null){
			return null;
		} else if(passPhrase.length() ==  0){
			return null;
		}else if (decrypter == null) {
			decrypter = new StringDeEncrypter(passPhrase);
		}
		return decrypter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#getBeanPhoneCall(java.lang
	 * .String, java.lang.String)
	 */

	public BeanPhoneCall getBeanPhoneCall(String email, String idStr) throws Exception {
		EntityManager em = UtilPersistence.createEntityManager();
		Long id = Long.parseLong(idStr);
		BeanPhoneCall ad = getBeanPhoneCall_(em, email, id);
		em.close();
		return ad;
	}

	private BeanPhoneCall getBeanPhoneCall_(EntityManager em, String email, Long id) throws Exception {
		if (id == null) {
			return null;
		}
		if (id == 0) {
			return null;
		}
		BeanPhoneCallEntityCrypted2 bpc = getBeanPhoneCallEntityCrypted(em, email, id);
		if (bpc == null) {
		} else {
			if (!bpc.getEmail().equals(email)) {
				bpc = null;
			}
		}
		return bpc.getBeanPhoneCall(getDecrypter());
	}

	private BeanPhoneCallEntityCrypted2 getBeanPhoneCallEntityCrypted(EntityManager em, String email, Long id) throws Exception {
		if (id == null) {
			return null;
		}
		if (id == 0) {
			return null;
		}
		BeanPhoneCallEntityCrypted2 bpc = em.find(BeanPhoneCallEntityCrypted2.class, id);
		if (bpc == null) {
		} else {
			if (!bpc.getEmail().equals(email)) {
				bpc = null;
			}
		}
		return bpc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#getListBeanPhoneCallByNumber
	 * (java.lang.String, java.lang.String, int)
	 */

	public List<BeanPhoneCall> getListBeanPhoneCallByNumber(String email, String phoneNumber, int index) {
		EntityManager em = UtilPersistence.createEntityManager();
		List<BeanPhoneCallEntityCrypted2> list = getListBeanPhoneCallByNumber(em, email, phoneNumber, index);
		em.close();
		return uncryptList(list);
	}

	private List<BeanPhoneCallEntityCrypted2> getListBeanPhoneCallByNumber(EntityManager em, String email, String phoneNumber, int fromIndex) {
		String queryStr;
		queryStr = "select t from BeanPhoneCallEntityCrypted2 t where t.email = :email ";
		queryStr = "select t from BeanPhoneCallEntityCrypted2 t where t.email = :email and t.phoneNumber = :phoneNumber   order by t.date DESC";

		Query query = em.createQuery(queryStr);
		query.setMaxResults(MAX_RESULT);
		query.setParameter("email", email.toLowerCase().trim());
		query.setParameter("phoneNumber", phoneNumber.trim());
		if (fromIndex < 0) {
			fromIndex = 0;
		}
		query.setFirstResult(fromIndex);
		List<BeanPhoneCallEntityCrypted2> list = query.getResultList();
		if (list.size() == 0) {
			return new ArrayList<BeanPhoneCallEntityCrypted2>();
		}
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#removePhoneCall(java.lang
	 * .String, java.lang.String)
	 */

	public void removePhoneCall(String email, String idStr) throws Exception {
		Long id = Long.parseLong(idStr);
		removePhoneCall(email, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#removePhoneCall(java.lang
	 * .String, java.lang.Long)
	 */

	public void removePhoneCall(String email, Long id) throws Exception {
		EntityManager em = UtilPersistence.createEntityManager();
		em.getTransaction().begin();
		BeanPhoneCallEntityCrypted2 ad = getBeanPhoneCallEntityCrypted(em, email, id);
		em.remove(ad);
		em.getTransaction().commit();
		em.close();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#removePhoneCall(java.lang
	 * .String, java.util.List)
	 */

	public void removePhoneCall(String email, List<Long> listId) throws Exception {
		for (Long id : listId) {
			removePhoneCall(email, id);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#addComment(java.lang.String,
	 * long, java.lang.String)
	 */

	public BeanPhoneCall addComment(String email, long id, String comment) throws Exception {
		EntityManager em = UtilPersistence.createEntityManager();
		BeanPhoneCall b = getBeanPhoneCall_(em, email, id);
		b.setComment(comment);
		em.persist(b);
		em.close();
		return b;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#getListBeanPhoneCall(java
	 * .lang.String, int)
	 */

	public List<BeanPhoneCall> getListBeanPhoneCall(String email, int fromIndex) {
		return getListBeanPhoneCall(email, fromIndex, 100);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#getListBeanPhoneCall(java
	 * .lang.String, int, int)
	 */

	public List<BeanPhoneCall> getListBeanPhoneCall(String email, int fromIndex, int length) {
		EntityManager em = UtilPersistence.createEntityManager();
		List<BeanPhoneCallEntityCrypted2> list = getListBeanPhoneCall(em, email, fromIndex, length);
		em.close();
		return uncryptList(list);
	}

	private List<BeanPhoneCall> uncryptList(List<BeanPhoneCallEntityCrypted2> list) {
		List<BeanPhoneCall> listClear = new ArrayList<BeanPhoneCall>();
		//log.warning("uncryptList listCrypted : " + list);
		log.warning("uncryptList listCrypted size : " + list.size());
		for (BeanPhoneCallEntityCrypted2 b : list) {
			try {
				BeanPhoneCall bClear = b.getBeanPhoneCall(getDecrypter());
				listClear.add(bClear);
			} catch (Exception e) {
				// log.warning("Exception uncryptList b: "+b);
				// log.warning("Exception uncryptList message: "+e.getMessage());
				log.log(Level.WARNING, "Bg_Exception uncryptList :", e);
			}
		}
		return listClear;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#updateComment(java.lang.
	 * String, java.lang.Long, java.lang.String)
	 */

	public void updateComment(String email, Long id, String comment) throws Exception {
		EntityManager em = UtilPersistence.createEntityManager();
		em.getTransaction().begin();
		BeanPhoneCallEntityCrypted2 bean = getBeanPhoneCallEntityCrypted(em, email, id);
		byte[] bComments = this.getEncrypter().encrypt(comment);
		bean.setbComment(new Blob(bComments));
		em.persist(bean);
		em.getTransaction().commit();
		em.close();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * bg.phonelog.server.call.BeanPhoneCallFactory#refreshListCallInfo(java
	 * .lang.String, java.util.Date)
	 */

	public List<BeanPhoneCall> refreshListCallInfo(String email, Date upToDate) {
		EntityManager em = UtilPersistence.createEntityManager();
		String queryStr;
		queryStr = "select t from BeanPhoneCallEntityCrypted2 t where t.email = :email  order by t.date DESC";
		queryStr = "select t from BeanPhoneCallEntityCrypted2 t where t.email = :email   and t.date > :date  order by t.date DESC";
		Query query = em.createQuery(queryStr);
		int fromIndex = 0;

		query.setFirstResult(fromIndex);
		query.setMaxResults(50);

		query.setParameter("email", email.toLowerCase().trim());
		query.setParameter("date", upToDate);
		List<BeanPhoneCallEntityCrypted2> list = query.getResultList();
		if (list.size() == 0) {
			list = new ArrayList<BeanPhoneCallEntityCrypted2>();
		}
		log.log(Level.INFO, "bg2 refreshListCallInfo email : " + email + "  upToDate " + upToDate + "    return list.size :" + list.size());

		em.close();
		return uncryptList(list);
	}

	public void updatePhoneCall(String emailUser, String phoneUser, String number, Date date, Long duration, Integer type, String cryptPhrase, String contactName, String comment,String messageSms) throws Exception {
		EntityManager em = UtilPersistence.createEntityManager();
		String queryStr;
		try {
			queryStr = "select t from BeanPhoneCallEntityCrypted2 t where t.email = :email and t.date = :date  ";
			Query query = em.createQuery(queryStr);
			query.setMaxResults(5);

			query.setParameter("email", emailUser.toLowerCase().trim());
			query.setParameter("date", date);
			List<BeanPhoneCallEntityCrypted2> list = query.getResultList();
			if (list.size() == 0) {
				list = new ArrayList<BeanPhoneCallEntityCrypted2>();
			}
			log.log(Level.INFO, "bg2 getBeanPhoneCall Date : " + date + "    return list.size :" + list.size());

			if (list.size() == 0) {
				// creer un appel . On a rater la notification de l'appel
				log.warning("BeanPhoneCall non trouve pour emailUser et date " + emailUser + " " + date + " Creation d'un phoneCall (Cela est un update et non un log et est donc anormal)");
				BeanPhoneCall beanPhoneCall = new BeanPhoneCall(emailUser, number, contactName, date, duration, type, phoneUser,messageSms);
				persist(beanPhoneCall);

				// throw new
				// Exception("updatePhoneCall no call found !! emailUser:"+emailUser);
			} else {
				BeanPhoneCallEntityCrypted2 bpcEncrypted = list.get(0);
				BeanPhoneCall pfc = bpcEncrypted.getBeanPhoneCall(this.getDecrypter());
				if (!pfc.phoneNumber.equals(number)) {
					throw new Exception("PhoneNumber wrong! emailUser:" + emailUser);
				}
				if (!pfc.phoneUser.equals(phoneUser)) {
					throw new Exception("PhoneUser wrong! emailUser:" + emailUser);
				}
				if (pfc.getType() != type) {
					throw new Exception("type wrong");
				}
				if (pfc.duration != duration) {
					log.warning("Erroduration wrong! emailUser:old duration : " +pfc.duration+" new Duration :"+duration+"  "+ emailUser);
				}
				Blob bComment = new Blob(this.getEncrypter().encrypt(comment));
				bpcEncrypted.setbComment(bComment);
				em.persist(bpcEncrypted);
				log.info("bg2 PhoneCAll updated !!! ");
			}
		} finally {
			em.close();
		}

	}

	
	public List<BeanPhoneCall> getListHistoric(String email, String phoneNumber, int fromIndex, int length) throws Exception {
		EntityManager em = UtilPersistence.createEntityManager();
		String queryStr;
		queryStr = "select t from BeanPhoneCallEntityCrypted2 t where t.email = :email   and t.bPhoneNumber = :bPhoneNumber";
		queryStr = "select t from BeanPhoneCallEntityCrypted2 t where t.email = :email   and t.bPhoneNumber = :bPhoneNumber order by t.date DESC";

		Query query = em.createQuery(queryStr);

		query.setFirstResult(fromIndex);
		query.setMaxResults(length);
		ShortBlob bPhoneNumber = new ShortBlob(this.getEncrypter().encrypt(phoneNumber));
		query.setParameter("email", email.toLowerCase().trim());
		query.setParameter("bPhoneNumber", bPhoneNumber);
		List<BeanPhoneCallEntityCrypted2> list = query.getResultList();
		if (list.size() == 0) {
			list = new ArrayList<BeanPhoneCallEntityCrypted2>();
		}
		log.log(Level.INFO, "bg2 getListHistoric  : " + phoneNumber + "    return list.size :" + list.size());

		em.close();
		return uncryptList(list);
	}

}
