package bg.phonelog.server.call;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import bg.phonelog.server.user.User_SS2;
import bg.phonelog.server.util.UtilPersistence;

public class BeanAlertFactory {

	public static BeanAlertFactory instance  = new BeanAlertFactory();

	public void persist(String emailUser, String phonecall, String contactName, String passwordUser) {
		EntityManager em = UtilPersistence.createEntityManager();
		BeanAlert ba = getBeanAlert(em, emailUser);
		ba.setContactName(contactName);
		ba.setPhonecall(phonecall);
		ba.setDate(new Date());
		ba.setNbAppels(ba.getNbAppels()+1);
		em.merge(ba);
		em.persist(ba);
		em.close();
	}
	
	public BeanAlert getBeanAlert(EntityManager em , String email){
		
		String queryStr ;
		queryStr = "select t from BeanAlert t where t.emailUser = :email ";
		Query query =  em.createQuery(queryStr);
		//query.ddSort("lastName", SortDirection.ASCENDING);
		query.setMaxResults(2);
		query.setParameter("email", email.toLowerCase().trim());
		List<BeanAlert>  list = query.getResultList();
		BeanAlert ba ;
		if (list.size()==0){
			ba = new BeanAlert();
			ba.setEmailUser(email.toLowerCase().trim());
		}else {
			ba = list.get(0);
		}
		return ba;

	}

	public BeanAlert getBeanAlert(String email) {
		EntityManager em = UtilPersistence.createEntityManager();
		BeanAlert ba = getBeanAlert(em,email);
		em.close();
		return ba;
	}
	
	public List<BeanAlert> getListAlerts(EntityManager em) {
		Query query =  em.createQuery("select t from BeanAlert ");
		List<BeanAlert>  list = query.getResultList();
		return list;
	}
}
