package bg.phonelog.server.call;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import bg.crypt.StringEncrypter;



public class BeanPhoneCall {

	@XmlElement
	String phoneNumber;
	@XmlTransient
	String contactName;
	@XmlElement
	String comment;
	@XmlTransient
	String email;
	@XmlTransient
    String search;
    @XmlElement
    Long id;
    @XmlElement
	Date date;
	@XmlElement
	Long duration;
	@XmlElement
	int type;
	@XmlElement
	String phoneUser;
	@XmlElement
	String messageSms;
	@XmlElement
	String idUser;
	@XmlElement
	String contact_id;
	
	public BeanPhoneCall() {
		super();
	}
	
	
	public BeanPhoneCall(String phoneNumber, String contactName, String comment, String email, Date date, Long duration, int type, String phoneUser, String messageSms, String idUser, String contact_id) {
		super();
		this.phoneNumber = phoneNumber;
		this.contactName = contactName;
		this.comment = comment;
		this.email = email;
		this.date = date;
		this.duration = duration;
		this.type = type;
		this.phoneUser = phoneUser;
		this.messageSms = messageSms;
		this.idUser = idUser;
		this.contact_id = contact_id;
	}

@Deprecated
	public BeanPhoneCall(String emailUser, String number, String contactName2,Date date2, Long duration2, Integer type2, String phoneUser2, String messageSms) {
		this.email = emailUser;
		this.phoneNumber = number;
		this.contactName = contactName2;
		this.date = date2;
		this.duration = duration2;
		this.type = type2;
		this.phoneUser = phoneUser2;
		this.messageSms = messageSms;
	}
	public BeanPhoneCall(HttpServletRequest request) {
		this();
			this.phoneNumber = request.getParameter("phoneNumber");
			this.contactName=request.getParameter("contactName");
			this.email  =request.getParameter("email");
			this.search = request.getParameter("search");
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getPhoneUser() {
		return phoneUser;
	}
	public void setPhoneUser(String phoneUser) {
		this.phoneUser = phoneUser;
	}
	
	public String getMessageSms() {
		return messageSms;
	}
	public void setMessageSms(String messageSms) {
		this.messageSms = messageSms;
	}
	public String getAge() {		
		return BeanPhoneCallEntityCrypted2.getAge(this.date);
	}
	


	public String getIdUser() {
		return idUser;
	}


	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}


	public String getContact_id() {
		return contact_id;
	}


	public void setContact_id(String contact_id) {
		this.contact_id = contact_id;
	}
	

	

	public BeanPhoneCallEntityCrypted2 getBeanPhoneCallEntityCrypted(StringEncrypter encrypter) throws Exception{
		BeanPhoneCallEntityCrypted2 entityCrypted = new BeanPhoneCallEntityCrypted2();
		entityCrypted.setbComment(encrypter.encryptBlob(this.comment));
		entityCrypted.setbContactName(encrypter.encryptShortBlob(this.contactName));
		entityCrypted.setbPhoneNumber(encrypter.encryptShortBlob(phoneNumber));
		entityCrypted.setbPhoneUser(encrypter.encryptShortBlob(phoneUser));
		entityCrypted.setDate(date);
		entityCrypted.setDuration(duration);
		entityCrypted.setEmail(email);
		entityCrypted.setId(id);
		entityCrypted.setType(type);
		entityCrypted.setbMessageSms(encrypter.encryptBlob(messageSms));
		return entityCrypted;
	}
	


	

}
