package bg.phonelog.server.call;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Logger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import bg.crypt.StringDeEncrypter;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.DataTypeUtils;
import com.google.appengine.api.datastore.ShortBlob;


@Entity
public class BeanPhoneCallEntityCrypted2  {

	private static final Logger log = Logger.getLogger(BeanPhoneCallEntityCrypted2.class.getName());

	
	private static final SimpleDateFormat formatAge = new SimpleDateFormat("HH 'h' mm 'mn'");
	private static final long TIME_ONE_DAY = 24l * 60l * 60l *1000l;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	private String email  ;
	private ShortBlob bPhoneNumber;
	private ShortBlob bContactName;
	               
	private Date date ;
	private  Blob  bComment;
	//@Transient
	private Long duration;
	private int type;
	private ShortBlob bPhoneUser ;
	private Blob bMessageSms;
	
	public BeanPhoneCallEntityCrypted2() {
		super();
		
	}
	
	

	



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}




	public ShortBlob getbPhoneNumber() {
		return bPhoneNumber;
	}







	public void setbPhoneNumber(ShortBlob bPhoneNumber) {
		this.bPhoneNumber = bPhoneNumber;
	}







	public ShortBlob getbContactName() {
		return bContactName;
	}







	public void setbContactName(ShortBlob bContactName) {
		this.bContactName = bContactName;
	}







	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public int getType() {
		return type;
	}



	public void setType(int type) {
		this.type = type;
	}



	public Blob getbComment() {
		return bComment;
	}







	public void setbComment(Blob bComment) {
		this.bComment = bComment;
	}







	public Long getDuration() {
		return duration;
	}







	public void setDuration(Long duration) {
		this.duration = duration;
	}







	public ShortBlob getbPhoneUser() {
		return bPhoneUser;
	}







	public void setbPhoneUser(ShortBlob bPhoneUser) {
		this.bPhoneUser = bPhoneUser;
	}







	public Blob getbMessageSms() {
		return bMessageSms;
	}







	public void setbMessageSms(Blob bMessageSms) {
		this.bMessageSms = bMessageSms;
	}







	public static String getAge(Date date2) {
		long age;
		
		if (date2 == null){
			age = System.currentTimeMillis();
		}else {
			age = System.currentTimeMillis() - date2.getTime();
		}
		
		if (age <= TIME_ONE_DAY+1000){
			return formatAge.format(age);
		} else {
			long nbJour =  (age/TIME_ONE_DAY);
			if (nbJour <= 1){
				return nbJour+" day";
			}else {
				return nbJour+" days";
			}
		}
		
	}

    	@Override
	public String toString() {
		return "BeanPhoneCallEntityCrypted [id=" + id + ", email=" + email
				+ ", bPhoneNumber=" + toString(bPhoneNumber)
				+ ", bContactName=" + toString(bContactName) 
				+ ", date=" + date 
				+ ", bComment=" + toString(bComment)
				+ ", bMessageSms=" + toString(bMessageSms)
				+ ", duration=" + duration + ", type=" + type + ", bPhoneUser="
				+ toString(bPhoneUser) + "]";
	}

    private static String toString(Blob b)	{
    	if (b == null){
    		return "";
    	}
    	return Arrays.toString(b.getBytes());
    }
    
    private static String toString(ShortBlob b)	{
    	if (b == null){
    		return "";
    	}
    	return Arrays.toString(b.getBytes());
    }
    	
    	
    	
    	
    


		public BeanPhoneCall getBeanPhoneCall(StringDeEncrypter decrypter) throws Exception{
			BeanPhoneCall beanPhoneCall = new BeanPhoneCall();
			beanPhoneCall.setDate(date);
    		beanPhoneCall.setDuration(duration);
    		beanPhoneCall.setEmail(email);
    		beanPhoneCall.setId(id);
    		beanPhoneCall.setType(type);
    		
			
			if (decrypter == null){
				String phoneDecrypted = new String(bPhoneNumber.getBytes());
				
				beanPhoneCall.setPhoneNumber(phoneDecrypted);
	    		 
	    		beanPhoneCall.setComment(toStringBg(this.bComment));
	    		beanPhoneCall.setContactName(toStringBg(this.bContactName));
	    		beanPhoneCall.setPhoneUser(toStringBg(bPhoneUser));
	    		beanPhoneCall.setMessageSms(toStringBg(bMessageSms));
			}else {
			String phoneDecrypted = decrypter.decrypt(bPhoneNumber);
			if (phoneDecrypted==null) {
    			throw new Exception("Decrypt no consistent !!");
    		}
			beanPhoneCall.setPhoneNumber(phoneDecrypted);
    		
    		beanPhoneCall.setComment(decrypter.decrypt(this.bComment));
    		beanPhoneCall.setContactName(decrypter.decrypt(this.bContactName));
    		beanPhoneCall.setPhoneUser(decrypter.decrypt(bPhoneUser));
    		beanPhoneCall.setMessageSms(decrypter.decrypt(bMessageSms));
			}
    		return beanPhoneCall;
    	}







		private String toStringBg(Blob blob) {
			if (blob == null){
				return "";
			}
			if (blob.getBytes() == null){
				return "";
			}
			return new String(blob.getBytes());
		
		}
		
		private String toStringBg(ShortBlob blob) {
			if (blob == null){
				return "";
			}
			if (blob.getBytes() == null){
				return "";
			}
			return new String(blob.getBytes());
		
		}
	
}


