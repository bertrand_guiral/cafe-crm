package bg.phonelog.server.call;

import java.net.URLEncoder;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BeanAlert {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	private String emailUser;
	private String phonecall;
	private String contactName;
	private Date date;
	private String search;
	private Integer nbAppels;
	
	public BeanAlert() {
	}



	public String getEmailUser() {
		return emailUser;
	}

	public void setEmailUser(String emailUser) {
		this.emailUser = emailUser;
	}

	public String getPhonecall() {
		return phonecall;
	}

	public void setPhonecall(String phonecall) {
		this.phonecall = phonecall;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}
	
	
	public String getAge() {
		return BeanPhoneCallEntityCrypted2.getAge(this.date);
	}


	public String getSearch() {
		if (search== null){
			return URLEncoder.encode((this.phonecall+" "+this.contactName).trim());
		}else {
			return URLEncoder.encode(search);
		}
	}


	public void setSearch(String search) {
		this.search = search;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}
	
	public int getNbAppels() {
		if (nbAppels == null){
			return 0;
		}
		return nbAppels;
	}


	public void setNbAppels(int nbAppels) {
		this.nbAppels = nbAppels;
	}


	
}
