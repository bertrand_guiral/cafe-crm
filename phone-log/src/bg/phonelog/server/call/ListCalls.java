package bg.phonelog.server.call;

import java.util.ArrayList;
import java.util.List;

public class ListCalls extends ArrayList<BeanPhoneCallEntityCrypted2>{

	private int size =0;
	
	public ListCalls() {
		super();
	}
	

	public ListCalls(List<BeanPhoneCallEntityCrypted2> list) {
		this();
		if (list != null){
			this.addAll(list);
		}
	}


	public int getSize() {
		return this.size();
	}


}
