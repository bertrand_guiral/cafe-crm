package bg.phonelog.server.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Cipher;

public class UtilCipher {
	
	private static final Logger log = Logger.getLogger(UtilCipher.class.getName());


	public UtilCipher() {
		// TODO Auto-generated constructor stub
	}

	public static Cipher getCipher() {
		try {
			Cipher cipher = Cipher.getInstance("PBEWithMD5AndDES");
			return cipher;
		} catch (Exception e) {
			log.log(Level.WARNING,"getCipher Exception"+e.getClass()+"  "+e.getMessage(),e);
			return null;
		}
	}
	
	
	public static byte[] getCrypted(String str, Cipher cipher) throws Exception{
		 return cipher.doFinal(str.getBytes());
	}


}
