package bg.phonelog.server.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.TagSupport;

public class DisplayCallType extends TagSupport {


	private static final long serialVersionUID = 1L;
	protected BodyContent bodyOut;
	private String type;
	
	
	
	public DisplayCallType() {		
	}

 

	@Override
	public int doEndTag() throws JspException {
		try {
			
			JspWriter out = pageContext.getOut();
			String img="<img src=\"/images/call_"+type+".png\"/>";
			String s = "<span title=\""+getComment(type)+"\">"+img+"</span>";
            out.println(s);
			
		} catch (IOException e) {
			throw new JspException(e);
		}
		return EVAL_PAGE;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	private String getComment(String type){
		if (type == null){
			return "??";
		}
		if (type.equals("1")){
			return "incomming call";
		}
		if (type.equals("2")){
			return "outgoing call";
		}
		if (type.equals("3")){
			return "unanswered incoming call";
		}
		
		return "????";
	}
}
