package bg.phonelog.server.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class DisplayPhoneNumber extends TagSupport{

	private static final long serialVersionUID = 1L;
	private String value;
	
	public DisplayPhoneNumber() {
	}

	@Override
	public int doEndTag() throws JspException {
		try {
			
			JspWriter out = pageContext.getOut();
			
			if (value== null){
				value ="";
			}
			if (value.trim().equals("-2")){
				value="UNKNOWN";
			}
			String s = value;
            out.println(s);
			
		} catch (IOException e) {
			throw new JspException(e);
		}
		return EVAL_PAGE;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
