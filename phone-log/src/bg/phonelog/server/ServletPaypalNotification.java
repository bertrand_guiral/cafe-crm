package bg.phonelog.server;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.phonelog.server.paypal.BeanPaypal;
import bg.phonelog.server.user.DelegateFactory;
import bg.phonelog.server.user.UserFactory;
import bg.phonelog.server.user.User_SS2;
import bg.phonelog.server.util.UtilPersistence;

public class ServletPaypalNotification extends HttpServlet {

	//23:17:39 Aug 07, 2013 PDT
	private static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss MMM dd, yyyy z",Locale.US);
	private static final Logger log = Logger.getLogger(ServletPaypalNotification.class.getName());

	private static String KEY_IdBg = "option_selection1";
	private static String KEY_payment_status = "payment_status";
	private static String KEY_payer_email = "payer_email";
	private static String KEY_payer_id = "payer_id";
	private static String KEY_receiver_email = "receiver_email";
	private static String KEY_mc_gross = "mc_gross";
	private static String KEY_mc_currency = "mc_currency";
	private static String KEY_item_number = "item_number";
	private static String KEY_payment_date ="payment_date";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ServletPaypalNotification() {
		super();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		String s = "";
		Enumeration enu = request.getParameterNames();
		while (enu.hasMoreElements()) {
			Object k = enu.nextElement();
			s += "&" + k + "=" + request.getParameter("" + k);
			log.warning("param ->  " + k + " : " + request.getParameter("" + k));
		}
		String emailUserBg = request.getParameter(KEY_IdBg);
		if (emailUserBg == null){
			emailUserBg= request.getParameter("custom");
		}
		String payment_status = request.getParameter(KEY_payment_status);
		String payer_email = request.getParameter(KEY_payer_email);
		String payer_id = request.getParameter(KEY_payer_id);
		String receiver_email = request.getParameter(KEY_receiver_email);
		String mc_gross = request.getParameter(KEY_mc_gross);
		String mc_currency = request.getParameter(KEY_mc_currency);
		String item_number = request.getParameter(KEY_item_number);
		String payment_date_str= request.getParameter(KEY_payment_date);
		String payment_type=request.getParameter("payment_type");
		String txn_type = request.getParameter("txn_type");
		Date payment_date = null;
			try {
				if (payment_date_str == null){
					payment_date = new Date();
				}else {
					payment_date = 	sdf.parse(payment_date_str);
				}
			} catch (Exception e) {
				payment_date = new Date();
				e.printStackTrace();
			}
		
		BeanPaypal beanPaypal = new BeanPaypal(emailUserBg, payment_status, payer_email, payer_id, receiver_email, mc_gross, mc_currency, item_number,payment_date,payment_type,txn_type);
		log.warning("----------- checkPayment (Masse ou Abonnement) : "+beanPaypal.checkPayment()+" emailUserBg : "+emailUserBg);
		if (payment_status==null){
			log.warning("----------- payment_status is null !! cette notification  n'est pas un payment !! ");
		}else if(beanPaypal.isPaymentOK()){
			EntityManager em = UtilPersistence.createEntityManager();
			User_SS2 user = UserFactory.getInstance().getUser_SS(em,emailUserBg);
			
			if (beanPaypal.isAbonnementRecurrent()){
				log.warning("----------- checkPaymentAbonnement "+beanPaypal.checkPaymentAbonnement()+" emailUserBg : "+emailUserBg);
				Date dateAbonnementOK = beanPaypal.getDateLimiteAbonementOK();			
				user.setDateAbonnementOK(dateAbonnementOK,User_SS2.PAYMENT_BY_ABONNEMENT);
				log.warning("user save persist done isAbonnementRecurrent isPAyementOK  emailUserBg : "+emailUserBg+" dateLimite : "+dateAbonnementOK);
			//}else if (beanPaypal.isPaymentMasse()){// C'est un paiement de masse
			}else {// C'est un paiement de masse
				user.setDatePaiementAbonnementDelegate(new Date());
				Date dateAbonnementOK = beanPaypal.getDateLimiteAbonementOK();
				DelegateFactory.getInstance().setPaiementDate(user, new Date(),dateAbonnementOK);
				log.warning("user save persist done isAbonnementMasse isPAyementOK  emailUserBg : "+emailUserBg);
			}
			em.persist(user);
			em.close();
			
			
		}else{
			log.warning("Payement KO emailUserBg : "+emailUserBg);
		}
		log.warning("parameters list : " + s);
		resp.getWriter().write("" + s);
	}
	
	
	public static void main(String[] a) throws Exception{
		Locale.setDefault(Locale.US);
		
		System.out.println(sdf.format(new Date()));
		String s ="23:17:39 Aug 07, 2013 PDT";
		Thread.sleep(10);
		System.out.println(s);
		Date d  = sdf.parse(s);
		System.out.println(d);
	}

}
