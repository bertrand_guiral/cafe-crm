package bg.phonelog.server.gwt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import bg.phonelog.client.alert.Alert;
import bg.phonelog.client.calls.CallInfo;
import bg.phonelog.server.call.BeanAlert;
import bg.phonelog.server.call.BeanPhoneCall;
import bg.phonelog.server.call.BeanPhoneCallFactory;

public class UtilServer {

	private static final Logger log = Logger.getLogger(UtilServer.class.getName());

	
	public static List<CallInfo> createMockListCallInfo(int fromIndex, int length) throws Exception {
		List<CallInfo> list = new ArrayList<CallInfo>();
		for(int i =fromIndex; i<fromIndex+length;i++){
			CallInfo callInfo = new CallInfo();
			String s = ("000000000000000"+i+"00");
			callInfo.setPhoneNumber(s.substring(s.length()-6,s.length()-1));
			callInfo.setContactName(""+i+" xxx");
			list.add(callInfo);
		}
		return list;
	}
	

	public static List<CallInfo> getListCallInfo(String pass, String email,int fromIndex, int length) throws Exception {
		List<BeanPhoneCall> listBeanPhoneCall = BeanPhoneCallFactory.getInstance(pass). getListBeanPhoneCall(email, fromIndex);
		List<CallInfo> list =toCallInfo(listBeanPhoneCall);
		return list;
	}

	private static List<CallInfo> toCallInfo(List<BeanPhoneCall> listBeanPhoneCall) {
		List<CallInfo> list = new ArrayList<CallInfo>();
		for(BeanPhoneCall beanPhoneCall: listBeanPhoneCall){
			CallInfo callInfo = toCallInfo(beanPhoneCall);
			list.add(callInfo);
		}
		return list;
	}

	private static CallInfo toCallInfo(BeanPhoneCall beanPhoneCall) {
		CallInfo callInfo = new CallInfo();
		callInfo.setPhoneNumber(beanPhoneCall.getPhoneNumber());
		callInfo.setContactName(beanPhoneCall.getContactName());
		callInfo.setPhoneUser(beanPhoneCall.getPhoneUser());
		callInfo.setId(beanPhoneCall.getId());
		callInfo.setComment(beanPhoneCall.getComment());
		callInfo.setDate(beanPhoneCall.getDate());
		callInfo.setTypeCall(""+beanPhoneCall.getType());
		callInfo.setDuration(beanPhoneCall.getDuration());
		callInfo.setMessageSms(beanPhoneCall.getMessageSms());
		return callInfo;
	}

	public static void deleteCallInfo(String pass, String email, List<Long> list) throws Exception{
			try {
				log.info("bg2 deleteCallInfo start");
				BeanPhoneCallFactory.getInstance(pass).removePhoneCall(email, list);
			} catch (Exception e) {
				String trace ="";
				for(Long l : list){
					trace +=" id="+l+";";
				}
				log.log(Level.WARNING,"bg2  deleteCallInfo "+trace+"exception",e);
				throw e;
			}
	}

	public static Alert toAlert(BeanAlert beanAlert) {
		if (beanAlert == null){
			return null;
		}
		Alert alert = new Alert();
		alert.setId(beanAlert.getId());
		alert.setContactName(beanAlert.getContactName());
		alert.setDate(beanAlert.getDate());
		alert.setEmailUser(beanAlert.getEmailUser());
		alert.setPhonecall(beanAlert.getPhonecall());
		return alert;
	}


	public static List<CallInfo> refreshListCallInfo(String pass, String email, Date upToDate) {
		log.log(Level.WARNING, "refresh start");
		log.log(Level.WARNING, "refresh start email "+email);
		log.log(Level.WARNING, "refresh pass pass "+pass);
		log.log(Level.WARNING, "refresh pass upToDate "+upToDate);
		List<BeanPhoneCall> listBeanPhoneCall = BeanPhoneCallFactory.getInstance(pass). refreshListCallInfo(email, upToDate);
		List<CallInfo> list =toCallInfo(listBeanPhoneCall);
		return list;
	}


	public static List<CallInfo> getHistoric(String pass, String email,String phoneNumber, int fromIndex, int length) throws Exception{
		List<BeanPhoneCall> listBeanPhoneCall = BeanPhoneCallFactory.getInstance(pass). getListHistoric(email,phoneNumber, fromIndex,length);
		List<CallInfo> list =toCallInfo(listBeanPhoneCall);
		return list;
	}
	
	

}
