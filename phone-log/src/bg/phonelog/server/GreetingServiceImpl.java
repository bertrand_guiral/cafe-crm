package bg.phonelog.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import bg.phonelog.client.GreetingService;
import bg.phonelog.client.alert.Alert;
import bg.phonelog.client.calls.CallInfo;
import bg.phonelog.client.delegates.Delegate;
import bg.phonelog.client.delegates.InfosFacturesGroup;
import bg.phonelog.server.call.BeanAlert;
import bg.phonelog.server.call.BeanAlertFactory;
import bg.phonelog.server.call.BeanPhoneCallFactory;
import bg.phonelog.server.gwt.UtilServer;
import bg.phonelog.server.user.DelegateFactory;
import bg.phonelog.server.user.Delegate_SS;
import bg.phonelog.server.user.UserFactory;
import bg.phonelog.server.user.User_SS2;
import bg.phonelog.server.user.UtilCalendar;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {

	private static final Logger log = Logger.getLogger(GreetingServiceImpl.class.getName());

	public String greetServer(String input) throws IllegalArgumentException {
		return "greetServer No More implemented !!!";
	}

	@Override
	public List<CallInfo> getListCallInfo(String pass,int fromIndex,int length) throws Exception {
		List<CallInfo> list;
		try {
			log.warning("getListCallInfo 111 pass "+pass+" fromIndex "+fromIndex+" length "+length);
			log.warning("getListCallInfo 111 email "+getEmail());
			list = UtilServer.getListCallInfo(pass,getEmail(), (int)fromIndex,length);
			//List<CallInfo>  list = UtilServer.createMockListCallInfo((int) fromIndex,length);
			log.warning("getListCallInfo 222 fromIndex "+fromIndex+"  length "+length+"  return list.size "+list.size()+" pass : "+pass);
		} catch (Exception e) {
			log.log( Level.WARNING,"getListCallInfo 333 Exception",e);
			throw e;
		}
		return list;
	}

	@Override
	public void deleteCallInfo(String pass,List<Long> list) throws Exception {
		UtilServer.deleteCallInfo(pass,getEmail(), list);
	}
	
	private String getEmail() throws Exception {
		User_SS2 user = getUser();
		return user.getEmail();
	}
	
	private User_SS2 getUser() {
		HttpSession httpSession = getThreadLocalRequest().getSession();
		User_SS2 user = (User_SS2) httpSession.getAttribute("user");
		return user;
	}
	

	@Override
	public void updateCallInfo(String pass,CallInfo callInfo) throws Exception {
			try {
				BeanPhoneCallFactory.getInstance(pass).updateComment(getEmail(), callInfo.getId(), callInfo.getComment());
			} catch (Exception e) {
				log.log(Level.WARNING, "Exception updating pass : "+pass+" callInfo :"+callInfo,e);
				throw new Exception("Error Updating");
			}
		
	}

	@Override
	public Alert getAlert(String pass) throws Exception {
		log.warning("getAlert  pass : "+pass);
		BeanAlert beanAlert =  BeanAlertFactory.instance.getBeanAlert(getEmail());
		return UtilServer.toAlert(beanAlert);
	}

	@Override
	public List<CallInfo> refreshListCallInfo(String pass,Date upToDate) throws Exception{
		log.warning("refreshListCallInfo  pass : "+pass);
		List<CallInfo> list = UtilServer.refreshListCallInfo(pass,getEmail(), upToDate);
		return list;
	}

	@Override
	public List<CallInfo> getHistoric(String pass, String phoneNumber,int fromIndex, int length) throws Exception {
		log.warning("getHistoric  pass : "+pass);
		List<CallInfo>  list = UtilServer.getHistoric(pass,getEmail(),phoneNumber, fromIndex,length);
		
		return list;
	}

	@Override
	public void addEmailDelegate(String email) throws Exception{
		try {
			User_SS2 user = getUser();
			log.warning("addEmailDelegate   : "+user.getEmail()+"   "+email);
			Delegate_SS d = new Delegate_SS(user.getEmail(),email, new Date());
			DelegateFactory.getInstance().persist(d);
			UserFactory.getInstance().updateUser(user);
		} catch (Exception e) {
			log.log(Level.SEVERE,e.getMessage(),e);
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public List<Delegate> getListDelegates() throws Exception {
		 List<Delegate> list = new ArrayList<Delegate>();
		 User_SS2 user = getUser();
		 List<Delegate_SS>  list_ss = DelegateFactory.getInstance().getListDelegates(user.getEmail());
		 for(Delegate_SS e : list_ss){
			 list.add(new Delegate(e.getEmailDelegate(),e.getDateAbonnementOK()));
		 }
		 log.warning("getListDelegates list.size "+list.size());
		 return list;
	}

	@Override
	public void deleteDelegates(List<Delegate> list) throws Exception {
		 try {
			User_SS2 user = getUser();
			 for(Delegate d :list){
				 String email = d.getEmail();
				 DelegateFactory.getInstance().delete(user.getEmail(),email);
				 log.warning("deleteDelegates - "+user.getEmail()+"  "+email);
			 }
			 log.warning("deleteDelegates list.size "+list.size());
			 //UserFactory.getInstance().updateUser(user);
		} catch (Exception e) {
			log.log(Level.SEVERE, ""+e.getMessage(),e);
			throw new Exception(e.getMessage());
		}
		return ;
	}

	@Override
	public InfosFacturesGroup getInfosFacturesGroup() {
		InfosFacturesGroup i = new InfosFacturesGroup();
		User_SS2 user = getUser();
		if (user != null){
			//i.setDateMaxPaiementMasse(user.getDateMaxPaiementMasse());
			i.setDateMaxPaiementMasse(UtilCalendar.getNextMonth_First());
			i.setEmailUser(user.getEmail());
		}else {
			i.setDateMaxPaiementMasse(new Date());
		}
		return i;
	}
	
	

	
	
}
