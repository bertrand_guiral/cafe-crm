package bg.phonelog.server.rest;

import java.net.URI;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
@Deprecated //see ServletPAypal
@Path("/paypal")
public class Paypal {
	
	private static final Logger log = Logger.getLogger(Paypal.class.getName());

	
	 @Context
	 private HttpServletRequest request;

	@GET
	@Path("test1")
	@Produces(MediaType.TEXT_HTML)
	public String test1() {
		return "<html><head> </head> <body>OK</body> </html>";
	}
	
	
	@GET
	@Path("return/ok")
	@Produces(MediaType.TEXT_HTML)
	public Response returnOK() {
		try {
			URI uri = new URI("../controller?action=displayJournal");
			return Response.temporaryRedirect(uri).build();
		} catch (Exception e) {
			String s= "<html><head> </head> <body>return paypal ok Exception : " +e.getMessage()+ "</body> </html>";
			return Response.status(500).header("bg2", "log").entity(s).build();
		}
	}
	@GET
	@Path("return/cancel")
	@Produces(MediaType.TEXT_HTML)
	public Response returnCancel() {
		try {
			URI uri = new URI("../controller?action=displayHome");
			return Response.temporaryRedirect(uri).build();
		} catch (Exception e) {
			String s= "<html><head> </head> <body>return paypal Cancel Exception : " +e.getMessage()+ "</body> </html>";
			return Response.status(500).header("bg2", "log").entity(s).build();
		}
	}
	
	@POST
	@Path("notification")

	public String notificationPaypal(
			@Context UriInfo uriInfo, 
			@Context final HttpServletRequest request,
		    @Context final HttpServletResponse response) throws Exception{
		String query = uriInfo.getRequestUri().getQuery();
		log.warning("notificationPaypal :"+query);
		log.warning("notificationPaypal QueryParameters :"+uriInfo.getQueryParameters());
		log.warning("notificationPaypal QueryParameters size :"+uriInfo.getQueryParameters().size());
		log.warning("notificationPaypal request :"+this.request);
		if(request != null){
			final Map<String, String[]> params = request.getParameterMap();
			log.warning("notificationPaypal params "+params); 
			log.warning("notificationPaypal request :"+request.getQueryString());
			if (params!= null){
				log.warning("notificationPaypal params size :"+params.size());
				for(String key : params.keySet()){
					log.warning("key : "+key+" value :  "+params.get(key));
				}
			}
		}
		//request.getRequestDispatcher("index_debug.jsp").forward(request, response);
		return "";
	}


}
