package bg.phonelog.server.rest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import bg.phonelog.server.call.BeanAlertFactory;
import bg.phonelog.server.call.BeanPhoneCall;
import bg.phonelog.server.call.BeanPhoneCallFactory;
import bg.phonelog.server.user.UserFactory;
import bg.phonelog.server.user.User_SS2;





@Path("/phonecall")
public class PhoneCall {

	public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static final Logger log = Logger.getLogger(PhoneCall.class.getName());

	@GET
	@Path("test1")
	@Produces(MediaType.TEXT_HTML)
	public String test1() {
		return "<html><head> </head> <body>OK</body> </html>";
	}
	
	@GET
	@Path("test/{id}")
	@Produces(MediaType.TEXT_HTML)
	public String test(@PathParam("id") String id) {
		return "<html><head> </head> <body>OK " +id+"</body> </html>";
	}
	/**
	 * 
	 * @param emailUser
	 * @param phonecall
	 * @param contactname
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("alert")
	@Produces(MediaType.TEXT_PLAIN)
	public Response alert( 
			@FormParam("emailUser") 
			String emailUser, 
			@FormParam("phonecall") String phonecall, 
			@FormParam("number") String number, 
			@FormParam("contact_id") String contact_id,
			@FormParam("contactName") String contactName, 
			@FormParam("cryptPhrase") String  cryptPhrase,
			@FormParam("passwordUser") String passwordUser) throws Exception {
		int code = 200;
		//BeanAlert beanALert = new BeanAlert(emailUser,phonecall, contactName,new Date());
		if (phonecall == null){ // phonecall est le parametre utilisé dans les plus anciennes versions. Pour assurer la compatibilité ...
			phonecall = number;
		}
		BeanAlertFactory.instance.persist(emailUser,phonecall,contactName,passwordUser);
		log.warning("bg2 alert emailUser:"+emailUser+" phonecall:"+phonecall+"  "+contactName+" cryptPhrase: "+cryptPhrase);
		
		return  Response.status(code).header("bg2", "changePassword").header("email", ""+emailUser).entity("ok "+emailUser).build();
	}
	/*
		 */
	@POST
	@Path("log")
	@Produces(MediaType.TEXT_PLAIN)
	public  Response log(
			@FormParam("idUser") String idUser,
			@FormParam("emailUser") String emailUser,
			@FormParam("passwordUser") String passwordUser,
			@FormParam("phoneUser") String phoneUser,
			@FormParam("number") String number,
			@FormParam("date") Long date,
			@FormParam("contact_id") String contact_id,
			@FormParam("contactName") String contactName,
			@FormParam("duration") Long duration,
			@FormParam("type") Integer type,
			@FormParam("cryptPhrase") String  cryptPhrase,
			@FormParam("messageSms") String  messageSms,
			@FormParam("comment") String  comment
			) throws Exception{
		log.warning("bg2 log emailUser:"+emailUser+" phoneUser:"+phoneUser+" "+number+" "+date+" "+contactName+" "+duration+" "+type+"  cryptPhrase: "+cryptPhrase+" messageSms :"+messageSms);
		int code = 200;
		if (comment == null){
			comment ="";
		}
		//BeanPhoneCall beanPhoneCall = new BeanPhoneCall(emailUser,number, contactName,new Date(date),duration,type,phoneUser,messageSms);
		BeanPhoneCall beanPhoneCall = new BeanPhoneCall( number,  contactName,  comment,  emailUser, new Date(date), duration,  type,  phoneUser,  messageSms,  idUser,  contact_id);
		BeanPhoneCallFactory.getInstance(cryptPhrase).persist(beanPhoneCall);
		log.warning("bg2 persist beanPhoneCall done");
		return  Response.status(code).header("bg2", "log").entity("ok "+emailUser+"  "+phoneUser).build();
	}
	
	@POST
	@Path("update")
	@Produces(MediaType.TEXT_PLAIN)
	public Response update(
			@FormParam("idUser") String idUser,
			@FormParam("contact_id") String contact_id,
			@FormParam("emailUser") String emailUser,
			@FormParam("phoneUser") String phoneUser,
			@FormParam("number") String number,
			@FormParam("date") Long date,
			@FormParam("contactName") String contactName,
			@FormParam("duration") Long duration,
			@FormParam("type") Integer type,
			@FormParam("cryptPhrase") String  cryptPhrase,
			@FormParam("comment") String  comment,
			@FormParam("messageSms") String  messageSms
			)throws Exception{
		
		log.warning("bg2 update emailUser:"+emailUser+"| phoneUser:"+phoneUser+"| number : "+number+"| date:"+date+"| contactName :"+contactName+"| duration :"+duration+"| type :"+type+"|  cryptPhrase: "+cryptPhrase+" comment "+comment);
		int code = 200;
		if (comment == null){
			comment ="";
		}
		//BeanPhoneCall beanPhoneCall = new BeanPhoneCall(emailUser,number, contactName,new Date(date),duration,type,phoneUser);
		BeanPhoneCallFactory.getInstance(cryptPhrase).updatePhoneCall(emailUser,phoneUser,number,new Date(date),duration,type,cryptPhrase,contactName,comment,messageSms);
		log.warning("bg2 update done : "+emailUser);
		return  Response.status(code).header("bg2", "log").entity("ok "+emailUser+"  "+phoneUser).build();

	}
	
	@POST
	@Path("generateTest")
	@Produces(MediaType.TEXT_PLAIN)
	public Response generateTest (
			@FormParam("date") String dateStr,
			@FormParam("emailUser") String emailUser,
			@FormParam("phoneUser") String phoneUser,
			@FormParam("type") int type,
			@FormParam("phoneNumber") String phoneNumber,
			@FormParam("duration") long duration,
			@FormParam("contactName") String contactName,
			@FormParam("comment") String comment,
			@FormParam("cryptPhrase") String cryptPhrase,
			@FormParam("messageSms") String messageSms
					
			) throws Exception{
		Date date = simpleDateFormat.parse(dateStr);
		BeanPhoneCall beanPhoneCall = new BeanPhoneCall(emailUser,phoneNumber, contactName,date,duration,type,phoneUser,messageSms);
		BeanPhoneCallFactory.getInstance(cryptPhrase).persist(beanPhoneCall);
		
		int code = 200;
		return  Response.status(code).header("bg2", "log").entity("ok date = "+date+"  "+emailUser+"  phoneUser = "+phoneUser+" type = "+type+" phoneNumber = "+phoneNumber+" duration = "+duration+" contactName = "+contactName+" comment= "+comment).build();
	}
	
	/**
	 * 
	 * @param idUser
	 * @param emailUser
	 * @param passwordUser
	 * @param cryptPhrase
	 * @param fromIndex
	 * @return List Historic Appels
	 */
	@POST
	@Path("historic")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BeanPhoneCall> getHistoric(
			@FormParam("idUser") String idUser,
			@FormParam("emailUser") String emailUser,
			@FormParam("passwordUser") String passwordUser,
			@FormParam("cryptPhrase") String  cryptPhrase,
			@FormParam("fromIndex") int  fromIndex
			) {
		log.info(" idUser "+idUser+" emailUser"+emailUser+" passwordUser "+passwordUser+" cryptPhrase "+cryptPhrase+" fromIndex "+fromIndex);
		List<BeanPhoneCall> listBeanPhoneCall = BeanPhoneCallFactory.getInstance(cryptPhrase). getListBeanPhoneCall(emailUser, fromIndex);
		return listBeanPhoneCall;
	}	
	
	@GET
	@Path("testjson")
	@Produces(MediaType.APPLICATION_JSON)
	public BeanPhoneCall testJson() {
			BeanPhoneCall b = new BeanPhoneCall();
			
			String s = UserFactory.getInstance().utilMigration();
			b.setComment(s);
			return b;
	}
	/**
	 * 
	 * @param emailUser
	 * @param name
	 * @param passwordUser
	 * @return
	 * @throws Exception if user already registered 
	 */
	@POST
	@Path("register")
	@Produces(MediaType.APPLICATION_JSON)
	public User_SS2 register(
			@FormParam("emailUser") String emailUser,
			@FormParam("name") String name,
			@FormParam("passwordUser") String passwordUser
		) throws Exception {
		try {
			log.info(" emailUser "+emailUser+" emailUser"+emailUser+" passwordUser "+passwordUser);
			User_SS2 user_SS = new User_SS2(emailUser, name, passwordUser);
			UserFactory.getInstance().register(user_SS);
			return user_SS;
		} catch (Exception e) {
			log.warning("Exception emailUser "+emailUser+" emailUser"+emailUser+" passwordUser "+passwordUser+"  e.getMessage : "+e.getMessage());
			
			throw new Exception(e.getMessage());
		}
	}
	
	@POST
	@Path("login")
	@Produces(MediaType.APPLICATION_JSON)
	public User_SS2 login(
			@FormParam("emailUser") String emailUser,
			@FormParam("passwordUser") String passwordUser
			){
		User_SS2 user = UserFactory.getInstance().getUser_SS(emailUser, passwordUser);
		return user;
	}

	
	

}
