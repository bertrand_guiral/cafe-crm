package bg.phonelog.server;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.phonelog.server.call.BeanUtil;
import bg.phonelog.server.user.UserFactory;
import bg.phonelog.server.user.User_SS2;
import bg.phonelog.server.util.UtilMail;

public class ServletController extends HttpServlet {

	//@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(ServletController.class.getName());

	private static final long serialVersionUID = 1L;

	private static String action_displayHome = "displayHome";
	private static String action_displayContact = "displayContact";
	private static String action_sendMessage ="sendMessage";
	private static String action_displayJournal ="displayJournal";
	private static String action_displayParams = "displayParams";
	private static String action_changePassword ="changePassword";
	private static String action_displayManagementDelegates="displayManagementDelegates";
	private static String action_cleanAbonnementDebug="cleanAbonnementDebug";
	
	private static String page_phone_log= "phone_log.jsp";
	
	private transient String name ;
	
	public ServletController() {
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String action = request.getParameter("action");
			String page_target = page_phone_log;
			if (action == null) {
				action = "";
			}
			User_SS2 user =(User_SS2) request.getAttribute("user");
			boolean isConnected = user!= null;
			if(isConnected ){
				page_target =  page_phone_log;
			}else {
				page_target = "page_login.jsp";
			}
			BeanUtil beanUtil = (BeanUtil) request.getAttribute("util");
			
			if (action.equals(action_displayJournal)){
			}else if (action.equals(action_sendMessage)){
				doProcessMessage(request);
				beanUtil.setLog("Message sent");
				page_target = "page_welcome.jsp";
			}else if (action.equals(action_displayHome)){
				page_target="page_welcome.jsp"; 
			}else if (action.equals(action_displayContact)){
				page_target="page_contact.jsp"; 
			}else if (action.equals(action_displayParams)){
				page_target="page_params.jsp"; 
			}else if (action.equals(action_displayManagementDelegates)){
				page_target="page_management_delegates.jsp"; 
			}else if (action.equals(action_changePassword)){
				String password = request.getParameter("password");
				String newPassword_1 = request.getParameter("newPassword_1");
				String newPassword_2 = request.getParameter("newPassword_2");
				CheckPassword checkPassword = new CheckPassword(user,password, newPassword_1, newPassword_2);
				if (checkPassword.isOK){
					user.setPassword(newPassword_1);
					UserFactory.getInstance().updateUser(user);
					beanUtil.setLog("Password updated");
					//page_target="page_params.jsp";
				}else {
					beanUtil.setLog(checkPassword.comment);
					page_target ="page_params.jsp";
				}
				
				
			}else if (action.equals(action_cleanAbonnementDebug)){
				log.warning("action_cleanAbonnementDebug start");
				String s = UserFactory.getInstance().doCleanAbonnementDebug();
				beanUtil.setLog(s);
			}else if (action.equalsIgnoreCase("excel")){
				doExcel_(request,response);
				return;
			}
			
			
			
			request.setAttribute("util", beanUtil);
			
			RequestDispatcher rd = request.getRequestDispatcher(page_target);
			rd.forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		} 
	}

	

	
	



	private void doProcessMessage(HttpServletRequest request) {
		@SuppressWarnings("rawtypes")
		Enumeration enu =  request.getParameterNames();
		String r ="date = "+new Date()+"  & \n";
		while(enu.hasMoreElements()){
			String key =""+enu.nextElement();
			r += key+"="+request.getParameter(key)+"&   \n";
		}
		log.info("message "+r);
		String[] emails = { "bertrand.guiral@gmail.com"};
		UtilMail.sendMessage("FROM PHONE LOG !!!! bg", ""+r, emails);
	}

	private void doExcel_(HttpServletRequest request, HttpServletResponse response) throws IOException{
		//response.getWriter().print("Excel no implemented yet !!!!!");
		new UtilExcelServlet(request,response);
	}

}
