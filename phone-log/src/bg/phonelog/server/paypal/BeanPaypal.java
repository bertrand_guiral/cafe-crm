package bg.phonelog.server.paypal;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;



public class BeanPaypal implements Serializable{

	private static String montantAbonnement ="2.00";
	private static double montant_abonnement_d = 2.0d;
	private static final long serialVersionUID = 1L;
	String emailUserBg;
	String payment_status ;
	String payer_email ;
	String payer_id  ;
	String receiver_email ; 
	String mc_gross ;
	String mc_currency ;
	String item_number  ;
	Date payment_date;
	String payment_type;
	String txn_type;
	public BeanPaypal() {
	}


	public BeanPaypal(String idBg, String payment_status, String payer_email,
			String payer_id, String receiver_email, String mc_gross,
			String mc_currency, String item_number,Date date, String payment_type, String txn_type) {
		super();
		this.emailUserBg = idBg;
		this.payment_status = payment_status;
		this.payer_email = payer_email;
		this.payer_id = payer_id;
		this.receiver_email = receiver_email;
		this.mc_gross = mc_gross;
		this.mc_currency = mc_currency;
		this.item_number = item_number;
		this.payment_date =date;
		this.payment_type =payment_type;
		this.txn_type=txn_type;
	}
	
	public String checkPaymentAbonnement() {
		String s ="";
		
		if( mc_gross == null){
			s+=" mc_gross (montant) is Null";
		}else if (getDouble(mc_gross) < montant_abonnement_d){
			s += "mc_gross  <  montantAbonnement ("+mc_gross+" - "+getDouble(mc_gross)+" < "+montant_abonnement_d+")";
		}
		return s;
	}
	
	public String checkPayment() {
		String s ="";
		if (payment_status ==  null){
			s += " payment_status is Null";
		}else if (!payment_status.equals("Completed")){
			s += " PAyment Status not \"completed\"";
		}
		
		return s;
	}
	
	
	private double getDouble(String mc_gross2) {
		try {
			return Double.parseDouble(mc_gross2);
		} catch (NumberFormatException e) {
			return -1;
		}
	}


	public boolean isPaymentOK(){
		if (!payment_status.equals("Completed")){
			return false;
		}
		if (!mc_currency.equals("EUR")){
			return false;
		}
		
		return true;
	}
	
	public Date getDateLimiteAbonementOK(){
		Calendar c = GregorianCalendar.getInstance();
		c.setTime(this.payment_date);
		c.add(Calendar.MONTH, 1);
		return c.getTime();
	}


	public boolean isAbonnementRecurrent() {
		if (this.txn_type == null){
			return false;
		}else if (txn_type.equals("subscr_payment")){
			return true;
		}
		return false;
	}
	
	public boolean isPaymentMasse() {
		if (this.txn_type == null){
			return false;
		}else if (txn_type.equals("web_accept")){
			return true;
		}
		return false;
	}
	
}
