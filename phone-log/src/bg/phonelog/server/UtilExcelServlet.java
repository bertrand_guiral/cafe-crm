package bg.phonelog.server;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UtilExcelServlet {

	
	
	
	public UtilExcelServlet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.setContentType("application/vnd.ms-excel");
		Writer out =   response.getWriter();
		String[] columns = request.getParameterValues("colonne");
		String nb_lignes_str = request.getParameter("nb_lignes");
		int nb_lignes = Integer.parseInt(nb_lignes_str);
		String titres ="";
		for(String colonne: columns){
			titres+=colonne+";";
		}	
		titres+=""+nb_lignes_str+";";
		titres+="\n";
		out.append(titres);
		for(int i=0; i<nb_lignes;i++){
			String ligne="";
			for(String colonne: columns){
				String c = colonne+"_"+i;
				String value = request.getParameter(c);
				value = (""+value).replace(';', ' ');
				ligne += value+";";
			}
			ligne += "\n";
			out.append(ligne);
		}
	}

}
